import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DeploymentDifferenceComponent} from './deployment-difference.component';

describe('DeploymentDifferenceComponent', () => {
  let component: DeploymentDifferenceComponent;
  let fixture: ComponentFixture<DeploymentDifferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DeploymentDifferenceComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeploymentDifferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
