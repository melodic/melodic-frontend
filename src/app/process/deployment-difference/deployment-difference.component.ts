import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {ProcessService} from '../service/process.service';
import {ComponentDifference} from '../model/component-difference';

@Component({
  selector: 'app-deployment-difference',
  templateUrl: './deployment-difference.component.html',
  styleUrls: ['./deployment-difference.component.css']
})
export class DeploymentDifferenceComponent implements OnInit {

  loadingDataInProgress = false;
  differenceAfterSolutionDownloaded = false;
  differenceAfterDeploymentDownloaded = false;

  displayedColumns = ['key', 'toCreate', 'toDelete', 'toRemain'];
  dataSource: MatTableDataSource<ComponentDifference>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private processService: ProcessService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
  }

  downloadDifference() {
    this.loadingDataInProgress = true;
    this.processService.getDeploymentDifference(localStorage.getItem('processId')).subscribe(value => {
        this.dataSource = new MatTableDataSource(value.diff);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.loadingDataInProgress = false;
        this.differenceAfterSolutionDownloaded = true;
      },
      error1 => {
        if (error1.error.status === 404) {
          console.log(`Deployment difference not created yet: ${error1.error.message}`);
          this.loadingDataInProgress = false;
        } else {
          this.snackBar.open(`Problem by getting deployment difference: ${error1.error.message}`, 'Close');
          this.loadingDataInProgress = false;
        }
      });
  }
}
