import {CpMetric} from './cp-metric';
import {CpVariable} from './cp-variable';
import {CpConstant} from './cp-constant';

export class CpModel {
  id: string;
  metrics: Array<CpMetric>;
  variables: Array<CpVariable>;
  constants: Array<CpConstant>;
  utilityFormula: string;
}
