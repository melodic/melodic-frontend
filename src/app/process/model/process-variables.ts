import {VariableStatus} from './variable-status';

export class ProcessVariables {
  discoveryServiceResult: VariableStatus;
  cpCreationResultCode: VariableStatus;
  cpSolutionResultCode: VariableStatus;
  applicationDeploymentResultCode: VariableStatus;
  processState: VariableStatus;
  reconfigurationProcess: boolean;
  applicationId: string;
  simulation: boolean;

  constructor() {
    this.discoveryServiceResult = VariableStatus.UNKNOWN;
    this.cpCreationResultCode = VariableStatus.UNKNOWN;
    this.cpSolutionResultCode = VariableStatus.UNKNOWN;
    this.applicationDeploymentResultCode = VariableStatus.UNKNOWN;
    this.processState = VariableStatus.UNKNOWN;
    this.reconfigurationProcess = false;
    this.applicationId = '';
    this.simulation = false;
  }
}
