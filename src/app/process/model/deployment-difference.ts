import {ComponentDifference} from './component-difference';

export class DeploymentDifference {
  diff: Array<ComponentDifference>;
}
