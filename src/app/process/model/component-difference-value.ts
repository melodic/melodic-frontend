import {ComponentDifferenceValueItem} from './component-difference-value-item';

export class ComponentDifferenceValue {
  toCreate: Array<ComponentDifferenceValueItem>;
  toDelete: Array<ComponentDifferenceValueItem>;
  toRemain: Array<ComponentDifferenceValueItem>;
}
