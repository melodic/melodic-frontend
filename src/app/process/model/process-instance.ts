import {ProcessState} from './process-state';

export class ProcessInstance {
  processId: string;
  applicationId: string;
  processState: ProcessState;
  finishDate: Date;
  startDate: Date;
  simulation: boolean;
}
