import {ComponentDifferenceValue} from './component-difference-value';

export class ComponentDifference {
  key: string;
  value: ComponentDifferenceValue;
}
