export enum VariableStatus {
  SUCCESS,
  ERROR,
  UNKNOWN,
  ACTIVE
}
