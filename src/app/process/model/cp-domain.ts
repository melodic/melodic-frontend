export class CpDomain {
  to?: number;
  from?: number;
  values?: Array<number>;
  type: string;
}
