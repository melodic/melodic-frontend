import {CpVariable} from './cp-variable';

export class CpVariableValue {
  variable: CpVariable;
  value: number;
}
