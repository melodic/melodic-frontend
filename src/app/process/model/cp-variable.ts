import {CpDomain} from './cp-domain';

export class CpVariable {
  variableType: string;
  id: string;
  componentId: string;
  domain: CpDomain;
}
