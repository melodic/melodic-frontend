export enum ProcessState {
  STARTED,
  FINISHED,
  UNKNOWN
}
