import {CpVariableValue} from './cp-variable-value';

export class CpSolution {
  variableValue: Array<CpVariableValue>;
  utilityValue: number;
  timestamp: number;
}
