import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CpSolutionViewComponent} from './cp-solution-view.component';

describe('CpSolutionViewComponent', () => {
  let component: CpSolutionViewComponent;
  let fixture: ComponentFixture<CpSolutionViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CpSolutionViewComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpSolutionViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
