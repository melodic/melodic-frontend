import {Component, OnInit, ViewChild} from '@angular/core';
import {ProcessService} from '../service/process.service';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {CpVariableValue} from '../model/cp-variable-value';
import {CpSolution} from '../model/cp-solution';

@Component({
  selector: 'app-cp-solution-view',
  templateUrl: './cp-solution-view.component.html',
  styleUrls: ['./cp-solution-view.component.css']
})
export class CpSolutionViewComponent implements OnInit {

  cpSolution: CpSolution;
  cpSolutionDownloaded = false;
  loadingDataInProgress = false;

  displayedColumns = ['variable', 'value'];
  dataSource: MatTableDataSource<CpVariableValue>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private processService: ProcessService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
  }

  downloadCpSolution() {
    this.loadingDataInProgress = true;
    this.processService.getCpSolution(localStorage.getItem('processId')).subscribe(value => {
        this.cpSolution = value;
        this.cpSolutionDownloaded = true;
        this.dataSource = new MatTableDataSource(this.cpSolution.variableValue);
        this.dataSource.sortingDataAccessor = (item, property) => {
          switch (property) {
            case 'variable':
              return item.variable.id;
            default:
              return item[property];
          }
        };
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.loadingDataInProgress = false;
      },
      error1 => {
        this.snackBar.open(`Problem by getting CP solution: ${error1.error.message}`, 'Close');
        this.loadingDataInProgress = false;
      }
    );
  }

}
