import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {CpVariable} from '../model/cp-variable';
import {CpDomain} from '../model/cp-domain';
import {ProcessService} from '../service/process.service';
import {ProcessHelperService} from '../service/process-helper.service';

@Component({
  selector: 'app-cp-variables-view',
  templateUrl: './cp-variables-view.component.html',
  styleUrls: ['./cp-variables-view.component.css']
})
export class CpVariablesViewComponent implements OnInit {

  cpDownloaded = false;
  loadingDataInProgress = false;

  displayedColumns = ['id', 'domainValues'];
  dataSource: MatTableDataSource<CpVariable>;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private processService: ProcessService,
              private processHelperService: ProcessHelperService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
  }

  downloadCp() {
    this.loadingDataInProgress = true;
    this.processService.getCpModel(localStorage.getItem('processId')).subscribe(value => {
        this.dataSource = new MatTableDataSource(value.variables);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.loadingDataInProgress = false;
        this.cpDownloaded = true;
      },
      error1 => {
        this.snackBar.open(`Problem by getting CP: ${error1.error.message}`, 'Close');
        this.loadingDataInProgress = false;
      });
  }

  getDomainValues(cpDomain: CpDomain): string {
    return this.processHelperService.getDomainValues(cpDomain);
  }

}
