import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CpVariablesViewComponent} from './cp-variables-view.component';

describe('CpVariablesViewComponent', () => {
  let component: CpVariablesViewComponent;
  let fixture: ComponentFixture<CpVariablesViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CpVariablesViewComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpVariablesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
