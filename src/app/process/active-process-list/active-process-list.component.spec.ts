import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ActiveProcessListComponent} from './active-process-list.component';

describe('ActiveProcessListComponent', () => {
  let component: ActiveProcessListComponent;
  let fixture: ComponentFixture<ActiveProcessListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActiveProcessListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveProcessListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
