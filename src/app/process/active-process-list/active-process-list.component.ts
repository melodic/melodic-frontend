import {Component, Inject, OnInit} from '@angular/core';
import {ProcessService} from '../service/process.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ProcessInstance} from '../model/process-instance';
import {Router} from '@angular/router';
import {ProcessHelperService} from '../service/process-helper.service';

@Component({
  selector: 'app-process-list',
  templateUrl: './active-process-list.component.html',
  styleUrls: ['./active-process-list.component.css']
})
export class ActiveProcessListComponent implements OnInit {

  existedProcesses: Array<ProcessInstance>;

  constructor(private router: Router,
              private processService: ProcessService,
              private processHelperService: ProcessHelperService,
              private dialogRef: MatDialogRef<ActiveProcessListComponent>,
              @Inject(MAT_DIALOG_DATA) public dialogData) {
  }

  ngOnInit() {
    this.existedProcesses = this.dialogData.startedProcesses;
    console.log('Started process:', this.existedProcesses);
  }

  onChooseProcessClick(chosenProcess: ProcessInstance) {
    console.log(`Chosen process id: ${chosenProcess.processId}`);
    this.dialogRef.close(chosenProcess);
  }

  getStartedProcesses(): Array<ProcessInstance> {
    return this.processHelperService.getStartedProcesses(this.existedProcesses);
  }
}
