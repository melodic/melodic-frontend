import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CamundaViewComponent} from '../camunda-view/camunda-view.component';
import {ProcessViewComponent} from '../process-view/process-view.component';
import {ProcessListComponent} from '../process-list/process-list.component';

const routes: Routes = [
  {path: '', component: ProcessViewComponent},

  {path: 'camunda', component: CamundaViewComponent},

  {path: 'list', component: ProcessListComponent},

  {path: ':id', component: ProcessViewComponent},

  {path: 'details', loadChildren: '../process-details/process-details.module#ProcessDetailsModule'},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProcessRoutingModule {
}
