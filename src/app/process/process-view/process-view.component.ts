import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProcessService} from '../service/process.service';
import {MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {ProcessVariables} from '../model/process-variables';
import {VariableStatus} from '../model/variable-status';
import {ProcessHelperService} from '../service/process-helper.service';
import {ProcessInstance} from '../model/process-instance';
import {ActiveProcessListComponent} from '../active-process-list/active-process-list.component';
import {CpSolutionViewComponent} from '../cp-solution-view/cp-solution-view.component';
import {CpVariablesViewComponent} from '../cp-variables-view/cp-variables-view.component';
import {DeploymentDifferenceComponent} from '../deployment-difference/deployment-difference.component';
import {AppConfigService} from '../../app-config/service/app-config.service';

@Component({
  selector: 'app-process-view',
  templateUrl: './process-view.component.html',
  styleUrls: ['../../app.component.css', './process-view.component.css']
})
export class ProcessViewComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(CpSolutionViewComponent) cpSolutionViewComponent: CpSolutionViewComponent;
  @ViewChild(CpVariablesViewComponent) cpVariablesViewComponent: CpVariablesViewComponent;
  @ViewChild(DeploymentDifferenceComponent) deploymentDifferenceComponent: DeploymentDifferenceComponent;

  currentProcessVariables: ProcessVariables;
  deploymentCompleted = false;
  stopMonitoring = false;
  errorInVariable = false;
  existingProcess: boolean;
  processListDialog: MatDialogRef<ActiveProcessListComponent>;
  totalNumberOffers = 0;
  initialCheckingFetchingOffersState = true;
  narrowWindowOnLastCheck = false;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private processService: ProcessService,
              private processHelperService: ProcessHelperService,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    localStorage.setItem('viewTitle', 'Process');
    this.narrowWindowOnLastCheck = this.isNarrowWindow();
    const processId = this.route.snapshot.paramMap.get('id');
    if (processId !== null) {
      localStorage.setItem('processId', processId);
    } else {
      localStorage.removeItem('processId');
      // find proper process
      this.processService.getProcessesList().subscribe(value => {
          console.log('Existed processes: ', value);
          if (value.length === 0) {
            console.log(`Any process doesn't exist`);
            this.existingProcess = false;
          } else {
            this.redirectToProperProcessView(value);
          }
        },
        error1 => {
          this.snackBar.open(`Problem by getting processes list: ${error1.error.message}`, 'Close');
        });
    }
    console.log(`Monitoring for process with id: ${processId}`);
  }

  ngAfterViewInit(): void {
    if (this.isMonitoringProcess()) {
      this.monitorProcess();
    }
  }

  ngOnDestroy(): void {
    this.stopMonitoring = true;
    console.log('Component Process-view destroyed');
  }

  isNarrowWindow() {
    const windowWidth = window.innerWidth;
    if (windowWidth < 1350 && !this.narrowWindowOnLastCheck) {
      this.narrowWindowOnLastCheck = true;
      if (this.deploymentCompleted) {
        console.log('First time with narrow - get data');
        this.ngAfterViewInit();
      }
      return true;
    } else if (windowWidth < 1350 && this.narrowWindowOnLastCheck) {
      return true;
    } else if (windowWidth >= 1350 && this.narrowWindowOnLastCheck) {
      if (this.deploymentCompleted) {
        console.log('First time with wide - get data');
        this.ngAfterViewInit();
      }
      this.narrowWindowOnLastCheck = false;
      return false;
    } else {
      return false;
    }
  }

  onAdvancedViewClick() {
    this.router.navigate(['/process/camunda']);
  }

  onComponentsLogsClick() {
    console.log(`Click for Kibana redirection`);
    window.open(AppConfigService.settings.kibanaUrl);
  }

  onProcessesHistoryClick() {
    console.log('History of processes click');
    this.router.navigate(['/process/list']);
  }

  onFetchingOffersClick() {
    console.log('Fetching offers click');
    this.router.navigate(['/process/details/offer']);
  }

  onGeneratingCPClick() {
    console.log('Generating CP click');
    this.router.navigate(['/process/details/cp']);
  }

  onReasoningClick() {
    console.log('Reasoning click');
    this.router.navigate(['/process/details/cp']);
  }

  onDeployingClick() {
    console.log('Deploying click');
    this.router.navigate(['/process/details/deployment']);
  }

  monitorProcess() {
    this.processService.getInformationAboutProcess(localStorage.getItem('processId'))
      .subscribe(value => {
          console.log(`Info about process`, value);
          this.currentProcessVariables = value;
          if (this.currentProcessVariables.simulation) {
            localStorage.setItem('viewTitle', `Process for '${value.applicationId}' in simulation mode`);
          } else {
            localStorage.setItem('viewTitle', `Process for '${value.applicationId}'`);
          }
          this.checkErrorsExistence();
          this.updateNumberOfOffers();
          this.updateVariablesView();
          this.updateSolutionView();
          if (this.currentProcessVariables.simulation) {
            this.updateDeploymentDifferenceViewAfterCreatingSolution();
            this.updateDeploymentDifferenceViewAfterFinishedDeployment();
          }
          if (!this.errorInVariable && !this.stopMonitoring) {
            if (value.processState.toString() === VariableStatus[VariableStatus.SUCCESS]) {
              console.log(`Process finished`);
              this.deploymentCompleted = true;
              this.onDeploymentFinish();
            } else {
              console.log(`Next request`);
              // wait before next request
              this.sleep(10000).then(() => this.monitorProcess());
            }
          }
        },
        error1 => {
          this.updateNumberOfOffers();
          if (error1.status === 0) {
            this.snackBar.open(`Problem by getting information about process: ${error1.error.message}.`,
              'Close', {duration: 10000});
          } else {
            console.log(`Problem with variable: ${error1.error.variableName}`);
            this.errorInVariable = true;
            this.removeActiveStatus();
            this.snackBar.open(`Problem by getting information about process: ${error1.error.message}`, 'Close');
          }
        }
      );
  }

  private removeActiveStatus() {
    if (this.currentProcessVariables) {
      if (this.currentProcessVariables.discoveryServiceResult.toString() === VariableStatus[VariableStatus.ACTIVE]) {
        this.currentProcessVariables.discoveryServiceResult = VariableStatus.UNKNOWN;
      } else if (this.currentProcessVariables.cpCreationResultCode.toString() === VariableStatus[VariableStatus.ACTIVE]) {
        this.currentProcessVariables.cpCreationResultCode = VariableStatus.UNKNOWN;
      } else if (this.currentProcessVariables.cpSolutionResultCode.toString() === VariableStatus[VariableStatus.ACTIVE]) {
        this.currentProcessVariables.cpSolutionResultCode = VariableStatus.UNKNOWN;
      } else if (this.currentProcessVariables.applicationDeploymentResultCode.toString() === VariableStatus[VariableStatus.ACTIVE]) {
        this.currentProcessVariables.applicationDeploymentResultCode = VariableStatus.UNKNOWN;
      }
    }
  }

  async sleep(periodInMs: number) {
    await new Promise(resolve => setTimeout(resolve, periodInMs));
  }

  getFetchingOffersStatus() {
    return this.currentProcessVariables ? this.currentProcessVariables.discoveryServiceResult : VariableStatus.UNKNOWN;
  }

  isFetchingOffersCompleted() {
    return this.currentProcessVariables &&
      this.currentProcessVariables.discoveryServiceResult.toString() === VariableStatus[VariableStatus.SUCCESS];
  }

  getGeneratingConstraintProblemStatus() {
    return this.currentProcessVariables ? this.currentProcessVariables.cpCreationResultCode : VariableStatus.UNKNOWN;
  }

  isGeneratingConstraintProblemCompleted() {
    return this.currentProcessVariables &&
      this.currentProcessVariables.cpCreationResultCode.toString() === VariableStatus[VariableStatus.SUCCESS];
  }

  getReasoningStatus() {
    return this.currentProcessVariables ? this.currentProcessVariables.cpSolutionResultCode : VariableStatus.UNKNOWN;
  }

  isReasoningCompleted() {
    return this.currentProcessVariables &&
      this.currentProcessVariables.cpSolutionResultCode.toString() === VariableStatus[VariableStatus.SUCCESS];
  }

  getDeployingStatus() {
    return this.currentProcessVariables ? this.currentProcessVariables.applicationDeploymentResultCode : VariableStatus.UNKNOWN;
  }

  isDeployingCompleted() {
    return this.currentProcessVariables &&
      this.currentProcessVariables.applicationDeploymentResultCode.toString() === VariableStatus[VariableStatus.SUCCESS];
  }

  getProcessFinishedStatus() {
    return this.currentProcessVariables ? this.currentProcessVariables.processState : VariableStatus.UNKNOWN;
  }

  isProcessFinished() {
    return this.currentProcessVariables && this.currentProcessVariables.processState.toString() === VariableStatus[VariableStatus.SUCCESS];
  }

  onDeploymentFinish() {
    this.snackBar.open(`Your application successfully started`, 'Close');
  }

  getClassForVariable(elementStatus) {
    if (elementStatus === VariableStatus[VariableStatus.ACTIVE]) {
      return 'process-element-color-accent';
    } else if (elementStatus === VariableStatus[VariableStatus.ERROR]) {
      return 'process-element-color-warn';
    } else if (elementStatus === VariableStatus[VariableStatus.SUCCESS]) {
      return 'process-element-color-primary';
    } else { // future task
      return 'process-element-color-grey';
    }
  }

  getIconNameForVariable(elementStatus) {
    if (elementStatus === VariableStatus[VariableStatus.ACTIVE]) {
      return 'loop';
    } else if (elementStatus === VariableStatus[VariableStatus.ERROR]) {
      return 'block';
    } else if (elementStatus === VariableStatus[VariableStatus.SUCCESS]) {
      return 'done_outline';
    } else { // future task
      return '';
    }
  }

  private checkErrorsExistence() {
    this.checkErrorInVariable(this.currentProcessVariables.discoveryServiceResult, 'Fetching offers');
    this.checkErrorInVariable(this.currentProcessVariables.cpCreationResultCode, 'Generating constraint problem');
    this.checkErrorInVariable(this.currentProcessVariables.cpSolutionResultCode, 'Reasoning');
    this.checkErrorInVariable(this.currentProcessVariables.applicationDeploymentResultCode, 'Deploying');
  }

  private checkErrorInVariable(variableStatus: VariableStatus, variableViewName: string) {
    if (variableStatus.toString() === VariableStatus[VariableStatus.ERROR]) {
      this.errorInVariable = true;
      console.log(`${variableViewName} in error state`);
      this.snackBar.open(`Error by ${variableViewName}`, 'Close', {duration: 10000});
    }
  }

  isMonitoringProcess(): boolean {
    if (!localStorage.getItem('processId')) {
      console.log(`Any process isn't chosen`);
      return false;
    } else {
      return true;
    }
  }

  isReconfigurationProcess(): boolean {
    if (!this.currentProcessVariables) {
      return false;
    } else {
      return this.currentProcessVariables.reconfigurationProcess;
    }
  }

  getNumberOfColsForWideWindow(): number {
    if (this.isReconfigurationProcess()) {
      return 3;
    } else {
      return 5;
    }
  }

  getRowHeightForWideWindow() {
    if (this.isReconfigurationProcess()) {
      return '1.5:1';
    } else {
      return '0.8:1';
    }
  }

  getRowHeightForNarrowWindow() {
    return '0.8:1';
  }

  redirectToProperProcessView(existedProcesses: Array<ProcessInstance>) {
    const startedProcesses = this.processHelperService.getStartedProcesses(existedProcesses);
    const finishedProcesses = this.processHelperService.getFinishedProcesses(existedProcesses);

    finishedProcesses.forEach(value => console.log(`Process in FINISHED state for application: ${value.applicationId}`));
    startedProcesses.forEach(value => console.log(`Process in STARTED state for application: ${value.applicationId}`));

    if (startedProcesses.length === 1) {
      this.router.navigate([`/process/${startedProcesses[0].processId}`]);
    } else if (startedProcesses.length > 1) {
      this.openProcessesListDialog(existedProcesses);
      console.log('Show active processes list');
    } else {
      this.processHelperService.redirectToProperFinishedProcess(finishedProcesses);
    }
  }

  private openProcessesListDialog(existedProcesses: Array<ProcessInstance>) {
    this.processListDialog = this.dialog.open(ActiveProcessListComponent, {
      hasBackdrop: false,
      data: {
        startedProcesses: existedProcesses
      },
      width: '30%'
    });

    this.processListDialog.afterClosed().subscribe(result => {
      console.log(`Chosen process id: ${result.processId}`);
      this.router.navigate([`/process/${result.processId}`]);
    });
  }

  private updateNumberOfOffers() {
    if (!this.isReconfigurationProcess() && (!this.isFetchingOffersCompleted() || this.initialCheckingFetchingOffersState)) {
      this.processService.getTotalOffersNumber().subscribe(value => {
          this.totalNumberOffers = value;
        },
        error1 => {
          this.snackBar.open(`Error by checking current total number of offers: ${error1.error.message}`, 'Close', {duration: 10000});
        });
    }
    this.initialCheckingFetchingOffersState = false;
  }

  private updateSolutionView() {
    if (!this.cpSolutionViewComponent.cpSolutionDownloaded && this.isReasoningCompleted()
      && this.cpSolutionViewComponent.loadingDataInProgress === false) {
      this.cpSolutionViewComponent.downloadCpSolution();
    }
  }

  private updateVariablesView() {
    if (!this.isReconfigurationProcess() && (!this.cpVariablesViewComponent.cpDownloaded && this.isGeneratingConstraintProblemCompleted()
      && this.cpVariablesViewComponent.loadingDataInProgress === false)) {
      this.cpVariablesViewComponent.downloadCp();
    }
  }

  private updateDeploymentDifferenceViewAfterCreatingSolution() {
    if (!this.deploymentDifferenceComponent.differenceAfterSolutionDownloaded && this.isReasoningCompleted()
      && this.deploymentDifferenceComponent.loadingDataInProgress === false) {
      this.deploymentDifferenceComponent.downloadDifference();
    }
  }

  private updateDeploymentDifferenceViewAfterFinishedDeployment() {
    if (!this.deploymentDifferenceComponent.differenceAfterDeploymentDownloaded && this.isDeployingCompleted()
      && this.deploymentDifferenceComponent.loadingDataInProgress === false) {
      this.deploymentDifferenceComponent.downloadDifference();
      this.deploymentDifferenceComponent.differenceAfterDeploymentDownloaded = true;
    }
  }
}
