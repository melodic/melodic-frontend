import {TestBed} from '@angular/core/testing';

import {ProcessHelperService} from './process-helper.service';

describe('ProcessHelperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProcessHelperService = TestBed.get(ProcessHelperService);
    expect(service).toBeTruthy();
  });
});
