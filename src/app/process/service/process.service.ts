import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {ProcessVariables} from '../model/process-variables';
import {AppConfigService} from '../../app-config/service/app-config.service';
import {ProcessInstance} from '../model/process-instance';
import {CpModel} from '../model/cp-model';
import {CpSolution} from '../model/cp-solution';
import {DeploymentDifference} from '../model/deployment-difference';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ProcessService {

  apiUrl = `${AppConfigService.settings.apiUrl}/auth/process`;

  constructor(private http: HttpClient) {
  }

  getInformationAboutProcess(processId: string): Observable<ProcessVariables> {
    const requestUrl = `${this.apiUrl}/${processId}`;
    return this.http.get(requestUrl, httpOptions).pipe(
      tap((response: ProcessVariables) => console.log(`Response from process with id: ${processId}`, response),
        e => console.log(`Error by getting information about process with id ${processId}: `, e))
    );
  }

  getProcessesList(): Observable<Array<ProcessInstance>> {
    return this.http.get(this.apiUrl, httpOptions).pipe(
      tap((response: Array<ProcessInstance>) => console.log(`Response with processes list: `, response),
        e => console.log(`Error by getting processes list: `, e))
    );
  }

  getTotalOffersNumber(): Observable<number> {
    return this.http.get(`${this.apiUrl}/offer/total`, httpOptions).pipe(
      tap((response: number) => console.log(`Total number of offers: ${response}`),
        e => console.log(`Error by getting total number of offers`, e))
    );
  }

  getCpModel(processId: string): Observable<CpModel> {
    return this.http.get(`${this.apiUrl}/cp/model/${processId}`, httpOptions).pipe(
      tap((response: CpModel) => console.log(`Get CP model with id: ${response.id}`),
        e => console.log(`Error by getting CP model`, e))
    );
  }

  getCpSolution(processId: string): Observable<CpSolution> {
    return this.http.get(`${this.apiUrl}/cp/solution/${processId}`, httpOptions).pipe(
      tap((response: CpSolution) => console.log(`Get CP solution for process with id: ${processId}`),
        e => console.log(`Error by getting CP solution`, e))
    );
  }

  getDeploymentDifference(processId: string): Observable<DeploymentDifference> {
    return this.http.get(`${this.apiUrl}/deployment/difference/${processId}`, httpOptions).pipe(
      tap((response: DeploymentDifference) => console.log(`Get deployment difference for process with id: ${processId}`),
        e => console.log(`Error by getting deployment difference:`, e))
    );
  }
}
