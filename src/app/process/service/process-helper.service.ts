import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {ProcessInstance} from '../model/process-instance';
import {ProcessState} from '../model/process-state';
import {CpDomain} from '../model/cp-domain';

@Injectable({
  providedIn: 'root'
})
export class ProcessHelperService {

  constructor(private router: Router) {
  }

  redirectToProperFinishedProcess(finishedProcesses: Array<ProcessInstance>) {
    if (finishedProcesses.length === 1) {
      console.log(`Navigate to 1 finished process: ${finishedProcesses[0].processId}`);
      this.router.navigate([`/process/${finishedProcesses[0].processId}`]);
    } else {
      console.log('Find latest process from list');
      const lastFinishedProcessId = this.findLatestProcessId(finishedProcesses);
      console.log(`Last finished process id: ${lastFinishedProcessId}`);
      this.router.navigate([`/process/${lastFinishedProcessId}`]);
    }
  }

  getStartedProcesses(existedProcesses: Array<ProcessInstance>): Array<ProcessInstance> {
    if (existedProcesses) {
      return existedProcesses
        .filter(value => value.processState.toString() === ProcessState[ProcessState.STARTED])
        .sort((a, b) => new Date(b.startDate).getTime() - new Date(a.startDate).getTime());
    } else {
      return [];
    }
  }

  getFinishedProcesses(existedProcesses: Array<ProcessInstance>): Array<ProcessInstance> {
    if (existedProcesses) {
      return existedProcesses
        .filter(value => value.processState.toString() === ProcessState[ProcessState.FINISHED])
        .sort((a, b) => new Date(b.finishDate).getTime() - new Date(a.finishDate).getTime());
    } else {
      return [];
    }
  }

  getDomainValues(cpDomain: CpDomain): string {
    if (cpDomain.from && cpDomain.to) {
      return `from ${cpDomain.from} to ${cpDomain.to}`;
    } else {
      let result = '{';
      cpDomain.values.forEach(value => {
        result += value;
        result += ',';
      });
      result = result.substring(0, result.length - 1); // remove last ',' sign
      result += '}';
      return result;
    }
  }

  private findLatestProcessId(finishedProcesses: Array<ProcessInstance>): string {
    return finishedProcesses
      .sort((a, b) => new Date(b.finishDate).getTime() - new Date(a.finishDate).getTime())[0].processId;
  }
}
