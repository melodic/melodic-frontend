import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProcessRoutingModule} from './route/process-routing.module';
import {AngularMaterialModule} from '../angular-material/angular-material.module';
import {CamundaViewComponent} from './camunda-view/camunda-view.component';
import {ProcessViewComponent} from './process-view/process-view.component';
import {ActiveProcessListComponent} from './active-process-list/active-process-list.component';
import {ProcessListComponent} from './process-list/process-list.component';
import {CpSolutionViewComponent} from './cp-solution-view/cp-solution-view.component';
import {CpVariablesViewComponent} from './cp-variables-view/cp-variables-view.component';
import {DeploymentDifferenceComponent} from './deployment-difference/deployment-difference.component';

@NgModule({
  declarations: [
    CamundaViewComponent,
    ProcessViewComponent,
    ActiveProcessListComponent,
    ProcessListComponent,
    CpSolutionViewComponent,
    CpVariablesViewComponent,
    DeploymentDifferenceComponent
  ],
  imports: [
    CommonModule,
    ProcessRoutingModule,
    AngularMaterialModule
  ],
  exports: [
    ActiveProcessListComponent
  ],
  entryComponents: [
    ActiveProcessListComponent
  ]
})
export class ProcessModule {
}
