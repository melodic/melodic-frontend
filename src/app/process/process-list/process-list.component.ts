import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ProcessService} from '../service/process.service';
import {MatSnackBar} from '@angular/material';
import {ProcessInstance} from '../model/process-instance';
import {ProcessHelperService} from '../service/process-helper.service';

@Component({
  selector: 'app-process-list',
  templateUrl: './process-list.component.html',
  styleUrls: ['./process-list.component.css']
})
export class ProcessListComponent implements OnInit {

  existedProcesses: Array<ProcessInstance>;

  constructor(private router: Router,
              private processService: ProcessService,
              private processHelperService: ProcessHelperService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    localStorage.setItem('viewTitle', 'Processes list');
    this.processService.getProcessesList().subscribe(value => {
        console.log(`GET all processes list`);
        this.existedProcesses = value;
      },
      error1 => {
        console.log(`Error by getting processes list`, error1);
        const errorMessage = error1.error.message ? `: ${error1.error.message}` : '';
        this.snackBar.open(`Problem by getting processes list${errorMessage}`, 'Close');
      });
  }

  getStartedProcesses(): Array<ProcessInstance> {
    return this.existedProcesses ? this.processHelperService.getStartedProcesses(this.existedProcesses) : [];
  }

  getFinishedProcesses(): Array<ProcessInstance> {
    return this.existedProcesses ? this.processHelperService.getFinishedProcesses(this.existedProcesses) : [];
  }

  onChooseProcessClick(chosenProcess: ProcessInstance) {
    console.log(`Chosen process id: ${chosenProcess.processId}`);
    this.router.navigate([`/process/${chosenProcess.processId}`]);
  }
}
