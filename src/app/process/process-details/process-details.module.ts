import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AngularMaterialModule} from '../../angular-material/angular-material.module';
import {ProcessDetailsRoutingModule} from './route/process-details-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProcessDetailsRoutingModule,
    AngularMaterialModule
  ]
})
export class ProcessDetailsModule {
}
