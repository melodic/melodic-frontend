import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: 'offer', loadChildren: '../offer/offer.module#OfferModule'},

  {path: 'cp', loadChildren: '../cp/cp.module#CpModule'},

  {path: 'deployment', loadChildren: '../deployment/deployment.module#DeploymentModule'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProcessDetailsRoutingModule {
}
