import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {OfferMainComponent} from '../offer-main/offer-main.component';

const routes: Routes = [
  {path: '', component: OfferMainComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OfferRoutingModule {
}
