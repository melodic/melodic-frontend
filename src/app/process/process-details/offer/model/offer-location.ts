import {GeoLocation} from './geo-location';

export class OfferLocation {
  id: string;
  name: string;
  providerId: string;
  locationScope: string;
  isAssignable: boolean;
  geoLocation: GeoLocation;
  parent: OfferLocation;
  state: string;
  owner: string;
}
