export class OperatingSystem {
  id: number;
  operatingSystemFamily: string;
  operatingSystemArchitecture: string;
  operatingSystemVersion: number;
}
