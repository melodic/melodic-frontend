export class GeoLocation {
  id: number;
  city: string;
  country: string;
  latitude: number;
  longitude: number;
}
