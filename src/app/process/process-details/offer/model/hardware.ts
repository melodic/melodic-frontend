export class Hardware {
  id: string;
  name: string;
  providerId: string;
  cores: number;
  ram: number;
  disk: number;
  location: any;
  state: string;
  owner: string;
}
