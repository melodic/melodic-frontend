import {CloudType} from '../../../../provider/model/cloud-type';
import {Api} from '../../../../provider/model/api';
import {Credential} from '../../../../provider/model/credential';
import {CloudConfiguration} from '../../../../provider/model/cloud-configuration';

export class Cloud {
  endpoint: string;
  cloudType: CloudType;
  api: Api;
  credential: Credential;
  cloudConfiguration: CloudConfiguration;
  id: string;
  owner: string;
  state: string;
  diagnostic: string;
}
