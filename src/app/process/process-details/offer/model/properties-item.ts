export class PropertiesItem {
  key: string;
  value: string;
  children?: PropertiesItem[];

  constructor(key: string, value: string, children?: PropertiesItem[]) {
    this.key = key;
    this.value = value;
    this.children = children ? children : null;
  }
}
