import {OperatingSystem} from './operating-system';
import {OfferLocation} from './offer-location';

export class Image {
  id: string;
  name: string;
  providerId: string;
  operatingSystem: OperatingSystem;
  location: OfferLocation;
  state: string;
  owner: string;
}
