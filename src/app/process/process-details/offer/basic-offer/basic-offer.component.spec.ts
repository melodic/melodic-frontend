import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BasicOfferComponent} from './basic-offer.component';

describe('BasicOfferComponent', () => {
  let component: BasicOfferComponent;
  let fixture: ComponentFixture<BasicOfferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BasicOfferComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
