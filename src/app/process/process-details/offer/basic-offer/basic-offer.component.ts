import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {ProcessOfferService} from '../service/process-offer.service';
import {AppInjector} from '../../../../injector/app-injector';

@Component({
  selector: 'app-basic-offer',
  templateUrl: './basic-offer.component.html',
  styleUrls: ['./basic-offer.component.css']
})
export class BasicOfferComponent implements OnInit {

  protected processOfferService: ProcessOfferService;
  protected snackBar: MatSnackBar;

  dataList: Array<any>;
  loadingDataInProgress = true;

  displayedColumns = [];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor() {
    const injector = AppInjector.getInjector();
    this.processOfferService = injector.get(ProcessOfferService);
    this.snackBar = injector.get(MatSnackBar);
  }

  ngOnInit() {
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  updateTableData(responseValue: any) {
    this.loadingDataInProgress = false;
    this.dataList = responseValue;
    this.dataSource = new MatTableDataSource(this.dataList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}
