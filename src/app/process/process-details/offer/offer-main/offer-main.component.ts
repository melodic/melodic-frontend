import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-offers-main',
  templateUrl: './offer-main.component.html',
  styleUrls: ['./offer-main.component.css']
})
export class OfferMainComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
    localStorage.setItem('viewTitle', 'Current offers');
  }

}
