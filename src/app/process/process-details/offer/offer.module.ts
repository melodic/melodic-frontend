import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AngularMaterialModule} from '../../../angular-material/angular-material.module';
import {OfferRoutingModule} from './route/offer-routing.module';
import {HardwareListComponent} from './hardware-list/hardware-list.component';
import {LocationListComponent} from './location-list/location-list.component';
import {ImageListComponent} from './image-list/image-list.component';
import {CloudListComponent} from './cloud-list/cloud-list.component';
import {BasicOfferComponent} from './basic-offer/basic-offer.component';
import {OfferMainComponent} from './offer-main/offer-main.component';

@NgModule({
  declarations: [
    OfferMainComponent,
    HardwareListComponent,
    LocationListComponent,
    ImageListComponent,
    CloudListComponent,
    BasicOfferComponent,
  ],
  imports: [
    CommonModule,
    OfferRoutingModule,
    AngularMaterialModule
  ]
})
export class OfferModule {
}
