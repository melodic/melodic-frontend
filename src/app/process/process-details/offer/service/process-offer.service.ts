import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AppConfigService} from '../../../../app-config/service/app-config.service';
import {Observable} from 'rxjs';
import {Hardware} from '../model/hardware';
import {tap} from 'rxjs/operators';
import {OfferLocation} from '../model/offer-location';
import {Image} from '../model/image';
import {Cloud} from '../model/cloud';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ProcessOfferService {

  apiUrl = `${AppConfigService.settings.apiUrl}/auth/process/offer`;

  constructor(private http: HttpClient) {
  }

  getHardwareList(): Observable<Array<Hardware>> {
    return this.http.get(`${this.apiUrl}/hardware`, httpOptions).pipe(
      tap((response: Array<Hardware>) => console.log(`Number of hardware element: ${response.length}`),
        e => console.log(`Error by getting hardware list: `, e))
    );
  }

  getLocationList(): Observable<Array<OfferLocation>> {
    return this.http.get(`${this.apiUrl}/location`, httpOptions).pipe(
      tap((response: Array<OfferLocation>) => console.log(`Number of location element: ${response.length}`),
        e => console.log(`Error by getting locations list: `, e))
    );
  }

  getImageList(): Observable<Array<Image>> {
    return this.http.get(`${this.apiUrl}/image`, httpOptions).pipe(
      tap((response: Array<Image>) => console.log(`Number of image element: ${response.length}`),
        e => console.log(`Error by getting image list: `, e))
    );
  }

  getCloudList(): Observable<Array<Cloud>> {
    return this.http.get(`${this.apiUrl}/cloud`, httpOptions).pipe(
      tap((response: Array<Cloud>) => console.log(`Number of cloud element: ${response.length}`),
        e => console.log(`Error by getting cloud list: `, e))
    );
  }
}
