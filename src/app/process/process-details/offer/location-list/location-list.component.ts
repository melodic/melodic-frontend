import {Component, OnInit} from '@angular/core';
import {OfferLocation} from '../model/offer-location';
import {MatTableDataSource} from '@angular/material';
import {GeoLocation} from '../model/geo-location';
import {NestedTreeControl} from '@angular/cdk/tree';
import {BasicOfferComponent} from '../basic-offer/basic-offer.component';

@Component({
  selector: 'app-location-list',
  templateUrl: './location-list.component.html',
  styleUrls: ['./location-list.component.css']
})
export class LocationListComponent extends BasicOfferComponent implements OnInit {

  displayedColumns = ['position', 'name', 'id', 'providerId', 'locationScope', 'isAssignable', 'country', 'city', 'parent', 'state',
    'owner'];
  dataSource: MatTableDataSource<OfferLocation>;
  treeControl = new NestedTreeControl<OfferLocation>(node => new Array(node.parent));

  constructor() {
    super();
  }

  ngOnInit() {
    this.processOfferService.getLocationList().subscribe(value => {
        this.updateTableData(value);
        console.log('Location list successfully loaded');
      },
      error1 => {
        this.loadingDataInProgress = false;
        this.snackBar.open(`Problem by getting locations list: ${error1.error.message}`, 'Close');
      });
  }

  updateTableData(offerLocations: Array<OfferLocation>) {
    this.loadingDataInProgress = false;
    this.dataList = offerLocations;
    this.dataSource = new MatTableDataSource(this.dataList);

    this.dataSource.filterPredicate = (data, filter) => {
      return this.getOfferLocationForFilteringAsString(data).indexOf(filter) !== -1;
    };

    this.dataSource.sortingDataAccessor = this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'country':
          return this.getCountryForGeoLocation(item.geoLocation);
        case 'city':
          return this.getCityForGeoLocation(item.geoLocation);
        default:
          return item[property];
      }
    };

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getCountryForGeoLocation(geoLocation: GeoLocation) {
    return geoLocation !== null ? geoLocation.country : '-';
  }

  getCityForGeoLocation(geoLocation: GeoLocation) {
    return geoLocation !== null ? geoLocation.city : '-';
  }

  createTreeDataSource(parent: OfferLocation) {
    return parent !== null ? new Array(parent) : [];
  }

  getOfferLocationForFilteringAsString(offerLocation: OfferLocation): string {
    return offerLocation.name + offerLocation.id + offerLocation.providerId + offerLocation.locationScope + offerLocation.isAssignable +
      this.getCountryForGeoLocation(offerLocation.geoLocation) + this.getCityForGeoLocation(offerLocation.geoLocation) + offerLocation.state
      + offerLocation.owner;
  }

  hasChild = (_: number, location: OfferLocation) => location.parent !== null;

}
