import {Component, OnInit} from '@angular/core';
import {Hardware} from '../model/hardware';
import {MatTableDataSource} from '@angular/material';
import {BasicOfferComponent} from '../basic-offer/basic-offer.component';

@Component({
  selector: 'app-hardware-list',
  templateUrl: './hardware-list.component.html',
  styleUrls: ['./hardware-list.component.css']
})
export class HardwareListComponent extends BasicOfferComponent implements OnInit {

  displayedColumns = ['position', 'name', 'id', 'cores', 'ram', 'disk', 'state', 'owner'];
  dataSource: MatTableDataSource<Hardware>;

  constructor() {
    super();
  }

  ngOnInit() {
    this.processOfferService.getHardwareList().subscribe(value => {
        this.updateTableData(value);
        console.log('Hardware list successfully loaded');
      },
      error1 => {
        this.loadingDataInProgress = false;
        this.snackBar.open(`Problem by getting hardware list: ${error1.error.message}`, 'Close');
      });
  }
}
