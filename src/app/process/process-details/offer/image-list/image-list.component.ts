import {Component, OnInit} from '@angular/core';
import {Image} from '../model/image';
import {MatTableDataSource} from '@angular/material';
import {BasicOfferComponent} from '../basic-offer/basic-offer.component';

@Component({
  selector: 'app-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.css']
})
export class ImageListComponent extends BasicOfferComponent implements OnInit {

  displayedColumns = ['position', 'name', 'amiId', 'OSFamily', 'OSArchitecture', 'OSVersion', 'location', 'state', 'owner'];
  dataSource: MatTableDataSource<Image>;

  constructor() {
    super();
  }

  ngOnInit() {
    this.processOfferService.getImageList().subscribe(value => {
        this.updateTableData(value);
        console.log('Image list successfully loaded');
      },
      error1 => {
        this.loadingDataInProgress = false;
        this.snackBar.open(`Problem by getting image list: ${error1.error.message}`, 'Close');
      });
  }

  updateTableData(imagesList: Array<Image>) {
    this.loadingDataInProgress = false;
    this.dataList = imagesList;
    this.dataSource = new MatTableDataSource(this.dataList);

    this.dataSource.filterPredicate = (data, filter) => {
      return this.getImageForFilteringAsString(data).indexOf(filter) !== -1;
    };

    this.dataSource.sortingDataAccessor = this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'amiId':
          return item.providerId;
        case 'OSFamily':
          return item.operatingSystem.operatingSystemFamily;
        case 'OSArchitecture':
          return item.operatingSystem.operatingSystemArchitecture;
        case 'OSVersion':
          return item.operatingSystem.operatingSystemVersion;
        case 'location':
          return item.location.name;
        default:
          return item[property];
      }
    };

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  private getImageForFilteringAsString(image: Image): string {
    return image.name + image.providerId + image.operatingSystem.operatingSystemFamily + image.operatingSystem.operatingSystemArchitecture
      + image.operatingSystem.operatingSystemVersion + image.location.name + image.state + image.owner;
  }
}
