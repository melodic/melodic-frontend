import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {Cloud} from '../model/cloud';
import {PropertiesItem} from '../model/properties-item';
import {BasicOfferComponent} from '../basic-offer/basic-offer.component';

@Component({
  selector: 'app-cloud-list',
  templateUrl: './cloud-list.component.html',
  styleUrls: ['./cloud-list.component.css']
})
export class CloudListComponent extends BasicOfferComponent implements OnInit {

  panelOpenState = false;

  displayedColumns = ['position', 'id', 'providerName', 'cloudType', 'endpoint', 'nodeGroup', 'state', 'diagnostic', 'owner', 'properties'];
  dataSource: MatTableDataSource<Cloud>;

  constructor() {
    super();
  }

  ngOnInit() {
    this.processOfferService.getCloudList().subscribe(value => {
        this.updateTableData(value);
        console.log('Cloud list successfully loaded');
      },
      error1 => {
        this.loadingDataInProgress = false;
        this.snackBar.open(`Problem by getting cloud list: ${error1.error.message}`, 'Close');
      });
  }

  updateTableData(clouds: Array<Cloud>) {
    this.loadingDataInProgress = false;
    this.dataList = clouds;
    this.dataSource = new MatTableDataSource(this.dataList);

    this.dataSource.filterPredicate = (data, filter) => {
      return this.getCloudForFilteringAsString(data).indexOf(filter) !== -1;
    };

    this.dataSource.sortingDataAccessor = this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'providerName':
          return item.api.providerName;
        case 'endpoint':
          return this.getEndpointName(item.endpoint);
        case 'nodeGroup':
          return item.cloudConfiguration.nodeGroup;
        default:
          return item[property];
      }
    };

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getEndpointName(endpoint) {
    return endpoint ? endpoint : '-';
  }

  createPropertiesListFromMap(properties: {}): Array<PropertiesItem> {
    const propertiesList = new Array<PropertiesItem>();
    for (const propertiesKey in properties) {
      if (properties.hasOwnProperty(propertiesKey)) {
        propertiesList.push(new PropertiesItem(propertiesKey, properties[propertiesKey], null));
      }
    }
    return propertiesList;
  }

  getNumberOfProperties(properties: {}): number {
    let numberOfProperties = 0;
    for (const propertiesKey in properties) {
      if (properties.hasOwnProperty(propertiesKey)) {
        numberOfProperties++;
      }
    }
    return numberOfProperties;
  }

  private getCloudForFilteringAsString(cloud: Cloud): string {
    return cloud.id + cloud.api.providerName + cloud.cloudType + cloud.endpoint + cloud.cloudConfiguration.nodeGroup
      + cloud.state + cloud.owner;
  }
}
