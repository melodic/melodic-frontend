import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CpDetailsComponent} from '../cp-details/cp-details.component';

const routes: Routes = [
  {path: '', component: CpDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CpRoutingModule {
}
