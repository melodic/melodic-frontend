import {Component, OnInit, ViewChild} from '@angular/core';
import {ProcessService} from '../../../service/process.service';
import {CpModel} from '../../../model/cp-model';
import {MatSnackBar} from '@angular/material';
import {CpVariablesListComponent} from '../cp-variables-list/cp-variables-list.component';
import {CpMetricsListComponent} from '../cp-metrics-list/cp-metrics-list.component';
import {CpConstantsListComponent} from '../cp-constants-list/cp-constants-list.component';

@Component({
  selector: 'app-cp-details',
  templateUrl: './cp-details.component.html',
  styleUrls: ['./cp-details.component.css']
})
export class CpDetailsComponent implements OnInit {

  @ViewChild(CpVariablesListComponent) cpVariablesListComponent: CpVariablesListComponent;
  @ViewChild(CpMetricsListComponent) cpMetricsListComponent: CpMetricsListComponent;
  @ViewChild(CpConstantsListComponent) cpConstantsListComponent: CpConstantsListComponent;

  cpModel: CpModel;
  isExistedProcess = true;

  constructor(private processService: ProcessService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    localStorage.setItem('viewTitle', 'Constraint Problem');
    // check if process exists
    if (localStorage.getItem('processId') === null) {
      console.log(`Any process doesn't exists`);
      this.isExistedProcess = false;
      this.setStateWithoutConstraintProblem();
    } else {
      this.processService.getCpModel(localStorage.getItem('processId')).subscribe(value => {
          this.cpModel = value;

          this.cpVariablesListComponent.updateTableData(value.variables);
          this.cpMetricsListComponent.updateTableData(value.metrics);
          this.cpConstantsListComponent.updateTableData(value.constants);
          this.isExistedProcess = true;
        },
        error1 => {
          this.snackBar.open(`Problem by getting CP model: ${error1.error.message}`, 'Close');
          this.setStateWithoutConstraintProblem();
        }
      );
    }
  }

  setStateWithoutConstraintProblem() {
    this.cpMetricsListComponent.loadingDataInProgress = false;
    this.cpVariablesListComponent.loadingDataInProgress = false;
    this.cpConstantsListComponent.loadingDataInProgress = false;
  }
}
