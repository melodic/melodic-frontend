import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CpRoutingModule} from './route/cp-routing.module';
import {CpDetailsComponent} from './cp-details/cp-details.component';
import {AngularMaterialModule} from '../../../angular-material/angular-material.module';
import {CpVariablesListComponent} from './cp-variables-list/cp-variables-list.component';
import {CpMetricsListComponent} from './cp-metrics-list/cp-metrics-list.component';
import {CpConstantsListComponent} from './cp-constants-list/cp-constants-list.component';

@NgModule({
  declarations: [
    CpDetailsComponent,
    CpVariablesListComponent,
    CpMetricsListComponent,
    CpConstantsListComponent
  ],
  imports: [
    CommonModule,
    CpRoutingModule,
    AngularMaterialModule,
  ]
})
export class CpModule {
}
