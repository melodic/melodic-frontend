import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CpConstantsListComponent} from './cp-constants-list.component';

describe('CpConstantsListComponent', () => {
  let component: CpConstantsListComponent;
  let fixture: ComponentFixture<CpConstantsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CpConstantsListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpConstantsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
