import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {CpConstant} from '../../../model/cp-constant';

@Component({
  selector: 'app-cp-constants-list',
  templateUrl: './cp-constants-list.component.html',
  styleUrls: ['./cp-constants-list.component.css']
})
export class CpConstantsListComponent implements OnInit {

  displayedColumns = ['position', 'id', 'type', 'value'];
  dataSource: MatTableDataSource<CpConstant>;
  loadingDataInProgress = true;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor() {
  }

  ngOnInit() {
  }

  updateTableData(cpConstantsList: Array<CpConstant>) {
    this.loadingDataInProgress = false;
    this.dataSource = new MatTableDataSource(cpConstantsList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
