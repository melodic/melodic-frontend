import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {CpVariable} from '../../../model/cp-variable';
import {CpDomain} from '../../../model/cp-domain';
import {ProcessHelperService} from '../../../service/process-helper.service';

@Component({
  selector: 'app-cp-variables-list',
  templateUrl: './cp-variables-list.component.html',
  styleUrls: ['./cp-variables-list.component.css']
})
export class CpVariablesListComponent implements OnInit {

  displayedColumns = ['position', 'id', 'componentId', 'variableType', 'domainValues', 'domainType'];
  dataSource: MatTableDataSource<CpVariable>;

  loadingDataInProgress = true;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private processHelperService: ProcessHelperService) {
  }

  ngOnInit() {
  }

  updateTableData(cpVariablesList: Array<CpVariable>) {
    this.loadingDataInProgress = false;
    this.dataSource = new MatTableDataSource(cpVariablesList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'domainType':
          return item.domain.type;
        case 'domainValues':
          return this.getDomainValues(item.domain);
        default:
          return item[property];
      }
    };
    this.dataSource.sort = this.sort;
  }

  getDomainValues(cpDomain: CpDomain): string {
    return this.processHelperService.getDomainValues(cpDomain);
  }
}
