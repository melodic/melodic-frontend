import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CpVariablesListComponent} from './cp-variables-list.component';

describe('CpVariablesListComponent', () => {
  let component: CpVariablesListComponent;
  let fixture: ComponentFixture<CpVariablesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CpVariablesListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpVariablesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
