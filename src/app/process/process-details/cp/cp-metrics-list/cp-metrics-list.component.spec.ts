import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CpMetricsListComponent} from './cp-metrics-list.component';

describe('CpMetricsListComponent', () => {
  let component: CpMetricsListComponent;
  let fixture: ComponentFixture<CpMetricsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CpMetricsListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpMetricsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
