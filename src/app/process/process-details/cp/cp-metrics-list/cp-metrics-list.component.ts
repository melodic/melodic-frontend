import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {CpMetric} from '../../../model/cp-metric';

@Component({
  selector: 'app-cp-metrics-list',
  templateUrl: './cp-metrics-list.component.html',
  styleUrls: ['./cp-metrics-list.component.css']
})
export class CpMetricsListComponent implements OnInit {

  displayedColumns = ['position', 'id', 'type', 'value'];
  dataSource: MatTableDataSource<CpMetric>;
  loadingDataInProgress = true;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor() {
  }

  ngOnInit() {
  }

  updateTableData(cpMetricsList: Array<CpMetric>) {
    this.loadingDataInProgress = false;
    this.dataSource = new MatTableDataSource(cpMetricsList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
