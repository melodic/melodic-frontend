import {Component, OnInit, ViewChild} from '@angular/core';
import {ScheduleCloudiator} from '../model/schedule-cloudiator';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {DeploymentDetailsService} from '../service/deployment-details.service';

@Component({
  selector: 'app-schedule-list',
  templateUrl: './schedule-list.component.html',
  styleUrls: ['./schedule-list.component.css']
})
export class ScheduleListComponent implements OnInit {

  panelOpenState = false;

  scheduleCloudiatorList: ScheduleCloudiator[];
  loadingScheduleCloudiatorListInProgress = false;

  data: MatTableDataSource<ScheduleCloudiator>;
  displayedColumns: string[] = ['no', 'id', 'job', 'instantiation', 'owner', 'state', 'processes'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private deploymentDetailsService: DeploymentDetailsService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.loadingScheduleCloudiatorListInProgress = true;
    this.deploymentDetailsService.getScheduleCloudiatorList().subscribe(value => {
        this.updateTableData(value);
      },
      error1 => {
        this.updateTableData([]);
        this.snackBar.open(`Problem by getting schedules list from Cloudiator: ${error1.error.message}`, 'Close');
      });
  }

  updateTableData(schedulesData: ScheduleCloudiator[]) {
    this.loadingScheduleCloudiatorListInProgress = false;
    this.scheduleCloudiatorList = schedulesData;
    this.data = new MatTableDataSource<ScheduleCloudiator>(this.scheduleCloudiatorList);
    this.data.paginator = this.paginator;
    this.data.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.data.filter = filterValue.trim();

    if (this.data.paginator) {
      this.data.paginator.firstPage();
    }
  }

  getTasksList(schedule: ScheduleCloudiator): string[] {
    const result = [];
    if (schedule.processes !== null) {
      schedule.processes
        .forEach(value => result.push(value.task));
    }
    return result;
  }
}
