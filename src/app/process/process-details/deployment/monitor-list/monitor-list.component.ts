import {Component, OnInit, ViewChild} from '@angular/core';
import {MonitorCloudiator} from '../model/monitor-cloudiator';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {DeploymentDetailsService} from '../service/deployment-details.service';
import {MonitoringTargetCloudiator} from '../model/monitoring-target-cloudiator';
import {DataSinkCloudiator} from '../model/data-sink-cloudiator';

@Component({
  selector: 'app-monitor-list',
  templateUrl: './monitor-list.component.html',
  styleUrls: ['./monitor-list.component.css']
})
export class MonitorListComponent implements OnInit {

  panelOpenState = false;
  monitorCloudiatorList: MonitorCloudiator[];
  loadingMonitorCloudiatorListInProgress = false;

  data: MatTableDataSource<MonitorCloudiator>;
  displayedColumns: string[] = ['no', 'metric', 'sensorType', 'sensorPort', 'targets', 'sinks', 'tags'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private deploymentDetailsService: DeploymentDetailsService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.loadingMonitorCloudiatorListInProgress = true;
    this.deploymentDetailsService.getMonitorCloudiatorList().subscribe(value => {
        this.updateTableData(value);
      },
      error1 => {
        this.updateTableData([]);
        this.snackBar.open(`Problem by getting monitors list from Cloudiator: ${error1.error.message}`, 'Close');
      });
  }

  updateTableData(monitorsList: MonitorCloudiator[]) {
    this.loadingMonitorCloudiatorListInProgress = false;
    this.monitorCloudiatorList = monitorsList;
    this.data = new MatTableDataSource<MonitorCloudiator>(this.monitorCloudiatorList);

    this.data.filterPredicate = (filteringData, filter) => {
      return this.getMonitorCloudiatorForFilteringAsString(filteringData).indexOf(filter) !== -1;
    };

    this.data.sortingDataAccessor = this.data.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'sensorType':
          return item.sensor.type;
        case 'sensorPort':
          return item.sensor.port;
        default:
          return item[property];
      }
    };

    this.data.paginator = this.paginator;
    this.data.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.data.filter = filterValue.trim();

    if (this.data.paginator) {
      this.data.paginator.firstPage();
    }
  }

  private getMonitorCloudiatorForFilteringAsString(filteringData: MonitorCloudiator) {
    return filteringData.metric + filteringData.sensor.type + filteringData.sensor.port
      + this.getTargetsListAsString(filteringData.targets) + this.getSinksListAsString(filteringData.sinks)
      + this.getMapAsString(filteringData.tags);
  }

  private getTargetsListAsString(targets: Array<MonitoringTargetCloudiator>) {
    let result = '';
    targets.forEach(value => {
      result += value.type + value.identifier;
    });
    return result;
  }

  private getSinksListAsString(sinks: Array<DataSinkCloudiator>) {
    let result = '';
    sinks.forEach(value => {
      result += value.type + this.getMapAsString(value.configuration);
    });
    return result;
  }

  private getMapAsString(mapOfStrings: Map<string, string>) {
    let result = '';
    Object.keys(mapOfStrings).forEach(key => {
      result += key + mapOfStrings[key];
    });
    return result;
  }
}
