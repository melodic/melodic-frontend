import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AppConfigService} from '../../../../app-config/service/app-config.service';
import {Observable} from 'rxjs';
import {ProcessCloudiator} from '../model/process-cloudiator';
import {tap} from 'rxjs/operators';
import {NodeCloudiator} from '../../../../application/model/node-cloudiator';
import {QueueCloudiator} from '../model/queue-cloudiator';
import {JobCloudiator} from '../model/job-cloudiator';
import {ScheduleCloudiator} from '../model/schedule-cloudiator';
import {MonitorCloudiator} from '../model/monitor-cloudiator';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class DeploymentDetailsService {

  apiUrl = `${AppConfigService.settings.apiUrl}/auth/process/deployment`;

  constructor(private http: HttpClient) {
  }

  getProcessCloudiatorList(): Observable<Array<ProcessCloudiator>> {
    const requestUrl = `${this.apiUrl}/process`;
    return this.http.get(requestUrl, httpOptions).pipe(
      tap((response: Array<ProcessCloudiator>) => console.log(`Response with cloudiator processes list with ${response.length} elements`),
        e => console.log(`Error by getting cloudiator processes list`, e))
    );
  }

  getNodeCloudiatorList(): Observable<Array<NodeCloudiator>> {
    const requestUrl = `${this.apiUrl}/node`;
    return this.http.get(requestUrl, httpOptions).pipe(
      tap((response: Array<NodeCloudiator>) => console.log(`Response with cloudiator nodes list with ${response.length} elements`),
        e => console.log('Error by getting cloudiator nodes list', e))
    );
  }

  getQueueCloudiatorList(): Observable<Array<QueueCloudiator>> {
    const requestUrl = `${this.apiUrl}/queue`;
    return this.http.get(requestUrl, httpOptions).pipe(
      tap((response: Array<QueueCloudiator>) => console.log(`Response from cloudiator queue list with ${response.length} elements`),
        e => console.log('Error by getting cloudiator queue list', e))
    );
  }

  getJobCloudiatorList(): Observable<Array<JobCloudiator>> {
    const requestUrl = `${this.apiUrl}/job`;
    return this.http.get(requestUrl, httpOptions).pipe(
      tap((response: Array<JobCloudiator>) => console.log(`Response from cloudiator jobs list with ${response.length} elements`),
        e => console.log('Error by getting cloudiator job list', e))
    );
  }

  getScheduleCloudiatorList(): Observable<Array<ScheduleCloudiator>> {
    const requestUrl = `${this.apiUrl}/schedule`;
    return this.http.get(requestUrl, httpOptions).pipe(
      tap((response: Array<ScheduleCloudiator>) => console.log(`Response from cloudiator schedules list with ${response.length} elements`),
        e => console.log('Error by getting cloudiator schedule list', e))
    );
  }

  getMonitorCloudiatorList(): Observable<Array<MonitorCloudiator>> {
    const requestUrl = `${this.apiUrl}/monitor`;
    return this.http.get(requestUrl, httpOptions).pipe(
      tap((response: Array<MonitorCloudiator>) => console.log(`Response from cloudiator monitor list with ${response.length} elements`),
        e => console.log('Error by getting cloudiator monitor list: ', e))
    );
  }
}
