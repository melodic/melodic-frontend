import {TestBed} from '@angular/core/testing';

import {DeploymentDetailsService} from './deployment-details.service';

describe('DeploymentDetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeploymentDetailsService = TestBed.get(DeploymentDetailsService);
    expect(service).toBeTruthy();
  });
});
