export class DataSinkCloudiator {
  type: string;
  configuration: Map<string, string>;
}
