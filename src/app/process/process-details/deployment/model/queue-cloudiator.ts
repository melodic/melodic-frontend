import {DateCloudiator} from './date-cloudiator';

export class QueueCloudiator {
  id: string;
  status: string;
  start: DateCloudiator;
  end: DateCloudiator;
  diagnosis: string;
  location: string;
}
