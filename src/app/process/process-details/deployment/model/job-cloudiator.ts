export class JobCloudiator {
  name: string;
  id: string;
  owner: string;
  tasks: Array<any>;
  communications: Array<any>;
  requirements: Array<any>;
  optimization: any;
}
