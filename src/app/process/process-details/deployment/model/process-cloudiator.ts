import {IpAddress} from '../../../../application/model/ip-address';

export class ProcessCloudiator {
  id: string;
  originId: string;
  processType: string;
  state: string;
  type: string;
  schedule: string;
  task: string;
  taskInterface: string;
  diagnostic: string;
  reason: string;
  owner: string;
  ipAddresses: Array<IpAddress>;
  endpoint: string;
  node: string;
}
