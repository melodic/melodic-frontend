import {ProcessCloudiator} from './process-cloudiator';

export class ScheduleCloudiator {
  id: string;
  job: string;
  instantiation: string;
  owner: string;
  state: string;
  processes: Array<ProcessCloudiator>;
}
