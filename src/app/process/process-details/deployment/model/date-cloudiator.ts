export class DateCloudiator {
  dayOfYear: number;
  dayOfWeek: string;
  month: string;
  dayOfMonth: number;
  year: number;
  monthValue: number;
  hour: number;
  minute: number;
  second: number;
  nano: number;


  constructor(dayOfMonthArg: number, yearArg: number, monthValueArg: number, hourArg: number, minuteArg: number, secondArg: number) {
    this.dayOfMonth = dayOfMonthArg;
    this.year = yearArg;
    this.monthValue = monthValueArg;
    this.hour = hourArg;
    this.minute = minuteArg;
    this.second = secondArg;
  }

  getDateCloudiatorAsString(): string {
    return `${this.getNumberValue(this.dayOfMonth)}-${this.getNumberValue(this.monthValue)}-${this.year}T${this.getNumberValue(this.hour)}`
      + `:${this.getNumberValue(this.minute)}:${this.getNumberValue(this.second)}`;
  }

  getNumberValue(value: number): string {
    if (value < 10) {
      return `0${value}`;
    } else {
      return value.toString();
    }
  }
}
