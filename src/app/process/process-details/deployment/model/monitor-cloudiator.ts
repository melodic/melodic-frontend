import {MonitoringTargetCloudiator} from './monitoring-target-cloudiator';
import {SensorCloudiator} from './sensor-cloudiator';
import {DataSinkCloudiator} from './data-sink-cloudiator';

export class MonitorCloudiator {
  metric: string;
  targets: Array<MonitoringTargetCloudiator>;
  sensor: SensorCloudiator;
  sinks: Array<DataSinkCloudiator>;
  tags: Map<string, string>;
}
