import {Component, OnInit, ViewChild} from '@angular/core';
import {NodeCloudiator} from '../../../../application/model/node-cloudiator';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Cloud} from '../../offer/model/cloud';
import {IpAddressType} from '../../../../application/model/ip-address';

@Component({
  selector: 'app-node-list',
  templateUrl: './node-list.component.html',
  styleUrls: ['./node-list.component.css']
})
export class NodeListComponent implements OnInit {

  loadingNodeListInProgress = false;
  nodeList: NodeCloudiator[];
  cloudList: Cloud[];

  data: MatTableDataSource<NodeCloudiator>;
  displayedColumns: string[] = ['no', 'name', 'id', 'nodeType', 'state', 'publicIp', 'privateIp', 'provider', 'diagnostic'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor() {
  }

  ngOnInit() {
  }

  updateData(nodeData: NodeCloudiator[], cloudData: Cloud[]) {
    this.loadingNodeListInProgress = false;
    this.nodeList = nodeData;
    this.cloudList = cloudData;
    this.nodeList.forEach(value => {
      value.provider = this.getProviderName(value);
      value.privateIp = this.getPrivateIp(value);
      value.publicIp = this.getPublicIp(value);
    });

    this.data = new MatTableDataSource<NodeCloudiator>(this.nodeList);
    this.data.paginator = this.paginator;
    this.data.sort = this.sort;
  }

  getProviderName(node: NodeCloudiator): string {
    const provider = this.cloudList
      .find(value => value.id === node.nodeProperties.providerId);
    return provider ? provider.api.providerName : 'UNKNOWN';
  }

  getPublicIp(node: NodeCloudiator): string {
    if (node.ipAddresses === null) {
      return '-';
    } else {
      return node.ipAddresses
        .find(value => value.ipAddressType.toString() === IpAddressType[IpAddressType.PUBLIC_IP])
        .value;
    }
  }

  getPrivateIp(node: NodeCloudiator): string {
    if (node.ipAddresses === null) {
      return '-';
    } else {
      const privateIp = node.ipAddresses
        .find(value => value.ipAddressType.toString() === IpAddressType[IpAddressType.PRIVATE_IP]);
      return privateIp ? privateIp.value : 'UNKNOWN';
    }
  }

  applyFilter(filterValue: string) {
    this.data.filter = filterValue.trim();

    if (this.data.paginator) {
      this.data.paginator.firstPage();
    }
  }
}
