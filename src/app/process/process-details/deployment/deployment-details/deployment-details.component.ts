import {Component, OnInit, ViewChild} from '@angular/core';
import {NodeListComponent} from '../node-list/node-list.component';
import {ProcessCloudiatorListComponent} from '../process-cloudiator-list/process-cloudiator-list.component';
import {DeploymentDetailsService} from '../service/deployment-details.service';
import {MatSnackBar} from '@angular/material';
import {ProcessOfferService} from '../../offer/service/process-offer.service';

@Component({
  selector: 'app-deployment-details',
  templateUrl: './deployment-details.component.html',
  styleUrls: ['./deployment-details.component.css']
})
export class DeploymentDetailsComponent implements OnInit {

  @ViewChild(NodeListComponent) nodeListComponent: NodeListComponent;
  @ViewChild(ProcessCloudiatorListComponent) processCloudiatorListComponent: ProcessCloudiatorListComponent;

  constructor(private deploymentDetailsService: DeploymentDetailsService,
              private processOfferService: ProcessOfferService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    localStorage.setItem('viewTitle', 'Deployment details');
    this.processCloudiatorListComponent.loadingProcessCloudiatorListInProgress = true;
    this.nodeListComponent.loadingNodeListInProgress = true;
    this.deploymentDetailsService.getNodeCloudiatorList().subscribe(nodeListResponse => {
        this.processOfferService.getCloudList().subscribe(cloudsResponse => {
            this.nodeListComponent.updateData(nodeListResponse, cloudsResponse);
          },
          error1 => {
            this.snackBar.open('Error by getting clouds list', 'Close');
            this.nodeListComponent.updateData([], []);
          });

        this.deploymentDetailsService.getProcessCloudiatorList().subscribe(processListResponse => {
            this.processCloudiatorListComponent.updateTableData(processListResponse, nodeListResponse);
          },
          error1 => {
            this.snackBar.open('Error by getting process Cloudiator list', 'Close');
            this.processCloudiatorListComponent.updateTableData([], []);
          });
      },
      error1 => {
        this.snackBar.open('Error by getting nodes Cloudiator list', 'Close');
        this.nodeListComponent.updateData([], []);
        this.processCloudiatorListComponent.updateTableData([], []);
      });
  }

}
