import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProcessCloudiatorListComponent} from './process-cloudiator-list.component';

describe('ProcessCloudiatorListComponent', () => {
  let component: ProcessCloudiatorListComponent;
  let fixture: ComponentFixture<ProcessCloudiatorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProcessCloudiatorListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessCloudiatorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
