import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {ProcessCloudiator} from '../model/process-cloudiator';
import {NodeCloudiator} from '../../../../application/model/node-cloudiator';

@Component({
  selector: 'app-process-cloudiator-list',
  templateUrl: './process-cloudiator-list.component.html',
  styleUrls: ['./process-cloudiator-list.component.css']
})
export class ProcessCloudiatorListComponent implements OnInit {

  processCloudiatorList: ProcessCloudiator[];
  nodeCloudiatorList: NodeCloudiator[];
  loadingProcessCloudiatorListInProgress = false;

  data: MatTableDataSource<ProcessCloudiator>;
  displayedColumns: string[] = ['no', 'task', 'id', 'processType', 'state', 'type', 'schedule', 'node', 'diagnostic', 'owner'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor() {
  }

  ngOnInit() {
  }

  updateTableData(processData: ProcessCloudiator[], nodeData: NodeCloudiator[]) {
    this.loadingProcessCloudiatorListInProgress = false;
    this.processCloudiatorList = processData;
    this.nodeCloudiatorList = nodeData;
    this.data = new MatTableDataSource<ProcessCloudiator>(this.processCloudiatorList);

    this.data.filterPredicate = (filteringData, filter) => {
      return this.getProcessCloudiatorForFilteringAsString(filteringData).indexOf(filter) !== -1;
    };

    this.data.sortingDataAccessor = this.data.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'node':
          return this.getNodeName(item);
        default:
          return item[property];
      }
    };

    this.data.paginator = this.paginator;
    this.data.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.data.filter = filterValue.trim();

    if (this.data.paginator) {
      this.data.paginator.firstPage();
    }
  }

  getNodeName(processCloudiator: ProcessCloudiator): string {
    const nodesWithId = this.nodeCloudiatorList
      .filter(value => value.id === processCloudiator.node);
    if (nodesWithId.length > 0) {
      return nodesWithId[0].name;
    } else {
      return '';
    }
  }

  private getProcessCloudiatorForFilteringAsString(processCloudiator: ProcessCloudiator) {
    return processCloudiator.task + processCloudiator.id + processCloudiator.processType + processCloudiator.state
      + processCloudiator.type + processCloudiator.schedule + this.getNodeName(processCloudiator) + processCloudiator.diagnostic
      + processCloudiator.owner;
  }
}
