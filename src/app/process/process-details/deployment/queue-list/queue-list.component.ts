import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {QueueCloudiator} from '../model/queue-cloudiator';
import {DeploymentDetailsService} from '../service/deployment-details.service';
import {DateCloudiator} from '../model/date-cloudiator';

@Component({
  selector: 'app-queue-list',
  templateUrl: './queue-list.component.html',
  styleUrls: ['./queue-list.component.css']
})
export class QueueListComponent implements OnInit {

  loadingQueueListInProgress = false;
  queueList: QueueCloudiator[];

  data: MatTableDataSource<QueueCloudiator>;
  displayedColumns: string[] = ['no', 'id', 'status', 'start', 'end', 'diagnosis', 'location'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private deploymentDetailsService: DeploymentDetailsService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.loadingQueueListInProgress = true;
    this.deploymentDetailsService.getQueueCloudiatorList().subscribe(value => {
        this.updateData(value);
      },
      error1 => {
        this.updateData([]);
        this.snackBar.open(`Problem by getting queue from Cloudiator: ${error1.error.message}`, 'Close');
      });
  }

  updateData(queueData: QueueCloudiator[]) {
    this.loadingQueueListInProgress = false;
    this.queueList = queueData;
    this.data = new MatTableDataSource<QueueCloudiator>(this.queueList);

    this.data.filterPredicate = (filteringData, filter) => {
      return this.getQueueCloudiatorForFilteringAsString(filteringData).indexOf(filter) !== -1;
    };

    this.data.sortingDataAccessor = this.data.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'start':
          return this.getStartDate(item);
        case 'end':
          return this.getEndDate(item);
        default:
          return item[property];
      }
    };

    this.data.paginator = this.paginator;
    this.data.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.data.filter = filterValue.trim();

    if (this.data.paginator) {
      this.data.paginator.firstPage();
    }
  }

  getStartDate(queue: QueueCloudiator) {
    if (queue.start === null) {
      return '-';
    } else {
      const startDate = new DateCloudiator(queue.start.dayOfMonth, queue.start.year, queue.start.monthValue, queue.start.hour,
        queue.start.minute, queue.start.second);
      return startDate.getDateCloudiatorAsString();
    }
  }

  getEndDate(queue: QueueCloudiator) {
    if (queue.end === null) {
      return '-';
    } else {
      const endDate = new DateCloudiator(queue.end.dayOfMonth, queue.end.year, queue.end.monthValue, queue.end.hour, queue.end.minute,
        queue.end.second);
      return endDate.getDateCloudiatorAsString();
    }
  }

  private getQueueCloudiatorForFilteringAsString(queue: QueueCloudiator) {
    return queue.id + queue.status + this.getStartDate(queue) + this.getEndDate(queue) + queue.diagnosis + queue.location;
  }
}
