import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DeploymentRoutingModule} from './route/deployment-routing.module';
import {ProcessCloudiatorListComponent} from './process-cloudiator-list/process-cloudiator-list.component';
import {NodeListComponent} from './node-list/node-list.component';
import {DeploymentDetailsComponent} from './deployment-details/deployment-details.component';
import {AngularMaterialModule} from '../../../angular-material/angular-material.module';
import {QueueListComponent} from './queue-list/queue-list.component';
import {JobListComponent} from './job-list/job-list.component';
import {ScheduleListComponent} from './schedule-list/schedule-list.component';
import {MonitorListComponent} from './monitor-list/monitor-list.component';

@NgModule({
  declarations: [
    ProcessCloudiatorListComponent,
    NodeListComponent,
    DeploymentDetailsComponent,
    QueueListComponent,
    JobListComponent,
    ScheduleListComponent,
    MonitorListComponent
  ],
  imports: [
    CommonModule,
    DeploymentRoutingModule,
    AngularMaterialModule
  ]
})
export class DeploymentModule {
}
