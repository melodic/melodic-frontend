import {Component, OnInit, ViewChild} from '@angular/core';
import {DeploymentDetailsService} from '../service/deployment-details.service';
import {JobCloudiator} from '../model/job-cloudiator';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.css']
})
export class JobListComponent implements OnInit {

  panelOpenState = false;
  loadingJobListInProgress = false;
  jobList: JobCloudiator[];

  data: MatTableDataSource<JobCloudiator>;
  displayedColumns: string[] = ['no', 'name', 'id', 'owner', 'properties'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private deploymentDetailsService: DeploymentDetailsService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.loadingJobListInProgress = true;
    this.deploymentDetailsService.getJobCloudiatorList().subscribe(value => {
        this.updateData(value);
      },
      error1 => {
        this.updateData([]);
        this.snackBar.open(`Problem by getting jobs list from Cloudiator: ${error1.error.message}`, 'Close');
      });
  }

  updateData(jobsData: JobCloudiator[]) {
    this.loadingJobListInProgress = false;
    this.jobList = jobsData;
    this.data = new MatTableDataSource<JobCloudiator>(this.jobList);
    this.data.paginator = this.paginator;
    this.data.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.data.filter = filterValue.trim();

    if (this.data.paginator) {
      this.data.paginator.firstPage();
    }
  }
}
