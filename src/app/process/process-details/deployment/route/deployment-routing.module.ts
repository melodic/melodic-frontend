import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DeploymentDetailsComponent} from '../deployment-details/deployment-details.component';

const routes: Routes = [
  {path: '', component: DeploymentDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeploymentRoutingModule {
}
