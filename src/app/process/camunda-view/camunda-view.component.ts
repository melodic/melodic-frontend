import {Component, OnInit} from '@angular/core';
import {AppConfigService} from '../../app-config/service/app-config.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-process-view',
  templateUrl: './camunda-view.component.html',
  styleUrls: ['./camunda-view.component.css']
})
export class CamundaViewComponent implements OnInit {

  private camundaUrl = `${AppConfigService.settings.camundaUrl}/app/cockpit/default/#/dashboard`;
  camundaRedirectionInProgress = false;

  constructor(private router: Router) {
  }

  ngOnInit() {
    localStorage.setItem('viewTitle', 'Process');
    this.camundaRedirectionInProgress = true;
    window.open(this.camundaUrl);
    this.router.navigate(['/process']);
  }
}
