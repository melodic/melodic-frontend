import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CamundaViewComponent} from './camunda-view.component';

describe('CamundaViewComponent', () => {
  let component: CamundaViewComponent;
  let fixture: ComponentFixture<CamundaViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CamundaViewComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CamundaViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
