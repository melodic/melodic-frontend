import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FileUploaderComponent} from '../uploader/file-uploader.component';
import {AuthGuard} from '../../security-config/auth.guard';

const routes: Routes = [
  {path: '', component: FileUploaderComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FileUploaderRoutingModule {
}
