import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SensitiveVariablesDialogComponent} from './sensitive-variables-dialog.component';

describe('SensitiveVariablesDialogComponent', () => {
  let component: SensitiveVariablesDialogComponent;
  let fixture: ComponentFixture<SensitiveVariablesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SensitiveVariablesDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SensitiveVariablesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
