import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar, MatTableDataSource} from '@angular/material';
import {FormArray, FormBuilder, FormControl, FormGroup, NgForm} from '@angular/forms';
import {SecureVariable} from '../model/secure-variable';

@Component({
  selector: 'app-sensitive-variables-dialog',
  templateUrl: './sensitive-variables-dialog.component.html',
  styleUrls: ['./sensitive-variables-dialog.component.css']
})
export class SensitiveVariablesDialogComponent implements OnInit {

  @Input() variablesNames: Array<string>;

  form: FormGroup;
  displayedColumns = ['name', 'value'];
  dataSource: MatTableDataSource<any>;
  bulkEditActive = false;

  constructor(
    private dialogRef: MatDialogRef<SensitiveVariablesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.variablesNames = this.data.variablesNames;
    this.form = this.formBuilder.group({
      variables: this.formBuilder.array([]),
      bulkEditVariables: ['']
    });
    const variablesAsFormArray = this.data.variables
      .map(SecureVariable.asFormGroup);
    this.form.setControl('variables', new FormArray(variablesAsFormArray));
    this.form.setControl('bulkEditVariables', new FormControl(this.mapVariablesFromArrayToText()));
    this.dataSource = new MatTableDataSource((this.variables).controls);
  }

  get variables(): FormArray {
    return this.form.get('variables') as FormArray;
  }

  get bulkEditVariables() {
    return this.form.get('bulkEditVariables');
  }

  saveSensitiveVariables(sensitiveVariablesForm: NgForm) {
    let sensitiveVariables = [];
    if (this.bulkEditActive) {
      console.log('Take variables from bulk edit area');
      sensitiveVariables = this.mapBulkEditVariablesTextToArray(false);
    } else {
      console.log('Take variables from table');
      sensitiveVariables = <Array<SecureVariable>> sensitiveVariablesForm.value.variables;
    }
    this.dialogRef.close(sensitiveVariables);
  }

  onBulkEditToggleChange(event: any) {
    console.log(`Bulk edit toggle changed for bulkEditActive= ${this.bulkEditActive}`);
    if (this.bulkEditActive) {
      this.form.controls.bulkEditVariables.setValue(this.mapVariablesFromArrayToText());
    } else {
      this.form.setControl('variables', this.formBuilder.array(this.mapBulkEditVariablesTextToArray(false)
        .map(SecureVariable.asFormGroup)));
      this.dataSource = new MatTableDataSource((this.variables).controls);
    }
  }

  isSensitiveVariablesDefinitionValid(): boolean {
    let validationResult = true;
    let sensitiveVariables = new Array<SecureVariable>();
    if (this.bulkEditActive) {
      sensitiveVariables = this.mapBulkEditVariablesTextToArray(true);
    } else {
      sensitiveVariables = <Array<SecureVariable>> this.form.value.variables;
    }
    sensitiveVariables.forEach(value => {
      if (value.value.trim().length === 0) {
        validationResult = false;
      }
    });
    return validationResult;
  }

  mapVariablesFromArrayToText(): string {
    let variablesAsText = '';
    this.variables.controls.forEach(value => {
      variablesAsText += `${value.value.name.trim()}: ${value.value.value.trim()}\n`;
    });
    return variablesAsText.trim();
  }

  private mapBulkEditVariablesTextToArray(isValidationParsing: boolean) {
    const secureVariablesFromText = new Array<SecureVariable>();
    const variableAsText: string = this.bulkEditVariables.value;
    const variablesPairs: string[] = variableAsText.trim().split('\n');
    variablesPairs.forEach(variable => {
      const singleVariable: string[] = variable.trim().split(':');
      if (singleVariable.length === 2) { // valid format key: value
        secureVariablesFromText.push(new SecureVariable(singleVariable[0].trim(), singleVariable[1].trim()));
      } else if (singleVariable.length !== 2 && !isValidationParsing && variable.trim().length !== 0) {
        console.log(`Invalid format of text variable: ${variable}`);
        this.snackBar.open(`Invalid format of text variable: ${variable}`, 'Close');
      }
    });
    return secureVariablesFromText;
  }
}
