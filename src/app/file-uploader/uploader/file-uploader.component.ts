import {Component, EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {HttpClient, HttpEventType} from '@angular/common/http';
import {MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {SensitiveVariablesDialogComponent} from '../sensitive-variables-dialog/sensitive-variables-dialog.component';
import {SecureVariablesService} from '../service/secure-variables.service';
import {UploaderXmiComponent} from '../../deploying-application/uploader-xmi/uploader-xmi.component';
import {DeploymentService} from '../../deploying-application/service/deployment.service';
import {SecureVariable} from '../model/secure-variable';
import {MenuComponent} from '../../common-template/menu/menu.component';

@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.css']
})
export class FileUploaderComponent implements OnDestroy {

  sensitiveVariablesDialog: MatDialogRef<SensitiveVariablesDialogComponent>;

  constructor(
    private httpClient: HttpClient,
    private dialog: MatDialog,
    private secureVariablesService: SecureVariablesService,
    private deploymentService: DeploymentService,
    private snackBar: MatSnackBar,
    private uploaderXmiComponent: UploaderXmiComponent,
    private menuComponent: MenuComponent
  ) {
  }

  @Input()
  httpUrl: string;

  @Input()
  fileAlias = 'file';

  @Input()
  set file(file: any) {
    this.uploadingFile = file;
    this.total = this.uploadingFile.size;
  }

  get file(): any {
    return this.uploadingFile;
  }

  @Input()
  set id(id: number) {
    this.fileId = id;
  }

  get id(): number {
    return this.fileId;
  }

  @Output() removeEvent = new EventEmitter<FileUploaderComponent>();
  @Output() uploadEvent = new EventEmitter();

  isUploading = false;
  progressPercentage = 0;
  total = 0;
  public loaded = 0;
  public uploadingFile: any;
  private fileId: number;
  private fileUploadSubscription: any;

  public upload(): void {
    this.isUploading = true;
    this.menuComponent.blockUIView(`Validation and storage of model from ${this.uploadingFile.name} in progress ...`);
    const formData = new FormData();
    formData.set(this.fileAlias, this.uploadingFile, this.uploadingFile.name);
    this.fileUploadSubscription = this.deploymentService.uploadModel(formData)
      .subscribe((event: any) => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progressPercentage = Math.floor(event.loaded * 100 / event.total);
          this.loaded = event.loaded;
          this.total = event.total;
        }
        this.uploadEvent.emit({file: this.uploadingFile, event});
        if (event.type === HttpEventType.Response) {
          const secureVariables: Array<SecureVariable> = event.body.secureVariables;
          console.log('File uploaded, removing from uploading list', event.body.secureVariables);
          const modelName = event.body.modelName;
          this.secureVariablesService.addUploadedModelToLocalStorage(modelName, secureVariables);
          if (secureVariables.length !== 0) {
            console.log(`Open dialog for sensitive variables. The first one: ${secureVariables[0]}`);
            this.openDialog(secureVariables, modelName);
          } else {
            console.log(`List of sensitive variables for ${modelName} is empty`);
          }
          this.setUploadingAsFinished();
          this.remove();
        }

      }, (error: any) => {
        this.setUploadingAsFinished();
        console.log(`Error by uploading`, error);
        console.log(`Error message: ${error.error.message}`);
        if (this.fileUploadSubscription) {
          this.fileUploadSubscription.unsubscribe();
        }
        this.progressPercentage = 0;
        this.loaded = 0;
        this.isUploading = false;
        this.uploadEvent.emit({file: this.uploadingFile, event: error});
        console.log(`After emit error event`, error);
      });
  }

  public remove(): void {
    if (this.fileUploadSubscription) {
      this.fileUploadSubscription.unsubscribe();
    }
    console.log('Removing file from list');
    this.removeEvent.emit(this);
  }

  convertBytesToSize(bytes): string {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes === 0) {
      return '0 Byte';
    }
    const i = Math.floor(Math.log(bytes) / Math.log(1024));
    return Math.round(bytes / Math.pow(1024, i)) + ' ' + sizes[i];
  }

  ngOnDestroy() {
    console.log(`file: ${this.uploadingFile.name} destroyed...`);
  }

  public openDialog(sensitiveVariables: Array<SecureVariable>, modelName: string): void {
    this.sensitiveVariablesDialog = this.dialog.open(SensitiveVariablesDialogComponent, {
      hasBackdrop: true,
      disableClose: true,
      data: {
        variables: sensitiveVariables
      },
      width: '50%'
    });

    this.sensitiveVariablesDialog.afterClosed().subscribe(result => {
      console.log(`Secure variables dialog closed with result`, result);
      this.uploaderXmiComponent.saveSensitiveVariables(result, modelName);
    });
  }

  public getUploadedModelsList() {
    this.uploaderXmiComponent.getUploadedModelsList();
  }

  public setUploadingInProgress() {
    this.uploaderXmiComponent.uploadInProgress = true;
    this.menuComponent.blockUIView('Validation and storage of model in progress ..');
  }

  public setUploadingAsFinished() {
    this.uploaderXmiComponent.uploadInProgress = false;
    this.menuComponent.unblockUIView();
  }
}
