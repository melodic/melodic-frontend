import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FileUploaderRoutingModule} from './route/file-uploader-routing.module';
import {FileUploaderComponent} from './uploader/file-uploader.component';
import {FilesQueueComponent} from './files-queue/files-queue.component';
import {AngularMaterialModule} from '../angular-material/angular-material.module';
import {SensitiveVariablesDialogComponent} from './sensitive-variables-dialog/sensitive-variables-dialog.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    FileUploaderComponent,
    FilesQueueComponent,
    SensitiveVariablesDialogComponent
  ],
  imports: [
    CommonModule,
    FileUploaderRoutingModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    FileUploaderComponent,
    FilesQueueComponent,
    SensitiveVariablesDialogComponent
  ],
  entryComponents: [
    SensitiveVariablesDialogComponent
  ]
})
export class FileUploaderModule {
}
