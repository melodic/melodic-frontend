import {FormControl, FormGroup} from '@angular/forms';

export class SecureVariable {
  name: string;
  value: string;

  constructor(private nameArg: string, private valueArg: string) {
    this.name = nameArg;
    this.value = valueArg;
  }

  static asFormGroup(sensitiveVariable: SecureVariable): FormGroup {
    return new FormGroup({
      name: new FormControl(sensitiveVariable.name),
      value: new FormControl(sensitiveVariable.value)
    });
  }
}
