import {SecureVariable} from './secure-variable';

export class UploadXmiResponse {
  modelName: string;
  secureVariables: Array<SecureVariable>;
  httpStatus: string;
  message: string;
}
