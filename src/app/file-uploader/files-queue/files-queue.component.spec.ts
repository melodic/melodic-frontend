import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FilesQueueComponent} from './files-queue.component';

describe('FilesQueueComponent', () => {
  let component: FilesQueueComponent;
  let fixture: ComponentFixture<FilesQueueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FilesQueueComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilesQueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
