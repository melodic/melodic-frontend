import {AfterViewInit, Component, ContentChildren, forwardRef, Input, OnDestroy, QueryList} from '@angular/core';
import {merge, Subscription} from 'rxjs';
import {FileUploaderComponent} from '../uploader/file-uploader.component';
import {startWith} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material';
import {DeploymentService} from '../../deploying-application/service/deployment.service';
import {SecureVariablesService} from '../service/secure-variables.service';
import {UploadXmiResponse} from '../model/upload-xmi-response';
import {UploadedModel} from '../../deploying-application/model/uploaded-model';

@Component({
  selector: 'app-files-queue',
  templateUrl: './files-queue.component.html',
  styleUrls: ['./files-queue.component.css']
})
export class FilesQueueComponent implements OnDestroy, AfterViewInit {

  @ContentChildren(forwardRef(() => FileUploaderComponent)) fileUploads: QueryList<FileUploaderComponent>;

  private fileRemoveSubscription: Subscription | null;

  private changeSubscription: Subscription;

  get fileUploadRemoveEvents() {
    return merge(...this.fileUploads.map(fileUpload => fileUpload.removeEvent));
  }

  files: Array<any> = [];
  uploadingInProgress = false;

  @Input()
  fileExtension = 'xmi';

  @Input()
  notAllowedNames = [];

  constructor(private snackBar: MatSnackBar,
              private deploymentService: DeploymentService,
              private secureVariablesService: SecureVariablesService) {
  }

  ngAfterViewInit() {
    // When the list changes, re-subscribe
    this.changeSubscription = this.fileUploads.changes.pipe(startWith(null)).subscribe(() => {
      if (this.fileRemoveSubscription) {
        this.fileRemoveSubscription.unsubscribe();
      }
      this.listenToFileRemoved();
    });
  }

  private listenToFileRemoved(): void {
    this.fileRemoveSubscription = this.fileUploadRemoveEvents.subscribe((event: FileUploaderComponent) => {
      this.files.splice(event.id, 1);
    });
  }

  add(file: any) {
    console.log(`Add file with name ${file.name} event`);
    if (this.validFileType(file.name)) {
      if (this.validFileName(file)) {
        this.files.push(file);
      } else {
        this.snackBar.open(`Such file is already attached.`, 'Close', {duration: 10000});
      }
    } else {
      this.snackBar.open(`Invalid file type. Required ${this.fileExtension} extension`, 'Close', {duration: 10000});
    }
  }

  public uploadAll() {
    this.fileUploads.forEach(item => item.setUploadingInProgress());
    this.uploadingInProgress = true;
    this.deploymentService.uploadMultipleModels(this.fileUploads).subscribe(value => {
        let successfullyUploadedFilesNames = '';
        let failedUploadedFilesMessage = '';
        console.log('Files uploaded successfully');
        this.uploadingInProgress = false;
        const numberOfModels = this.fileUploads.length;
        this.fileUploads.forEach(item => {
          item.setUploadingAsFinished();
          for (let i = 0; i < numberOfModels; i++) {
            item.remove();
          }
        });
        let uploadXmiResponseWithSecureVariable: UploadXmiResponse;
        value.forEach(uploadedXmiFileResponse => {
          if (uploadedXmiFileResponse.httpStatus !== 'CREATED') {
            failedUploadedFilesMessage += uploadedXmiFileResponse.message + ';\n';
          } else {
            console.log(`Uploaded file ${uploadedXmiFileResponse.modelName} with secure variables in number =
        ${uploadedXmiFileResponse.secureVariables.length}`);
            if (uploadedXmiFileResponse.secureVariables.length !== 0) {
              uploadXmiResponseWithSecureVariable = uploadedXmiFileResponse;
            }
            successfullyUploadedFilesNames += uploadedXmiFileResponse.modelName + ', ';
            this.secureVariablesService.addUploadedModelToLocalStorage(uploadedXmiFileResponse.modelName,
              uploadedXmiFileResponse.secureVariables);
          }
        });
        this.fileUploads.first.getUploadedModelsList();
        if (successfullyUploadedFilesNames.length !== 0 && failedUploadedFilesMessage.length === 0) {
          successfullyUploadedFilesNames = successfullyUploadedFilesNames.substring(0, successfullyUploadedFilesNames.length - 2);
          this.snackBar.open(`Models: ${successfullyUploadedFilesNames} uploaded successfully`, 'Close');
        } else if (successfullyUploadedFilesNames.length !== 0 && failedUploadedFilesMessage.length !== 0) {
          this.snackBar.open(`Models: ${successfullyUploadedFilesNames} uploaded successfully, but uploading of other files failed:
          ${failedUploadedFilesMessage}`, 'Close');
        } else {
          this.snackBar.open(`No file has been uploaded successfully: ${failedUploadedFilesMessage}`, 'Close');
        }
        if (uploadXmiResponseWithSecureVariable) {
          this.fileUploads.first.openDialog(uploadXmiResponseWithSecureVariable.secureVariables,
            uploadXmiResponseWithSecureVariable.modelName);
        }
      },
      error1 => {
        this.uploadingInProgress = false;
        this.fileUploads.first.setUploadingAsFinished();
        this.snackBar.open('Error by uploading files list', 'Close');
      });
  }

  public removeAll() {
    this.files.splice(0, this.files.length);
  }

  ngOnDestroy() {
    if (this.files) {
      this.removeAll();
    }
  }

  private validFileType(name: string): boolean {
    return name.endsWith('.' + this.fileExtension);
  }

  private validFileName(file: any) {
    const modelName = file.name.substring(0, file.name.indexOf('.' + this.fileExtension));
    const isFileInQueue = this.files
      .map(value => value.name)
      .includes(file.name);
    let uploadedModelsFromLocalStorage: Array<UploadedModel> =
      JSON.parse(localStorage.getItem(this.secureVariablesService.modelsInLocalStorageLabel));
    uploadedModelsFromLocalStorage = uploadedModelsFromLocalStorage !== null ? uploadedModelsFromLocalStorage : [];
    const isFileAlreadyUploaded = uploadedModelsFromLocalStorage
      .map(value => value.name)
      .includes(modelName);
    return !this.notAllowedNames.includes(modelName, 0) && !isFileInQueue && !isFileAlreadyUploaded;
  }
}
