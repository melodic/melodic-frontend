import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AppConfigService} from '../../app-config/service/app-config.service';
import {SecureVariable} from '../model/secure-variable';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {SecureVariablesExistence, UploadedModel} from '../../deploying-application/model/uploaded-model';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class SecureVariablesService {

  apiUrl = `${AppConfigService.settings.apiUrl}/auth/deployment/secure/variable`;
  modelsInLocalStorageLabel = 'uploadedModels';

  constructor(private http: HttpClient) {
  }

  saveSecureVariables(secureVariablesRequest: Array<SecureVariable>): Observable<Array<string>> {
    return this.http.post(this.apiUrl, JSON.stringify(secureVariablesRequest), httpOptions).pipe(
      tap((response: Array<string>) => console.log(`Secure variables stored:`, response),
        e => console.log(`Error by saving secure variables:`, e))
    );
  }

  removeSecureExistenceFromModelInLocalStorage(modelName: string) {
    const uploadedModelsFromLocalStorage: Array<UploadedModel> = JSON.parse(localStorage.getItem(this.modelsInLocalStorageLabel));
    uploadedModelsFromLocalStorage
      .find(valueFormLocalStorage => valueFormLocalStorage.name === modelName).secureVariablesToDefine = [];
    localStorage.setItem(this.modelsInLocalStorageLabel, JSON.stringify(uploadedModelsFromLocalStorage));
  }

  addUploadedModelToLocalStorage(modelName: string, sensitiveVariables: Array<SecureVariable>) {
    const uploadedModelsFromLocalStorage = JSON.parse(localStorage.getItem(this.modelsInLocalStorageLabel));
    const uploadedModels: Array<UploadedModel> = uploadedModelsFromLocalStorage === null ? [] : uploadedModelsFromLocalStorage;
    uploadedModels.push(new UploadedModel(modelName, sensitiveVariables, null));
    localStorage.setItem(this.modelsInLocalStorageLabel, JSON.stringify(uploadedModels));
  }

  removeInfoFromLocalStorageAboutFilesNotFromCdo(modelFromCdoNames: Array<string>,
                                                 uploadedModelsFromLocalStorage: Array<UploadedModel>) {
    uploadedModelsFromLocalStorage = uploadedModelsFromLocalStorage
      .filter(modelFromLocalStorageName => modelFromCdoNames.includes(modelFromLocalStorageName.name, 0));
    localStorage.setItem(this.modelsInLocalStorageLabel, JSON.stringify(uploadedModelsFromLocalStorage));
  }

  isAnyModelWithMissingSecureVariables() {
    const uploadedModelsFromLocalStorage: Array<UploadedModel> = JSON.parse(localStorage.getItem(this.modelsInLocalStorageLabel));
    if (uploadedModelsFromLocalStorage === null) {
      return false;
    } else {
      return uploadedModelsFromLocalStorage
        .find(model => model.secureVariablesExistence === SecureVariablesExistence.YES
          && model.secureVariablesToDefine.length !== 0);
    }
  }

  findModelInLocalStorage(modelName: string, uploadedModelsFromLocalStorage: Array<UploadedModel>): UploadedModel {
    return uploadedModelsFromLocalStorage.find(valueFormLocalStorage => valueFormLocalStorage.name === modelName);
  }

  mapUploadedModelNamesToModels(value: Array<string>): Array<UploadedModel> {
    const uploadedModelsTmp = [];
    let uploadedModelsFromLocalStorage = JSON.parse(localStorage.getItem(this.modelsInLocalStorageLabel));
    uploadedModelsFromLocalStorage = uploadedModelsFromLocalStorage !== null ? uploadedModelsFromLocalStorage : [];

    value.forEach(modelName => {
      const modelFromLocalStorage: UploadedModel =
        this.findModelInLocalStorage(modelName, uploadedModelsFromLocalStorage);
      if (modelFromLocalStorage) {
        console.log('Model info from local storage');
        uploadedModelsTmp.push(new UploadedModel(modelFromLocalStorage.name, modelFromLocalStorage.secureVariablesToDefine,
          modelFromLocalStorage.secureVariablesExistence));
      } else {
        console.log('Model does not exist in LS');
        uploadedModelsTmp.push(new UploadedModel(modelName, null, null));
      }
    });
    return uploadedModelsTmp;
  }
}
