import {TestBed} from '@angular/core/testing';

import {SecureVariablesService} from './secure-variables.service';

describe('SecureVariablesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SecureVariablesService = TestBed.get(SecureVariablesService);
    expect(service).toBeTruthy();
  });
});
