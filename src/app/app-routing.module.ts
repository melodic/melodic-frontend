import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonUserAdminRoleGuard} from './security-config/common-user-admin-role.guard';

const routes: Routes = [
  {
    path: 'deploying', loadChildren: './deploying-application/deploying-application.module#DeployingApplicationModule',
    canActivate: [CommonUserAdminRoleGuard]
  },

  {
    path: 'process', loadChildren: './process/process.module#ProcessModule',
    canActivate: [CommonUserAdminRoleGuard]
  },

  {
    path: 'provider', loadChildren: './provider/provider.module#ProviderModule',
    canActivate: [CommonUserAdminRoleGuard]
  },

  {path: 'user', loadChildren: './user/user.module#UserModule'},

  {
    path: 'application', loadChildren: './application/application.module#ApplicationModule',
    canActivate: [CommonUserAdminRoleGuard]
  },

  {
    path: 'byon', loadChildren: './byon/byon.module#ByonModule',
    canActivate: [CommonUserAdminRoleGuard]
  },

  {
    path: 'simulation', loadChildren: './simulation/simulation.module#SimulationModule',
    canActivate: [CommonUserAdminRoleGuard]
  },

  {
    path: 'serverless-testing', loadChildren: './serverless-testing/serverless-testing.module#ServerlessTestingModule',
    canActivate: [CommonUserAdminRoleGuard]
  },

  {path: '**', loadChildren: './user/user.module#UserModule'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
