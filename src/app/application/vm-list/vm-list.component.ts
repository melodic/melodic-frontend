import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {NodeCloudiator} from '../model/node-cloudiator';
import {ApplicationService} from '../service/application.service';
import {IpAddressType} from '../model/ip-address';
import {Cloud} from '../../process/process-details/offer/model/cloud';
import {ProcessOfferService} from '../../process/process-details/offer/service/process-offer.service';
import {UserService} from '../../user/service/user.service';
import {UserRole} from '../../user/model/user-role.enum';
import {OfferLocation} from '../../process/process-details/offer/model/offer-location';
import {WebSshService} from '../service/web-ssh.service';

@Component({
  selector: 'app-vm-list',
  templateUrl: './vm-list.component.html',
  styleUrls: ['./vm-list.component.css']
})
export class VmListComponent implements OnInit {

  @Input() vms: NodeCloudiator[];
  cloudsList: Cloud[];
  locationList: OfferLocation[];
  loadingVmListInProgress = false;

  data: MatTableDataSource<NodeCloudiator>;
  displayedColumns: string[] = ['no', 'name', 'id', 'state', 'publicIP', 'privateIP', 'providerName', 'city', 'diagnostic', 'sshButton'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private applicationService: ApplicationService,
              private processOfferService: ProcessOfferService,
              private snackBar: MatSnackBar,
              private userService: UserService,
              private webSshService: WebSshService) {
  }

  ngOnInit() {
    this.loadingVmListInProgress = true;
    this.processOfferService.getCloudList().subscribe(cloudsResponse => {
      this.cloudsList = cloudsResponse;
      this.processOfferService.getLocationList().subscribe(locationResponse => {
          this.locationList = locationResponse;
          this.applicationService.getVmNodeList().subscribe(vmNodesResponse => {
              this.vms = vmNodesResponse;
              this.loadingVmListInProgress = false;
              this.updateTableData();
              console.log('Successfully getting VM list');
            },
            error1 => {
              this.vms = [];
              this.loadingVmListInProgress = false;
              this.updateTableData();
              this.snackBar.open(`Problem by getting VMs list: ${error1.error.message}`, 'Close');
        });
        },
        error1 => {
          this.locationList = [];
          this.loadingVmListInProgress = false;
          this.updateTableData();
          this.snackBar.open(`Problem by getting data about locations: ${error1.error.message}`, 'Close');
        });
    }, error1 => {
      console.log('Error by getting clouds for VMs list: ', error1);
      this.vms = [];
      this.loadingVmListInProgress = false;
      this.updateTableData();
      this.snackBar.open(`Problem by getting data about clouds: ${error1.error.message}`, 'Close');
    });
  }

  updateTableData() {
    this.vms.forEach(value => {
      value.provider = this.getProviderName(value);
      value.privateIp = this.getPrivateIp(value);
      value.publicIp = this.getPublicIp(value);
      value.city = this.getCity(value);
    });
    this.data = new MatTableDataSource<NodeCloudiator>(this.vms);
    this.data.paginator = this.paginator;
    this.data.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.data.filter = filterValue.trim();

    if (this.data.paginator) {
      this.data.paginator.firstPage();
    }
  }

  getPublicIp(vm: NodeCloudiator): string {
    return vm.ipAddresses
      .find(value => value.ipAddressType.toString() === IpAddressType[IpAddressType.PUBLIC_IP])
      .value;
  }

  getPrivateIp(vm: NodeCloudiator): string {
    const privateIp = vm.ipAddresses
      .find(value => value.ipAddressType.toString() === IpAddressType[IpAddressType.PRIVATE_IP]);
    return privateIp ? privateIp.value : 'UNKNOWN';
  }

  getProviderName(vm: NodeCloudiator): string {
    const provider = this.cloudsList
      .find(value => value.id === vm.nodeProperties.providerId);
    return provider ? provider.api.providerName : 'UNKNOWN';
  }

  getCity(vm: NodeCloudiator): string {
    if (vm.nodeProperties.geoLocation != null && vm.nodeProperties.geoLocation.city != null) {
      return vm.nodeProperties.geoLocation.city;
    } else { // find location by locationId
      const locationId = vm.originId.split('/')[0];
      const location = this.locationList
        .find(value => value.id === locationId);
      return (location && location.geoLocation) ? location.geoLocation.city : 'UNKNOWN';
    }
  }

  userHasPermissionToSshConnection(vm: NodeCloudiator): boolean {
    return UserRole.ADMIN === UserRole[this.userService.currentUser.userRole];
  }

  onSshConnectionClick(vm: NodeCloudiator) {
    this.webSshService.createSshConnection(vm.loginCredential, vm.publicIp, vm.name);
  }
}
