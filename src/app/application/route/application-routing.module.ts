import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {VmListComponent} from '../vm-list/vm-list.component';
import {FunctionListComponent} from '../function-list/function-list.component';
import {ApplicationMainComponent} from '../application-main/application-main.component';
import {GrafanaViewComponent} from '../grafana-view/grafana-view.component';

const routes: Routes = [
  {path: '', component: ApplicationMainComponent},
  {path: 'vm', component: VmListComponent},
  {path: 'function', component: FunctionListComponent},
  {path: 'grafana', component: GrafanaViewComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationRoutingModule {
}
