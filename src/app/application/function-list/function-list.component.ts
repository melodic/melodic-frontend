import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Faas} from '../model/faas';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {ApplicationService} from '../service/application.service';
import {ProcessOfferService} from '../../process/process-details/offer/service/process-offer.service';
import {Cloud} from '../../process/process-details/offer/model/cloud';
import {NodeCloudiator} from '../model/node-cloudiator';
import {OfferLocation} from '../../process/process-details/offer/model/offer-location';

@Component({
  selector: 'app-function-list',
  templateUrl: './function-list.component.html',
  styleUrls: ['./function-list.component.css']
})
export class FunctionListComponent implements OnInit {

  @Input() faasList: Faas[];
  cloudsList: Cloud[];
  faasNodeList: NodeCloudiator[];
  locationList: OfferLocation[];
  loadingFaasListInProgress = false;

  data: MatTableDataSource<Faas>;
  displayedColumns: string[] = ['no', 'name', 'id', 'state', 'provider', 'locationId', 'memory', 'runtime', 'stackId', 'city'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private applicationService: ApplicationService,
              private processOfferService: ProcessOfferService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.loadingFaasListInProgress = true;
    this.processOfferService.getCloudList().subscribe(cloudsResponse => {
      this.cloudsList = cloudsResponse;
      this.processOfferService.getLocationList().subscribe(locationResponse => {
          this.locationList = locationResponse;
          this.applicationService.getFaasNodeList().subscribe(faasNodeResponse => {
              this.faasNodeList = faasNodeResponse;
              this.applicationService.getFunctionList().subscribe(functionsResponse => {
                  this.faasList = functionsResponse;
                  this.loadingFaasListInProgress = false;
                  this.updateTableData();
                  console.log('Successfully getting function list');
                },
                error1 => {
                  this.faasList = [];
                  this.loadingFaasListInProgress = false;
                  this.updateTableData();
                  this.snackBar.open(`Problem by getting functions list: ${error1.error.message}`, 'Close');
                });
            },
            error1 => {
              this.faasList = [];
              this.loadingFaasListInProgress = false;
              this.updateTableData();
              this.snackBar.open(`Problem by getting FAAS nodes list: ${error1.error.message}`, 'Close');
            });
        },
        error1 => {
          this.locationList = [];
          this.loadingFaasListInProgress = false;
          this.updateTableData();
          this.snackBar.open(`Problem by getting data about locations: ${error1.error.message}`, 'Close');
        });
    }, error1 => {
      console.log('Error by getting clouds for functions list: ', error1);
      this.faasList = [];
      this.loadingFaasListInProgress = false;
      this.updateTableData();
      this.snackBar.open(`Problem by getting data about functions: ${error1.error.message}`, 'Close');
    });
  }

  updateTableData() {
    this.faasList.forEach(value => {
      value.name = this.getFunctionName(value);
      value.provider = this.getProviderName(value);
      value.state = this.getFunctionState(value);
      value.city = this.getCity(value);
    });
    this.data = new MatTableDataSource<Faas>(this.faasList);
    this.data.paginator = this.paginator;
    this.data.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.data.filter = filterValue.trim();

    if (this.data.paginator) {
      this.data.paginator.firstPage();
    }
  }

  getProviderName(faas: Faas): string {
    return this.cloudsList
      .find(value => value.id === faas.cloudId)
      .api.providerName;
  }

  getFunctionName(faas: Faas): string {
    return this.faasNodeList
      .find(value => value.originId === faas.id)
      .name;
  }

  getFunctionState(faas: Faas): string {
    return this.faasNodeList
      .find(value => value.originId === faas.id)
      .state;
  }

  private getCity(faas: Faas) {
    return this.locationList
      .find(location => location.id === faas.locationId)
      .geoLocation.city;
  }
}
