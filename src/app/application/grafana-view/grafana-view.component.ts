import {Component, OnInit} from '@angular/core';
import {AppConfigService} from '../../app-config/service/app-config.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-grafana-view',
  templateUrl: './grafana-view.component.html',
  styleUrls: ['./grafana-view.component.css']
})
export class GrafanaViewComponent implements OnInit {

  grafanaRedirectionInProgress = false;

  constructor(private router: Router) {
  }

  ngOnInit() {
    localStorage.setItem('viewTitle', 'Grafana');
    this.grafanaRedirectionInProgress = true;
    window.open(`${AppConfigService.settings.grafanaUrl}`);
    this.router.navigate(['/application']);
  }
}
