import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GrafanaViewComponent} from './grafana-view.component';

describe('GrafanaViewComponent', () => {
  let component: GrafanaViewComponent;
  let fixture: ComponentFixture<GrafanaViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GrafanaViewComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrafanaViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
