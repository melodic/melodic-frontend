export class LoginCredential {
  id: number;
  username: string;
  password: string;
  privateKey: string;
}
