import {OperatingSystem} from '../../process/process-details/offer/model/operating-system';
import {GeoLocation} from '../../process/process-details/offer/model/geo-location';

export class NodeProperties {
  id: number;
  providerId: string;
  numberOfCores: number;
  memory: number;
  disk: number;
  operatingSystem: OperatingSystem;
  geoLocation: GeoLocation;
}
