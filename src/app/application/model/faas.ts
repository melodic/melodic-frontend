export class Faas {
  id: string;
  cloudId: string;
  locationId: string;
  memory: number;
  runtime: string;
  stackId: string;
  name: string;
  state: string;
  provider: string;
  city: string;
}
