import {LoginCredential} from './login-credential';
import {IpAddress} from './ip-address';
import {NodeProperties} from './node-properties';

export class NodeCloudiator {
  id: string;
  originId: string;
  name: string;
  state: string;
  nodeType: string;
  loginCredential: LoginCredential;
  ipAddresses: Array<IpAddress>;
  nodeProperties: NodeProperties;
  diagnostic: string;
  provider: string;
  publicIp: string;
  privateIp: string;
  city: string;
}
