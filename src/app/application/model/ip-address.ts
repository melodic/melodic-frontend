export enum IpAddressType {
  PRIVATE_IP, PUBLIC_IP
}

export enum IpVersion {
  V4, V6
}

export class IpAddress {
  id: number;
  ipAddressType: IpAddressType;
  ipVersion: string;
  value: string;
}
