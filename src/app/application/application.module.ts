import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FunctionListComponent} from './function-list/function-list.component';
import {VmListComponent} from './vm-list/vm-list.component';
import {ApplicationMainComponent} from './application-main/application-main.component';
import {AngularMaterialModule} from '../angular-material/angular-material.module';
import {ApplicationRoutingModule} from './route/application-routing.module';
import {GrafanaViewComponent} from './grafana-view/grafana-view.component';

@NgModule({
  declarations: [
    VmListComponent,
    FunctionListComponent,
    ApplicationMainComponent,
    GrafanaViewComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    ApplicationRoutingModule
  ]
})
export class ApplicationModule {
}
