import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    ContentType: 'application/json',
    Authorization: 'Basic ' + btoa('user:password'),
    Accept: '*/*'
  }),
  withCredentials: true
};

@Injectable({
  providedIn: 'root'
})
export class GrafanaService {

  // todo change this service after adding Nginx for Grafana
  constructor(private http: HttpClient) {
  }

  login(username: string, password: string): Observable<any> {
    // todo change Authorization header
    const requestUrl = `http://158.39.75.33:3000/api/org`; // todo change from variable
    return this.http.get(requestUrl, httpOptions).pipe(
      tap(response => {
          console.log('Successful Grafana username with response: ', response);
        },
        e => console.log('Error by username to Grafana', e))
    );
  }
}
