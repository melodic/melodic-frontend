import {Injectable} from '@angular/core';
import {AppConfigService} from '../../app-config/service/app-config.service';
import {LoginCredential} from '../model/login-credential';

@Injectable({
  providedIn: 'root'
})
export class WebSshService {

  constructor() {
  }

  createSshConnection(loginCredential: LoginCredential, publicIp: string, vmName: string) {
    console.log(`Create SSH connection for vm: ${vmName}`);
    if (loginCredential.privateKey) {
      this.connectBySshWithKey(loginCredential, publicIp);
    } else {
      this.connectBySshWithPassword(loginCredential, publicIp);
    }
  }

  private connectBySshWithKey(loginCredential: LoginCredential, publicIp: string) {
    const formattedPrivateKey = this.formatKey(loginCredential.privateKey);
    const webSshUrl = `${AppConfigService.settings.webSshUrl}/?hostname=${publicIp}&username=${loginCredential.username}` +
      `&privatekey=${formattedPrivateKey}`;
    console.log(`Key-based SSH connection under url: ${webSshUrl}`);
    window.open(webSshUrl);
  }

  private connectBySshWithPassword(loginCredential: LoginCredential, publicIp: string) {
    const webSshUrl = `${AppConfigService.settings.webSshUrl}/?hostname=${publicIp}&username=${loginCredential.username}` +
      `&password=${btoa(loginCredential.password)}`;
    console.log(`Password-based SSH connection under address: ${webSshUrl}`);
    window.open(webSshUrl);
  }

  private formatKey(privateKey: string): string {
    const lines = privateKey.split('\n');
    let result = '';
    for (const keyLine of lines) {
      result += keyLine + '\\n';
    }
    return result.substring(0, result.length - 2); // remove last '\n' sign
  }
}
