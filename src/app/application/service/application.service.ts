import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AppConfigService} from '../../app-config/service/app-config.service';
import {Observable} from 'rxjs';
import {NodeCloudiator} from '../model/node-cloudiator';
import {tap} from 'rxjs/operators';
import {Faas} from '../model/faas';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  apiUrl = `${AppConfigService.settings.apiUrl}/auth/application`;

  constructor(private http: HttpClient) {
  }

  getVmNodeList(): Observable<Array<NodeCloudiator>> {
    return this.http.get(`${this.apiUrl}/node/vm`, httpOptions).pipe(
      tap((response: Array<NodeCloudiator>) => {
          console.log(`Get VM node list with ${response.length} elements`);
        },
        e => console.log('Error by getting VM node list:', e))
    );
  }

  getFaasNodeList(): Observable<Array<NodeCloudiator>> {
    return this.http.get(`${this.apiUrl}/node/faas`, httpOptions).pipe(
      tap((response: Array<NodeCloudiator>) => {
          console.log(`Get FAAS node list with ${response.length} elements`);
        },
        e => console.log('Error by getting FAAS node list:', e))
    );
  }

  getFunctionList(): Observable<Array<Faas>> {
    return this.http.get(`${this.apiUrl}/function`, httpOptions).pipe(
      tap((response: Array<Faas>) => {
          console.log(`Get functions list with ${response.length} elements`);
        },
        e => console.log('Error by getting functions list:', e))
    );
  }
}
