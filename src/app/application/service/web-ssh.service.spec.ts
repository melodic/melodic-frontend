import {TestBed} from '@angular/core/testing';

import {WebSshService} from './web-ssh.service';

describe('WebSshService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WebSshService = TestBed.get(WebSshService);
    expect(service).toBeTruthy();
  });
});
