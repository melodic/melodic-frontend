import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MenuComponent} from './menu/menu.component';
import {AngularMaterialModule} from '../angular-material/angular-material.module';
import {RouterModule} from '@angular/router';
import {ConfirmationDialogComponent} from './confirmation-dialog/confirmation-dialog.component';
import {AutoFocusDirective} from './directive/auto-focus.directive';
import {BlockUIModule} from 'ng-block-ui';

@NgModule({
  declarations: [
    MenuComponent,
    ConfirmationDialogComponent,
    AutoFocusDirective,
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    RouterModule,
    BlockUIModule.forRoot()
  ],
  exports: [
    MenuComponent,
    ConfirmationDialogComponent,
    AutoFocusDirective
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ]
})
export class CommonTemplateModule {
}
