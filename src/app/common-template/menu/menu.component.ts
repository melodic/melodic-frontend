import {AfterViewChecked, ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import {UserService} from '../../user/service/user.service';
import {UserRole} from '../../user/model/user-role.enum';
import {BlockUI, NgBlockUI} from 'ng-block-ui';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnDestroy, AfterViewChecked {

  @BlockUI() blockUI: NgBlockUI;
  mobileQuery: MediaQueryList;

  private readonly mobileQueryListener: () => void;

  constructor(private changeDetectorRef: ChangeDetectorRef,
              private media: MediaMatcher,
              private userService: UserService) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this.mobileQueryListener);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this.mobileQueryListener);
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  isMenuVisible() {
    return this.getCurrentViewTitle() !== 'Login';
  }

  getCurrentViewTitle(): string {
    return localStorage.getItem('viewTitle');
  }

  onLogOutClick() {
    console.log('Logout click');
    this.userService.logout(undefined);
  }

  isAdmin(): boolean {
    if (this.userService.currentUser !== null) {
      return UserRole.ADMIN === UserRole[this.userService.currentUser.userRole];
    } else {
      return false;
    }
  }

  blockUIView(message: string) {
    this.blockUI.start(message);
  }

  unblockUIView() {
    this.blockUI.stop();
  }
}
