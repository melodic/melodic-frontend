import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {UserService} from '../user/service/user.service';
import {User} from '../user/model/user';
import {UserRole} from '../user/model/user-role.enum';

@Injectable({
  providedIn: 'root'
})
export class CommonUserAdminRoleGuard implements CanActivate {

  constructor(
    private router: Router,
    private userService: UserService
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser: User = this.userService.currentUser;
    if (this.userService.currentUser &&
      (UserRole.USER === UserRole [currentUser.userRole] || UserRole.ADMIN === UserRole [currentUser.userRole])) {
      return true;
    }

    this.router.navigate(['/user/login'], {queryParams: {returnUrl: state.url}});
    return false;
  }
}
