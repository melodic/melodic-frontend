import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {UserService} from '../user/service/user.service';
import {UserRole} from '../user/model/user-role.enum';
import {User} from '../user/model/user';

@Injectable({
  providedIn: 'root'
})
export class AdminRoleGuard implements CanActivate {

  constructor(
    private router: Router,
    private userService: UserService
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser: User = this.userService.currentUser;
    if (this.userService.currentUser && UserRole.ADMIN === UserRole [currentUser.userRole]) {
      return true;
    }

    this.router.navigate(['/user/login'], {queryParams: {returnUrl: state.url}});
    return false;
  }
}
