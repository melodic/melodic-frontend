import {ParentProperty} from '../parent-property';

export class CloudConfigurationForRead {
  id?: number;
  nodeGroup: string;
  properties: Array<ParentProperty>;
}
