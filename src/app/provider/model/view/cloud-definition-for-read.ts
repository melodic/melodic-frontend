import {CloudType} from '../cloud-type';
import {Api} from '../api';
import {Credential} from '../credential';
import {CloudConfigurationForRead} from './cloud-configuration-for-read';

export class CloudDefinitionForRead {
  id?: number;
  endpoint: string;
  cloudType: CloudType;
  api: Api;
  credential: Credential;
  cloudConfiguration: CloudConfigurationForRead;
  checked?: boolean;
}
