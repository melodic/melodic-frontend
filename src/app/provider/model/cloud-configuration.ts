export class CloudConfiguration {
  id?: number;
  nodeGroup: string;
  properties: {};

  constructor(nodeGroupArg: string, propertiesArg: {}, idArg?: number) {
    this.id = idArg;
    this.nodeGroup = nodeGroupArg;
    this.properties = propertiesArg;
  }
}
