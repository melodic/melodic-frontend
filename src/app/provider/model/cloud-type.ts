export enum CloudType {
  PUBLIC,
  PRIVATE
}

export namespace CloudType {
  export function values() {
    return Object.keys(CloudType).filter(
      (type) => isNaN(<any> type) && type !== 'values'
    );
  }
}

