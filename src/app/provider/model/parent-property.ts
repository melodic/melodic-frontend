import {SingleProperty} from './single-property';

export class ParentProperty {
  id?: number;
  name: string;
  properties: Array<SingleProperty>;
  checked: boolean;
}
