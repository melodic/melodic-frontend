import {Api} from './api';
import {CloudConfiguration} from './cloud-configuration';
import {Credential} from './credential';
import {CloudType} from './cloud-type';

export class CloudDefinition {
  id?: number;
  endpoint: string;
  cloudType: CloudType;
  api: Api;
  credential: Credential;
  cloudConfiguration: CloudConfiguration;

  constructor(endpointArg: string, cloudTypeArg: CloudType, apiArg: Api, credentialArg: Credential,
              cloudConfigurationArg: CloudConfiguration, idArg?: number) {
    this.id = idArg;
    this.endpoint = endpointArg;
    this.cloudType = cloudTypeArg;
    this.api = apiArg;
    this.credential = credentialArg;
    this.cloudConfiguration = cloudConfigurationArg;
  }
}
