export class SingleProperty {
  id?: number;
  key: string;
  value: string;

  constructor(private keyArg: string, private valueArg: string, private idArg?: number) {
    this.id = idArg;
    this.key = keyArg;
    this.value = valueArg;
  }
}
