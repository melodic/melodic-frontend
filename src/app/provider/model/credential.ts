export class Credential {
  id?: number;
  user: string;
  secret: string;
}
