import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {CloudDefinitionForRead} from '../model/view/cloud-definition-for-read';
import {AppConfigService} from '../../app-config/service/app-config.service';
import {MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {ProviderSecretDialogComponent} from '../provider-secret-dialog/provider-secret-dialog.component';
import {SecureVariablesService} from '../../file-uploader/service/secure-variables.service';
import {SecureVariable} from '../../file-uploader/model/secure-variable';
import {ProviderEnums} from '../model/provider-enums';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ProviderService {

  apiUrl = `${AppConfigService.settings.apiUrl}/auth/provider`;
  providerSecretDialog: MatDialogRef<ProviderSecretDialogComponent>;

  constructor(private http: HttpClient,
              private dialog: MatDialog,
              private secureVariablesService: SecureVariablesService,
              private snackBar: MatSnackBar) {
  }

  getAllCloudDefinitions(): Observable<Array<CloudDefinitionForRead>> {
    return this.http.get(this.apiUrl, httpOptions).pipe(
      tap((response: Array<CloudDefinitionForRead>) => console.log(
        `Get response with all providersSettings with number: ${response.length}`
        ),
        e => console.log(`Error by getting cloud definitions for all providers`, e)
      ));
  }

  createNewCloudDefinition(newCloudDefinitionRequest: any): Observable<CloudDefinitionForRead> {
    return this.http.post(this.apiUrl, JSON.stringify(newCloudDefinitionRequest), httpOptions).pipe(
      tap((response: CloudDefinitionForRead) => console.log(
        `Response for new cloud definition with id: ${response.id} for provider: ${response.api.providerName}`
        ),
        e => console.log(`Error by creating new cloud definition`)
      ));
  }

  deleteCloudDefinition(cloudDefId: number): Observable<any> {
    return this.http.delete(`${this.apiUrl}/${cloudDefId}`, httpOptions).pipe(
      tap(response => console.log(
        `Successfully deleted cloud definition with id: ${cloudDefId}.`
        ),
        e => console.log('Error by deleting cloud definition:', e)
      ));
  }

  updateCloudDefinition(cloudDefId: number, updatedCloudDefinition: any): Observable<CloudDefinitionForRead> {
    return this.http.put(`${this.apiUrl}/${cloudDefId}`, JSON.stringify(updatedCloudDefinition), httpOptions).pipe(
      tap((response: CloudDefinitionForRead) => console.log(
        `Successfully updated cloud definition with id: ${cloudDefId}.`
        ),
        e => console.log('Error by updating cloud definition:', e)
      ));
  }

  openMissingSecureVariableDialog(secureVariableName: string) {
    console.log(`Open dialog for ${secureVariableName}`);
    this.providerSecretDialog = this.dialog.open(ProviderSecretDialogComponent, {
      hasBackdrop: false,
      disableClose: true,
      data: {
        variableName: secureVariableName
      },
      width: '50%'
    });

    this.providerSecretDialog.afterClosed().subscribe((result: SecureVariable) => {
      console.log(`The dialog with secret was closed with result: `, result);
      this.secureVariablesService.saveSecureVariables(Array.of(result))
        .subscribe(value => {
            location.reload();
            this.snackBar.open(`Secret with key ${value[0]} successfully stored`, 'Close');
          },
          error1 => {
            this.snackBar.open('Error by saving secret for provider: ', error1.error.message);
          });
    });
  }

  getProviderEnums(): Observable<ProviderEnums> {
    const requestUrl = `${this.apiUrl}/enum`;
    return this.http.get(requestUrl, httpOptions).pipe(
      tap((response: ProviderEnums) =>
          console.log(`Successfully get provider enums`),
        e => console.log(`Error by getting provider enums:`, e)
      ));
  }
}
