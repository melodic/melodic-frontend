import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CloudDefinitionFormComponent} from './cloud-definition-form.component';

describe('CloudDefinitionFormComponent', () => {
  let component: CloudDefinitionFormComponent;
  let fixture: ComponentFixture<CloudDefinitionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CloudDefinitionFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloudDefinitionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
