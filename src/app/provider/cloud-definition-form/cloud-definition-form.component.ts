import {Component, Inject, OnInit, Optional} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSnackBar, MatTableDataSource} from '@angular/material';
import {CloudDefinition} from '../model/cloud-definition';
import {CloudType} from '../model/cloud-type';
import {SingleProperty} from '../model/single-property';
import {CloudDefinitionForRead} from '../model/view/cloud-definition-for-read';
import {ParentProperty} from '../model/parent-property';
import {CloudPropertyFormComponent} from '../cloud-property-form/cloud-property-form.component';
import {ProviderService} from '../service/provider.service';
import {ConfirmationDialogComponent} from '../../common-template/confirmation-dialog/confirmation-dialog.component';
import {credentialsUserValidator} from '../validator/credential-user.validator';
import {requiredEndpointValidator} from '../validator/required-endpoint.validator';
import {ProviderEnums} from '../model/provider-enums';

@Component({
  selector: 'app-cloud-definition-form',
  templateUrl: './cloud-definition-form.component.html',
  styleUrls: ['./cloud-definition-form.component.css']
})
export class CloudDefinitionFormComponent implements OnInit {

  cloudDefinitionForm: FormGroup;
  apiForm: FormGroup;
  credentialForm: FormGroup;
  cloudConfigurationForm: FormGroup;
  cloudProperties: Array<ParentProperty>;
  cloudData: CloudDefinitionForRead;
  isReadMode: boolean;
  cloudPropertyDialog: MatDialogRef<CloudPropertyFormComponent>;
  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;
  secretHide = true;

  cloudType = CloudType.PUBLIC;
  providerEnums = new ProviderEnums();
  providerEnumsLoadingInProgress = false;
  nodeGroupPattern = '^[a-z0-9]{3,}$';

  data: MatTableDataSource<ParentProperty>;
  displayedColumns: string[] = ['name', 'properties', 'edit', 'delete'];
  displayedColumnsProperty: string[] = ['key', 'value'];

  constructor(private formBuilder: FormBuilder,
              private providerService: ProviderService,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              private dialogRef: MatDialogRef<CloudDefinitionFormComponent>,
              @Optional() @Inject(MAT_DIALOG_DATA) public dialogData?: any) {
  }

  ngOnInit() {
    if (this.dialogData) {
      this.cloudData = this.dialogData ? this.dialogData.data : undefined;
      this.isReadMode = this.dialogData ? this.dialogData.isReadMode : false;
    }

    this.providerEnumsLoadingInProgress = true;
    this.providerService.getProviderEnums().subscribe(providerEnumsResponse => {
        this.providerEnums = providerEnumsResponse;
        this.providerEnumsLoadingInProgress = false;
      },
      error1 => {
        this.providerEnumsLoadingInProgress = false;
        this.snackBar.open(`Error by getting available options for clouds form: ${error1.error.message}`, 'Close');
      });

    this.apiForm = this.formBuilder.group({
      id: this.cloudData ? this.cloudData.api.id : null,
      providerName: this.cloudData ? new FormControl({value: this.cloudData.api.providerName, disabled: this.isReadMode})
        : ['', Validators.required]
    });
    this.credentialForm = this.formBuilder.group({
      id: this.cloudData ? this.cloudData.credential.id : null,
      user: this.cloudData ? new FormControl({value: this.cloudData.credential.user, disabled: this.isReadMode},
        [Validators.required])
        : ['', [Validators.required]],
      secret: this.cloudData ? new FormControl({value: this.cloudData.credential.secret, disabled: this.isReadMode})
        : ['', Validators.required]
    });
    this.cloudConfigurationForm = this.formBuilder.group({
      id: this.cloudData ? this.cloudData.cloudConfiguration.id : null,
      nodeGroup: this.cloudData ? new FormControl({value: this.cloudData.cloudConfiguration.nodeGroup, disabled: this.isReadMode})
        : ['', [Validators.required, Validators.pattern(this.nodeGroupPattern)]],
      properties: this.cloudData ? this.cloudData.cloudConfiguration.properties : this.cloudProperties
    });
    this.cloudDefinitionForm = this.formBuilder.group({
      id: this.cloudData ? this.cloudData.id : null,
      endpoint: this.cloudData ? new FormControl({value: this.cloudData.endpoint, disabled: this.isReadMode})
        : [''],
      cloudType: this.cloudData ? new FormControl({value: this.cloudData.cloudType, disabled: this.isReadMode})
        : ['', Validators.required],
      api: this.apiForm,
      credential: this.credentialForm,
      cloudConfiguration: this.cloudConfigurationForm
    }, {
      validators: [credentialsUserValidator(this.dialogData.cloudDefinitions, this.apiForm, this.credentialForm, this.cloudData ?
        this.cloudData.credential.user : '', this.cloudData ? this.cloudData.api.providerName : ''),
        requiredEndpointValidator('endpoint', 'cloudType')]
    });

    if (this.cloudData) {
      this.cloudProperties = this.cloudData.cloudConfiguration.properties;
    } else {
      this.cloudProperties = [];
    }
    this.data = new MatTableDataSource<ParentProperty>(this.cloudProperties);
  }

  get form() {
    return this.cloudDefinitionForm.controls;
  }

  get apiFormControl() {
    return this.apiForm.controls;
  }

  get credentialFormControl() {
    return this.credentialForm.controls;
  }

  get cloudConfigurationFormControl() {
    return this.cloudConfigurationForm.controls;
  }

  getRequiredMsg() {
    return 'Mandatory field';
  }

  getInvalidCredentialsUserMessage() {
    return 'Such user for this provide already exists. User for provider must be unique.';
  }

  getMissingEndpointMessage() {
    return 'Required field for private provider';
  }

  getNodeGroupPatternMsg() {
    return 'Required min 3 sings length. Only lowercase and digits are allowed.';
  }

  saveCloudDefinition(cloudForm: NgForm) {
    this.cloudConfigurationFormControl.properties.setValue(this.cloudProperties);
    const cloudDefinition = <CloudDefinition> cloudForm.value;
    this.dialogRef.close(cloudDefinition);
    console.log(`Saving cloud definition for provider: ${cloudDefinition.api.providerName}`);
  }

  onAddCloudPropertyButtonClick() {
    this.openPropertyDialog();
  }

  onEditClick(row: ParentProperty) {
    console.log(`Edit cloud property with name: ${row.name} click`);
    this.openPropertyDialog(row);
  }

  onDeleteClick(row: ParentProperty) {
    console.log(`Delete cloud property with name: ${row.name} click`);
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      hasBackdrop: false,
      data: {
        title: 'Delete cloud property',
        message: `Cloud property: ${row.name} will be deleted. Do you want to continue?`
      },
      width: '30%'
    });

    this.confirmationDialog.afterClosed().subscribe(value => {
      if (value) {
        console.log(`Confirmed deleting of ${row.name}`);
        this.deleteCloudProperty(row);
      } else {
        console.log(`Cancel deleting of ${row.name}`);
      }
    });
  }

  createCloudPropertyData(row: ParentProperty): MatTableDataSource<SingleProperty> {
    return new MatTableDataSource<SingleProperty>(row.properties);
  }

  private openPropertyDialog(property?: ParentProperty) {
    this.cloudPropertyDialog = this.dialog.open(CloudPropertyFormComponent, {
      hasBackdrop: false,
      data: {
        property: property ? property : undefined,
        allDefinedProperties: this.cloudProperties
      },
      width: '100%'
    });

    this.cloudPropertyDialog.afterClosed().subscribe(result => {
      console.log(`Property dialog closed.`);
      if (result && !property) {
        console.log(`Closed dialog with new property with name: ${result.name}`);
        this.cloudProperties.push(result);
        this.data = new MatTableDataSource<ParentProperty>(this.cloudProperties);
      } else if (result && property) {
        console.log(`Closed dialog with edited property with name: ${result.name}`);
        const oldCloudPropertyId = this.cloudProperties.indexOf(property);
        this.cloudProperties[oldCloudPropertyId] = result;
        this.data = new MatTableDataSource<ParentProperty>(this.cloudProperties);
      }
    });
  }

  private deleteCloudProperty(row: ParentProperty) {
    const deletedPropertyId = this.cloudProperties.findIndex(value => value.id === row.id);
    this.cloudProperties.splice(deletedPropertyId, 1);
    this.data = new MatTableDataSource<ParentProperty>(this.cloudProperties);
  }
}
