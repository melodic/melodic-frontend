import {AbstractControl, ValidatorFn} from '@angular/forms';
import {SingleProperty} from '../model/single-property';

export function propertyKeyValidator(singleProperties: Array<SingleProperty>, oldKey: string): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const invalidKey = singleProperties.find(value => value.key === control.value && oldKey !== control.value);
    return invalidKey ? {invalidKey: {value: control.value}} : null;
  };
}
