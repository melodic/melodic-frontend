import {AbstractControl, ValidatorFn} from '@angular/forms';
import {ParentProperty} from '../model/parent-property';

export function propertyNameValidator(parentProperties: Array<ParentProperty>, oldName: string): ValidatorFn {
  return (control: AbstractControl): { [name: string]: any } | null => {
    const invalidName = parentProperties.find(value => value.name === control.value && oldName !== control.value);
    return invalidName ? {invalidName: {value: control.value}} : null;
  };
}
