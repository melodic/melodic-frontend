import {FormGroup, ValidatorFn} from '@angular/forms';
import {CloudType} from '../model/cloud-type';

export function requiredEndpointValidator(endpointKey: string, cloudTypeKey: string): ValidatorFn {
  return (group: FormGroup): { [key: string]: any } => {
    const endpoint = group.controls[endpointKey];
    const cloudType = group.controls[cloudTypeKey];
    if (cloudType.value === CloudType[CloudType.PRIVATE] && endpoint.value.trim() === '') {
      endpoint.setErrors({missingEndpoint: true});
      return {missingEndpoint: true};
    } else {
      endpoint.setErrors(null);
      return null;
    }
  };
}
