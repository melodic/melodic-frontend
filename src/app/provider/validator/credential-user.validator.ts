import {FormGroup, ValidatorFn} from '@angular/forms';
import {CloudDefinitionForRead} from '../model/view/cloud-definition-for-read';

export function credentialsUserValidator(cloudDefinitions: Array<CloudDefinitionForRead>, api: FormGroup, credentials: FormGroup,
                                         oldUser: string, oldProviderName: string): ValidatorFn {
  return (): { [key: string]: any } | null => {
    if (!cloudDefinitions) {
      return null;
    }
    const invalidCredentialsUser = cloudDefinitions.find(value => value.api.providerName === api.controls.providerName.value &&
      value.credential.user === credentials.controls.user.value && oldUser !== credentials.controls.user.value &&
      oldProviderName !== api.controls.providerName.value);
    if (invalidCredentialsUser) {
      credentials.controls.user.setErrors({invalidCredentialsUser: true});
    } else if (!credentials.controls.user.hasError('required')) {
      credentials.controls.user.setErrors(null);
    }
    return invalidCredentialsUser ? {invalidCredentialsUser: true} : null;
  };
}
