import {Component, Inject, OnInit, Optional} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SecureVariable} from '../../file-uploader/model/secure-variable';

@Component({
  selector: 'app-provider-secret-dialog',
  templateUrl: './provider-secret-dialog.component.html',
  styleUrls: ['./provider-secret-dialog.component.css']
})
export class ProviderSecretDialogComponent implements OnInit {

  providerSecretForm: FormGroup;
  secretHide = true;

  constructor(
    private dialogRef: MatDialogRef<ProviderSecretDialogComponent>,
    private formBuilder: FormBuilder,
    @Optional() @Inject(MAT_DIALOG_DATA) public dialogData?: any) {
  }

  ngOnInit() {
    this.providerSecretForm = this.formBuilder.group({
      name: new FormControl(this.dialogData.variableName),
      value: ['', Validators.required]
    });
  }

  get form() {
    return this.providerSecretForm.controls;
  }

  getVariableName() {
    return this.dialogData.variableName;
  }

  saveSecretVariable(secretVariableForm: NgForm) {
    console.log('Saving secure variable with result: ', secretVariableForm);
    this.dialogRef.close(<SecureVariable> secretVariableForm.value);
  }
}
