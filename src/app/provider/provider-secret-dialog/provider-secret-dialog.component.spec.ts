import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProviderSecretDialogComponent} from './provider-secret-dialog.component';

describe('ProviderSecretDialogComponent', () => {
  let component: ProviderSecretDialogComponent;
  let fixture: ComponentFixture<ProviderSecretDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProviderSecretDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderSecretDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
