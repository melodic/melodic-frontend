import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CloudDefinitionListComponent} from '../cloud-definition-list/cloud-definition-list.component';
import {CloudDefinitionFormComponent} from '../cloud-definition-form/cloud-definition-form.component';

const routes: Routes = [
  {path: 'cloud-definition', component: CloudDefinitionListComponent},
  {path: 'cloud-definition/:cloudDefId', component: CloudDefinitionFormComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderRoutingModule {
}
