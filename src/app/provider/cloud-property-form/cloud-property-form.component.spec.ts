import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CloudPropertyFormComponent} from './cloud-property-form.component';

describe('CloudPropertyFormComponent', () => {
  let component: CloudPropertyFormComponent;
  let fixture: ComponentFixture<CloudPropertyFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CloudPropertyFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloudPropertyFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
