import {Component, Inject, OnInit, Optional} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {SingleProperty} from '../model/single-property';
import {MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource} from '@angular/material';
import {ParentProperty} from '../model/parent-property';
import {ProviderService} from '../service/provider.service';
import {propertyKeyValidator} from '../validator/property-key.validator';
import {propertyNameValidator} from '../validator/property-name.validator';

@Component({
  selector: 'app-cloud-property-form',
  templateUrl: './cloud-property-form.component.html',
  styleUrls: ['./cloud-property-form.component.css']
})
export class CloudPropertyFormComponent implements OnInit {

  cloudPropertyForm: FormGroup;
  singlePropertyForm: FormGroup;
  displayedColumns: string[] = ['key', 'value', 'edit', 'delete'];
  singleProperties = new Array<SingleProperty>();
  parentProperties = new Array<ParentProperty>();
  singlePropertyFormTitle: string;
  data: MatTableDataSource<SingleProperty>;
  propertyData: ParentProperty;
  propertiesNameForSG = 'sword.default.securityGroup';

  editedPropertyKey: string;

  constructor(private formBuilder: FormBuilder,
              private providerService: ProviderService,
              private dialogRef: MatDialogRef<CloudPropertyFormComponent>,
              @Optional() @Inject(MAT_DIALOG_DATA) public dialogData) {
  }

  ngOnInit() {
    if (this.dialogData) {
      this.propertyData = this.dialogData.property;
      this.parentProperties = this.dialogData.allDefinedProperties;
    }

    this.singlePropertyFormTitle = 'New property';
    this.cloudPropertyForm = this.formBuilder.group({
      name: this.propertyData ? [this.propertyData.name, [Validators.required, propertyNameValidator(this.parentProperties,
        this.propertyData.name)]] : ['', [Validators.required, propertyNameValidator(this.parentProperties, '')]],
      properties: this.propertyData ? new FormControl({value: this.propertyData.properties}) : this.singleProperties
    });
    this.singleProperties = this.propertyData ? this.propertyData.properties : [];
    this.singlePropertyForm = this.createSinglePropertyForm();
    this.data = new MatTableDataSource<SingleProperty>(this.singleProperties);
  }

  createSinglePropertyForm(property?: SingleProperty) {
    if (property) {
      this.singlePropertyFormTitle = 'Edit property';
    } else {
      this.singlePropertyFormTitle = 'New property';
    }
    return this.formBuilder.group({
      key: property ? [property.key, [Validators.required, propertyKeyValidator(this.singleProperties, property.key)]] :
        ['', [Validators.required, propertyKeyValidator(this.singleProperties, '')]],
      value: property ? [property.value, Validators.required] : ['', Validators.required],
      id: property ? property.id : null
    });
  }

  get form() {
    return this.singlePropertyForm.controls;
  }

  get cloudDefForm() {
    return this.cloudPropertyForm.controls;
  }

  getInvalidKeyMessage() {
    return 'Such key for properties already exists';
  }

  getInvalidPropertyNameMessage() {
    return 'Such name for property for this user already exists';
  }

  getRequiredMsg() {
    return 'Mandatory field';
  }

  onSaveSinglePropertiesButtonClick(propertyForm: NgForm) {
    const property = new SingleProperty(propertyForm.value.key, propertyForm.value.value, propertyForm.value.id);
    if (this.editedPropertyKey && this.editedPropertyKey !== '') {
      console.log(`Edit single property with id: ${property.id}, key: ${propertyForm.value.key} value: ${propertyForm.value.value}`);
      const editedPropertyIndex = this.singleProperties.findIndex(value => value.key === this.editedPropertyKey);
      this.singleProperties[editedPropertyIndex] = property;
      this.editedPropertyKey = undefined;
    } else {
      console.log(`Create new single property: key: ${propertyForm.value.key} value: ${propertyForm.value.value}`);
      this.singleProperties.push(property);
    }
    this.data = new MatTableDataSource<SingleProperty>(this.singleProperties);
    this.singlePropertyForm = this.createSinglePropertyForm();
    this.editedPropertyKey = undefined;
  }

  onCancelSinglePropertiesButtonClick() {
    this.singlePropertyForm = this.createSinglePropertyForm();
    this.editedPropertyKey = undefined;
  }

  onEditClick(row: SingleProperty) {
    console.log(`Edit click for property with id: ${row.id} and key: ${row.key}`);
    this.editedPropertyKey = row.key;
    this.singlePropertyForm = this.createSinglePropertyForm(row);
  }

  onDeleteClick(row: SingleProperty) {
    console.log(`Delete click for property with id: ${row.id} and key: ${row.key}`);
    const deletedPropertyId = this.singleProperties.findIndex(value => value.key === row.key);
    this.singleProperties.splice(deletedPropertyId, 1);
    this.data = new MatTableDataSource<SingleProperty>(this.singleProperties);
  }

  saveCloudProperty(cloudPropertyForm: NgForm) {
    console.log(`Cloud property save click for property with name: ${cloudPropertyForm.name}`);
    const cloudProperty = <ParentProperty> cloudPropertyForm.value;
    cloudProperty.properties = this.singleProperties;
    this.dialogRef.close(cloudProperty);
    console.log(`Saving cloud property with name: ${cloudProperty.name}`);
  }
}
