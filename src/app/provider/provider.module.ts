import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProviderRoutingModule} from './route/provider-routing.module';
import {CloudDefinitionListComponent} from './cloud-definition-list/cloud-definition-list.component';
import {AngularMaterialModule} from '../angular-material/angular-material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CloudDefinitionFormComponent} from './cloud-definition-form/cloud-definition-form.component';
import {CloudPropertyFormComponent} from './cloud-property-form/cloud-property-form.component';
import {ProviderSecretDialogComponent} from './provider-secret-dialog/provider-secret-dialog.component';

@NgModule({
  declarations: [
    CloudDefinitionListComponent,
    CloudDefinitionFormComponent,
    CloudPropertyFormComponent,
    ProviderSecretDialogComponent
  ],
  imports: [
    CommonModule,
    ProviderRoutingModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    CloudDefinitionFormComponent,
    CloudPropertyFormComponent,
    ProviderSecretDialogComponent
  ],
  entryComponents: [
    CloudDefinitionFormComponent,
    CloudPropertyFormComponent,
    ProviderSecretDialogComponent
  ]
})
export class ProviderModule {
}
