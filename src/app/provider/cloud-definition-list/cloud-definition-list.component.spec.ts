import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CloudDefinitionListComponent} from './cloud-definition-list.component';

describe('CloudDefinitionListComponent', () => {
  let component: CloudDefinitionListComponent;
  let fixture: ComponentFixture<CloudDefinitionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CloudDefinitionListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloudDefinitionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
