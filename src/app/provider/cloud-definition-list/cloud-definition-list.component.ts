import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {CloudDefinition} from '../model/cloud-definition';
import {MatDialog, MatDialogRef, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {CloudDefinitionFormComponent} from '../cloud-definition-form/cloud-definition-form.component';
import {ProviderService} from '../service/provider.service';
import {ConfirmationDialogComponent} from '../../common-template/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-provider-credentials-list',
  templateUrl: './cloud-definition-list.component.html',
  styleUrls: ['./cloud-definition-list.component.css']
})
export class CloudDefinitionListComponent implements OnInit {

  @Input() cloudDefinitions: Array<CloudDefinition>;

  data: MatTableDataSource<CloudDefinition>;
  displayedColumns: string[] = ['no', 'provider', 'edit', 'delete'];
  cloudDefinitionDialog: MatDialogRef<CloudDefinitionFormComponent>;
  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;
  operationOnDataInProgress = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dialog: MatDialog,
              private providerService: ProviderService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    localStorage.setItem('viewTitle', 'Cloud definitions for providers');
    this.operationOnDataInProgress = true;
    this.providerService.getAllCloudDefinitions().subscribe(
      value => {
        this.cloudDefinitions = value;
        this.operationOnDataInProgress = false;
        this.updateCloudDefinitionsTable();
      },
      error1 => {
        this.operationOnDataInProgress = false;
        console.log('Error by getting cloud definitions for providers');
        this.snackBar.open(`Error by getting cloud definitions: ${error1.error.message}`, 'Close');
      });
  }

  onAddCloudButtonClick() {
    console.log('On add cloud button click');
    this.openCloudDefinitionDialog();
  }

  onRowClick(cloudDef: CloudDefinition) {
    console.log(`On row for provider cloud definition with id: ${cloudDef.id} and provider: ${cloudDef.api.providerName} click`);
    this.openCloudDefinitionDialog(cloudDef, true, false);
  }

  onEditClick(cloudDef: CloudDefinition) {
    console.log(`On edit for provider cloud definition with id: ${cloudDef.id} and provider: ${cloudDef.api.providerName} click`);
    this.openCloudDefinitionDialog(cloudDef, false, true);
  }

  onDeleteClick(cloudDef: CloudDefinition) {
    console.log(`On delete for provider cloud definition with id: ${cloudDef.id} and provider: ${cloudDef.api.providerName} click`);
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      hasBackdrop: false,
      data: {
        title: 'Delete cloud definition',
        message: `Cloud definition for provider: ${cloudDef.api.providerName} for user: ${cloudDef.credential.user} will be deleted.
         Do you want to continue?`
      },
      width: '30%'
    });

    this.confirmationDialog.afterClosed().subscribe(value => {
      if (value) {
        console.log(`Confirmed deleting of cloud definition for ${cloudDef.api.providerName}`);
        this.deleteCloudDefinition(cloudDef);
      } else {
        console.log(`Cancel deleting of cloud definition for ${cloudDef.api.providerName}`);
      }
    });
  }

  openCloudDefinitionDialog(definition?: CloudDefinition, isReadMode?: boolean, isEditMode?: boolean): void {
    this.cloudDefinitionDialog = this.dialog.open(CloudDefinitionFormComponent, {
      hasBackdrop: false,
      disableClose: true,
      data: {
        data: definition ? definition : undefined,
        isReadMode: isReadMode ? isReadMode : false,
        isEditMode: isEditMode ? isEditMode : false,
        cloudDefinitions: this.cloudDefinitions
      },
      width: '75%'
    });

    this.cloudDefinitionDialog.afterClosed().subscribe(result => {
      console.log(`The dialog was closed.`);
      if (result && !isReadMode && !isEditMode) {
        console.log(`Closed dialog with new cloud definition for provider: ${result.api.providerName}`);
        this.saveNewCloudDefinition(result);
      } else if (result && isEditMode) {
        console.log(`Closed dialog with edited cloud definition with id : ${result.id} for provider: ${result.api.providerName}`);
        this.updateCloudDefinition(result);
      }
    });
  }

  updateCloudDefinitionsTable() {
    this.data = new MatTableDataSource<CloudDefinition>(this.cloudDefinitions);
    this.data.paginator = this.paginator;

    this.data.sortingDataAccessor = this.data.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'provider':
          return item.api.providerName;
        default:
          return item[property];
      }
    };

    this.data.sort = this.sort;
  }

  private deleteCloudDefinition(cloudDef: CloudDefinition) {
    this.operationOnDataInProgress = true;
    this.providerService.deleteCloudDefinition(cloudDef.id).subscribe(value => {
        console.log(`Cloud def with id=${cloudDef.id} deleted`);
        this.snackBar.open(`Cloud definition for provider: ${cloudDef.api.providerName} successfully deleted`,
          'Close', {duration: 10000});
        this.getAllCloudDefinition();
      },
      error1 => {
        this.operationOnDataInProgress = false;
        console.log('Error by deleting cloud definition');
        this.snackBar.open(`Error by deleting cloud definition: ${error1.error.message}`, 'Close');
      });
  }

  private saveNewCloudDefinition(newCloudDef: any) {
    this.operationOnDataInProgress = true;
    this.providerService.createNewCloudDefinition(newCloudDef).subscribe(newCloudDefinitionResponse => {
        this.getAllCloudDefinition();
        console.log(`New cloud definition with id= ${newCloudDefinitionResponse.id} successfully created.`);
        this.snackBar.open(`New cloud definition for provider: ${newCloudDefinitionResponse.api.providerName} successfully created`,
          'Close', {duration: 10000});
      },
      error1 => {
        this.operationOnDataInProgress = false;
        console.log(`Error by creating cloud definition:`, error1);
        this.snackBar.open(`Error by creating new cloud definition: ${error1.error.message}`, 'Close');
      });
  }

  private getAllCloudDefinition() {
    this.operationOnDataInProgress = true;
    this.providerService.getAllCloudDefinitions().subscribe(
      value => {
        this.operationOnDataInProgress = false;
        this.cloudDefinitions = value;
        this.updateCloudDefinitionsTable();
      },
      error1 => {
        this.operationOnDataInProgress = false;
        console.log('Error by getting cloud definitions list');
        this.snackBar.open(`Error by getting cloud definitions list: ${error1.error.message}`, 'Close');
      });
  }

  private updateCloudDefinition(updatedCloudDef: CloudDefinition) {
    this.operationOnDataInProgress = true;
    this.providerService.updateCloudDefinition(updatedCloudDef.id, updatedCloudDef).subscribe(updatedCloudDefResponse => {
        this.getAllCloudDefinition();
        console.log(`Cloud definition with id= ${updatedCloudDefResponse.id} successfully updated.`);
        this.snackBar.open(`Cloud definition for provider: ${updatedCloudDefResponse.api.providerName} successfully updated`,
          'Close', {duration: 10000});
      },
      error1 => {
        this.operationOnDataInProgress = false;
        this.snackBar.open(`Error by updating cloud definition: ${error1.error.message}`, 'Close');
      }
    );
  }
}
