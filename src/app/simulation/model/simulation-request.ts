export class SimulationRequest {
  applicationId: string;
  metricValues: {};


  constructor(applicationId: string, metricValues: {}) {
    this.applicationId = applicationId;
    this.metricValues = metricValues;
  }
}
