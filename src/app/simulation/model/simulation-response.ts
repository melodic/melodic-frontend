import {SimulationState} from './simulation-state';

export class SimulationResponse {
  applicationId: string;
  result: SimulationState;
}
