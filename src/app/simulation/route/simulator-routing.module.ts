import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SimulationFormComponent} from '../simulation-form/simulation-form.component';

const routes: Routes = [
  {path: '', component: SimulationFormComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SimulatorRoutingModule {

}
