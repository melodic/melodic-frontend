import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SimulationFormComponent} from './simulation-form/simulation-form.component';
import {AngularMaterialModule} from '../angular-material/angular-material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SimulatorRoutingModule} from './route/simulator-routing.module';

@NgModule({
  declarations: [SimulationFormComponent],
  imports: [
    CommonModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SimulatorRoutingModule
  ]
})
export class SimulationModule {
}
