import {Injectable} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class MetricControlService {

  constructor() {
  }

  toFormGroup(metricNames: Array<string>): FormGroup {
    const group = {};
    metricNames.forEach(value => {
      group[value] = new FormControl('', Validators.required);
      group[value + 'Label'] = new FormControl({value, disabled: true});
    });
    return new FormGroup(group);
  }
}
