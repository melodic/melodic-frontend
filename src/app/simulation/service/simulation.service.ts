import {Injectable} from '@angular/core';
import {AppConfigService} from '../../app-config/service/app-config.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {MetricsNamesResponse} from '../model/metrics-names-response';
import {SimulationResponse} from '../model/simulation-response';
import {SimulationRequest} from '../model/simulation-request';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class SimulationService {

  apiUrl = `${AppConfigService.settings.apiUrl}/auth/simulation/metric`;

  constructor(private http: HttpClient) {
  }

  getMetricNamesList(applicationId: string): Observable<MetricsNamesResponse> {
    const requestUrl = `${this.apiUrl}/${applicationId}`;
    return this.http.get(requestUrl, httpOptions).pipe(
      tap((response: MetricsNamesResponse) => console.log(`Metrics names response for ${applicationId}: `, response),
        e => console.log(`Error by getting metric names list for ${applicationId}: `, e)
      )
    );
  }

  sendSimulationRequest(simulationRequest: SimulationRequest): Observable<SimulationResponse> {
    return this.http.post(this.apiUrl, simulationRequest, httpOptions).pipe(
      tap((response: SimulationResponse) => console.log(`Simulation started for application ${response.applicationId}`),
        e => console.log(`Error by sending simulation request for ${simulationRequest.applicationId}: `, e)
      )
    );
  }
}
