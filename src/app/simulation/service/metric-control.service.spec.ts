import {TestBed} from '@angular/core/testing';

import {MetricControlService} from './metric-control.service';

describe('MetricControlService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MetricControlService = TestBed.get(MetricControlService);
    expect(service).toBeTruthy();
  });
});
