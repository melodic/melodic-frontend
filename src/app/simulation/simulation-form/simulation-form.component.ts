import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {SimulationService} from '../service/simulation.service';
import {MetricControlService} from '../service/metric-control.service';
import {ProcessService} from '../../process/service/process.service';
import {ProcessState} from '../../process/model/process-state';
import {SimulationRequest} from '../model/simulation-request';
import {Router} from '@angular/router';

@Component({
  selector: 'app-simulation-form',
  templateUrl: './simulation-form.component.html',
  styleUrls: ['./simulation-form.component.css']
})
export class SimulationFormComponent implements OnInit {

  simulationForm: FormGroup;
  metricsForm: FormGroup;
  metricsNames = new Array<string>();
  applicationIdsLoadingInProgress = false;
  metricNamesLoadingInProgress = false;
  applicationIds = new Set<string>();

  constructor(private snackBar: MatSnackBar,
              private formBuilder: FormBuilder,
              private simulationService: SimulationService,
              private metricControlService: MetricControlService,
              private processService: ProcessService,
              private router: Router
  ) {
  }

  ngOnInit() {
    localStorage.setItem('viewTitle', 'Simulation');
    this.metricsForm = this.metricControlService.toFormGroup(this.metricsNames);
    this.simulationForm = this.formBuilder.group({
      applicationId: ['', Validators.required],
      metricValues: this.metricsForm
    });
    this.getFinishedApplicationIdsInSimulationMode();
  }

  get form() {
    return this.simulationForm.controls;
  }

  get metricsControl() {
    return this.metricsForm.controls;
  }

  onLoadMetricNamesClick() {
    const selectedAppId = this.form.applicationId.value;
    console.log(`On get metrics click for applicationId = ${selectedAppId}`);
    this.metricNamesLoadingInProgress = true;
    this.simulationService.getMetricNamesList(selectedAppId).subscribe(value => {
        this.metricNamesLoadingInProgress = false;
        this.metricsForm = this.metricControlService.toFormGroup(value.metricsNames);
        this.metricsNames = value.metricsNames;
        this.form.metricValues = this.metricsForm;
      },
      error1 => {
        this.snackBar.open(`Error by getting metric names list for application ${selectedAppId}: `, error1.error.message);
        this.metricNamesLoadingInProgress = false;
        this.metricsForm = this.metricControlService.toFormGroup([]);
        this.metricsNames = [];
        this.form.metricValues = this.metricsForm;
      });
  }

  isEmptyApplicationIdsList(): boolean {
    return this.applicationIds.size === 0;
  }

  isMetricsFormInvalid(): boolean {
    return this.metricsNames.length === 0 || this.metricsForm.invalid;
  }

  isEmptyMetricsList(): boolean {
    return this.metricsNames.length === 0;
  }

  getErrorRequiredMessage(): string {
    return `Field is required`;
  }

  sendMetrics(simulationForm: NgForm) {
    const applicationId = this.form.applicationId.value;
    console.log(`Metrics form by sending metrics for application ${applicationId}: `, this.metricsForm.value);
    const simulationRequest = new SimulationRequest(applicationId, this.metricsForm.value);
    this.simulationService.sendSimulationRequest(simulationRequest).subscribe(value => {
        this.snackBar.open(`Successfully started simulation for ${value.applicationId}`, 'Close');
        this.router.navigate([`/process`]);
      },
      error1 => {
        this.snackBar.open(`Error by starting simulation for ${simulationForm.value.applicationId}: ${error1.error.message}`, 'Close');
      });
  }

  private getFinishedApplicationIdsInSimulationMode() {
    this.applicationIdsLoadingInProgress = true;
    this.processService.getProcessesList().subscribe(processList => {
        this.applicationIdsLoadingInProgress = false;
        this.applicationIds = new Set(processList.filter(process =>
          process.processState.toString() === ProcessState[ProcessState.FINISHED] && process.simulation === true)
          .map(finishedProcess => finishedProcess.applicationId));
        console.log(`Number of applicationIds of finished processes = ${this.applicationIds.size}`);
        if (this.applicationIds.size > 0) {
          this.form.applicationId.setValue(this.applicationIds.values().next().value);
        }
      },
      error1 => {
        this.snackBar.open(`Error by getting application ids: `, error1.error.message);
        this.applicationIdsLoadingInProgress = false;
        this.applicationIds = new Set();
      });
  }
}
