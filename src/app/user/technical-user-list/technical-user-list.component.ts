import {Component, OnInit} from '@angular/core';
import {UserMainComponent} from '../user-main/user-main.component';
import {User} from '../model/user';

@Component({
  selector: 'app-technical-user-list',
  templateUrl: './technical-user-list.component.html',
  styleUrls: ['./technical-user-list.component.css']
})
export class TechnicalUserListComponent extends UserMainComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.userService.getTechnicalUserList().subscribe(value => {
        this.updateTableData(value);
        console.log('Technical user list successfully loaded');
      },
      error1 => {
        this.updateTableData([]);
        this.snackBar.open(`Problem by getting technical user list: ${error1.error.message}`, 'Close');
      });
  }

  onUnlockAccountClick(user: User) {
    console.log(`Unlock account for user ${user.username} click`);
    this.userService.unlockAccount(user.username).subscribe(value => {
        this.snackBar.open(`Account for user ${user.username} successfully unlocked.`, 'Close');
        this.getData();
      },
      error1 => {
        this.snackBar.open(`Error by unlocking account for user ${user.username}: ${error1.error.message}`);
      });
  }

  onCreateNewTechnicalUserClick() {
    console.log('Creating new technical user click');
    this.router.navigate(['/user/technical']);
  }

}
