import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TechnicalUserListComponent} from './technical-user-list.component';

describe('TechnicalUserListComponent', () => {
  let component: TechnicalUserListComponent;
  let fixture: ComponentFixture<TechnicalUserListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TechnicalUserListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicalUserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
