import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../service/user.service';
import {MatDialog, MatDialogRef, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {AppInjector} from '../../injector/app-injector';
import {User} from '../model/user';
import {Router} from '@angular/router';
import {ConfirmationDialogComponent} from '../../common-template/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-main.component.html',
  styleUrls: ['./user-main.component.css']
})
export class UserMainComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  protected userService: UserService;
  protected snackBar: MatSnackBar;
  protected router: Router;
  protected dialog: MatDialog;

  loadingDataInProgress = true;
  deletingUserInProgress = false;
  displayedColumns = ['no', 'username', 'unlockAccount', 'delete'];
  data: MatTableDataSource<User>;
  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;

  constructor() {
    const injector = AppInjector.getInjector();
    this.userService = injector.get(UserService);
    this.snackBar = injector.get(MatSnackBar);
    this.router = injector.get(Router);
    this.dialog = injector.get(MatDialog);
  }

  ngOnInit() {
    localStorage.setItem('viewTitle', 'Users list');
  }

  updateTableData(newData: any) {
    this.loadingDataInProgress = false;
    this.data = new MatTableDataSource(newData);
    this.data.paginator = this.paginator;
    this.data.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.data.filter = filterValue.trim();

    if (this.data.paginator) {
      this.data.paginator.firstPage();
    }
  }

  onDeleteClick(user: User) {
    console.log(`Delete click for user: ${user.username}`);
    this.confirmationDialog = this.createConfirmationDialog(user);

    this.confirmationDialog.afterClosed().subscribe(value => {
      if (value) {
        console.log(`Confirmed deleting of user ${user.username}.`);
        this.deleteUser(user);
      } else {
        console.log(`Cancel deleting of user ${user.username}`);
      }
    });
  }

  createConfirmationDialog(user: User): MatDialogRef<ConfirmationDialogComponent> {
    return this.dialog.open(ConfirmationDialogComponent, {
      hasBackdrop: false,
      data: {
        title: 'Deleting user account',
        message: `Account of user ${user.username} will be deleted.
         Do you want to continue?`
      },
      width: '30%'
    });
  }

  deleteUser(user: User) {
    this.deletingUserInProgress = true;
    this.userService.deleteUserAccount(user.username).subscribe(() => {
        this.snackBar.open(`Account of user ${user.username} successfully deleted`, 'Close');
        this.deletingUserInProgress = false;
        this.getData();
      },
      error1 => {
        this.snackBar.open(`Error by deleting account of user ${user.username}: ${error1.error.message}`, 'Close');
        this.deletingUserInProgress = false;
      });
  }

  getData() {
  }
}
