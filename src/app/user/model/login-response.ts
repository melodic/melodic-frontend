import {UserPublicData} from './user-public-data';

export class LoginResponse extends UserPublicData {
  token: string;
  refreshToken: string;
}
