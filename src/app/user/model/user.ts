import {UserRole} from './user-role.enum';

export class User {
  username: string;
  token: string;
  refreshToken: string;
  password: string;
  userRole: UserRole;
  lockedAccount: boolean;


  constructor(login: string, password: string, token: string, refreshToken: string, userRole: UserRole, lockedAccount: boolean) {
    this.username = login;
    this.token = token;
    this.refreshToken = refreshToken;
    this.password = password;
    this.userRole = userRole;
    this.lockedAccount = lockedAccount;
  }
}
