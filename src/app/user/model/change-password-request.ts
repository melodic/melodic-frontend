export class ChangePasswordRequest {
  username: string;
  oldPassword: string;
  newPassword: string;
  newPasswordConfirmation: string;
}
