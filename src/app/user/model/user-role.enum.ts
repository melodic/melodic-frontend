export enum UserRole {
  ADMIN = 'ADMIN',
  USER = 'USER',
  TECHNICAL_USER = 'TECHNICAL_USER'
}
