import {UserPublicData} from './user-public-data';

export class NewUserWithPasswordResponse extends UserPublicData {
  password: string;
}
