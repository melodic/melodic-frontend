import {UserRole} from './user-role.enum';

export class UserPublicData {
  username: string;
  userRole: UserRole;
}
