import {UserPublicData} from './user-public-data';

export class NewUserRequest extends UserPublicData {
  password: string;
  passwordConfirmation: string;
}
