import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UserService} from '../service/user.service';
import {MatSnackBar} from '@angular/material';
import {ChangePasswordRequest} from '../model/change-password-request';
import {mustMatchValidator} from '../validator/must-match.validator';
import {UserValidationMessagesService} from '../validator/user-validation-messages.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class ChangePasswordComponent implements OnInit {

  changePasswordForm: FormGroup;
  changingPasswordInProgress = false;
  currentPasswordHide = true;
  newPasswordHide = true;
  newPasswordConfirmationHide = true;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private userService: UserService,
              private snackBar: MatSnackBar,
              public messages: UserValidationMessagesService) {
  }

  ngOnInit() {
    localStorage.setItem('viewTitle', 'Change password');
    this.changePasswordForm = this.formBuilder.group({
      username: new FormControl({value: this.userService.currentUser.username, disabled: true}),
      oldPassword: ['', Validators.required],
      newPassword: ['', [Validators.required, Validators.minLength(8),
        Validators.pattern(this.userService.strongPasswordPattern)]],
      newPasswordConfirmation: ['', [Validators.required, Validators.minLength(8),
        Validators.pattern(this.userService.strongPasswordPattern)]],
    }, {
      validators: mustMatchValidator('newPassword', 'newPasswordConfirmation')
    });
  }

  get form() {
    return this.changePasswordForm.controls;
  }

  onSaveClick() {
    this.changingPasswordInProgress = true;
    const changePasswordRequest: ChangePasswordRequest = this.changePasswordForm.value;
    changePasswordRequest.username = this.userService.currentUser.username;
    this.userService.changePassword(changePasswordRequest).subscribe(() => {
        this.changingPasswordInProgress = false;
        this.snackBar.open('Your password successfully changed', 'Close');
        this.router.navigate(['/deploying']);
      },
      error1 => {
        this.changingPasswordInProgress = false;
        this.snackBar.open(`Error by changing password: ${error1.error.message}`, 'Close');
      });
  }

}
