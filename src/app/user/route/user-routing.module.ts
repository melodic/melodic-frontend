import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from '../login/login.component';
import {ChangePasswordComponent} from '../change-password/change-password.component';
import {UserMainComponent} from '../user-main/user-main.component';
import {AdminRoleGuard} from '../../security-config/admin-role.guard';
import {NewUserFormComponent} from '../new-user-form/new-user-form.component';
import {CommonUserAdminRoleGuard} from '../../security-config/common-user-admin-role.guard';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'password', component: ChangePasswordComponent, canActivate: [CommonUserAdminRoleGuard]},
  {path: '', component: UserMainComponent, canActivate: [AdminRoleGuard]},
  {path: 'admin', component: NewUserFormComponent, canActivate: [AdminRoleGuard]},
  {path: 'common', component: NewUserFormComponent, canActivate: [AdminRoleGuard]},
  {path: 'technical', component: NewUserFormComponent, canActivate: [AdminRoleGuard]},
  {path: '**', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {
}
