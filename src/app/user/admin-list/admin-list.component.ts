import {Component, OnInit} from '@angular/core';
import {UserMainComponent} from '../user-main/user-main.component';
import {User} from '../model/user';

@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.css']
})
export class AdminListComponent extends UserMainComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.userService.getAdminList().subscribe(value => {
        this.updateTableData(value);
        console.log('Admin list successfully loaded');
      },
      error1 => {
        this.updateTableData([]);
        this.snackBar.open(`Problem by getting admin list: ${error1.error.message}`, 'Close');
      });
  }

  onUnlockAccountClick(user: User) {
    console.log(`Unlock account for user ${user.username} click`);
    this.userService.unlockAccount(user.username).subscribe(value => {
        this.snackBar.open(`Account for user ${user.username} successfully unlocked.`, 'Close');
        this.getData();
      },
      error1 => {
        this.snackBar.open(`Error by unlocking account for user ${user.username}: ${error1.error.message}`);
      });
  }

  onCreateNewAdminUserClick() {
    console.log('On create new admin account click');
    this.router.navigate(['/user/admin']);
  }

  isCurrentUserAccount(user: User) {
    return this.userService.currentUser.username === user.username;
  }
}
