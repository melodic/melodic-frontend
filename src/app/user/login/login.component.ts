import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../service/user.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loginInProgress = false;
  returnUrl: string;
  passwordHide = true;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private userService: UserService,
              private snackBar: MatSnackBar) {

    // redirect logged in user to deployment page, after checking token validation
    if (this.userService.currentUser) {
      this.router.navigate(['deploying']);
    }
  }

  ngOnInit() {
    localStorage.setItem('viewTitle', 'Login');
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.returnUrl = this.route.snapshot.queryParams.returnUrl || 'deploying';
  }

  get form() {
    return this.loginForm.controls;
  }

  onLoginClick(loginForm: NgForm) {
    this.loginInProgress = true;
    console.log(`On login click for user: ${loginForm.value.username}`);
    this.userService.login(loginForm.value.username, loginForm.value.password).subscribe(value => {
        console.log('Successful login');
        this.router.navigate([this.returnUrl]);
        this.loginInProgress = false;
      },
      error1 => {
        this.loginInProgress = false;
        this.snackBar.open(`${error1.error.message}`, 'Close');
      });
  }
}
