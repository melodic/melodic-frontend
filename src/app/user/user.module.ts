import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {UserRoutingModule} from './route/user-routing.module';
import {AngularMaterialModule} from '../angular-material/angular-material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonTemplateModule} from '../common-template/common-template.module';
import {ChangePasswordComponent} from './change-password/change-password.component';
import {UserMainComponent} from './user-main/user-main.component';
import {AdminListComponent} from './admin-list/admin-list.component';
import {CommonUserListComponent} from './common-user-list/common-user-list.component';
import {NewUserFormComponent} from './new-user-form/new-user-form.component';
import {TechnicalUserListComponent} from './technical-user-list/technical-user-list.component';

@NgModule({
  declarations: [
    LoginComponent,
    ChangePasswordComponent,
    UserMainComponent,
    AdminListComponent,
    CommonUserListComponent,
    NewUserFormComponent,
    TechnicalUserListComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CommonTemplateModule
  ]
})
export class UserModule {
}
