import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserValidationMessagesService {

  constructor() {
  }

  requiredMessage = 'Required field';
  minLengthMessage = 'Required min 8 signs';
  patternMessage = 'Strong password is required: min 8 signs length, min 1 uppercase letter, min 1 small letter and min 1 digit, ' +
    'no white signs';

  validationMessages = {
    oldPassword: [
      {type: 'required', message: this.requiredMessage}
    ],
    newPassword: [
      {type: 'required', message: this.requiredMessage},
      {type: 'minLength', message: this.minLengthMessage},
      {type: 'pattern', message: this.patternMessage}
    ],
    newPasswordConfirmation: [
      {type: 'required', message: this.requiredMessage},
      {type: 'minLength', message: this.minLengthMessage},
      {type: 'pattern', message: this.patternMessage},
      {type: 'mustMatch', message: 'New password and new password confirmation must be equal'}
    ],
    username: [
      {type: 'required', message: this.requiredMessage},
      {type: 'invalidName', message: 'Such username is already in use'}
    ],
    password: [
      {type: 'required', message: this.requiredMessage},
      {type: 'minLength', message: this.minLengthMessage},
      {type: 'pattern', message: this.patternMessage}
    ],
    passwordConfirmation: [
      {type: 'required', message: this.requiredMessage},
      {type: 'minLength', message: this.minLengthMessage},
      {type: 'pattern', message: this.patternMessage},
      {type: 'mustMatch', message: 'Password and password confirmation must be equal'}
    ]
  };
}
