import {AbstractControl, ValidatorFn} from '@angular/forms';

export function uniqueUsernameValidator(currentNames: Array<string>): ValidatorFn {
  return (control: AbstractControl): { [username: string]: any } | null => {
    const invalidName = currentNames.find(value => value === control.value);
    return invalidName ? {invalidName: {value: control.value}} : null;
  };
}
