import {FormGroup, ValidatorFn} from '@angular/forms';

export function mustMatchValidator(targetKey: string, toMatchKey: string): ValidatorFn {
  return (group: FormGroup): { [key: string]: any } => {
    const target = group.controls[targetKey];
    const toMatch = group.controls[toMatchKey];
    if (target.touched || toMatch.touched) {
      const isMatch = target.value === toMatch.value;
      if (!isMatch && target.valid && toMatch.valid) {
        toMatch.setErrors({mustMatch: targetKey});
        return {mustMatch: true};
      }
      if (isMatch && toMatch.hasError('mustMatch')) {
        toMatch.setErrors(null);
      }
    }

    return null;
  };
}
