import {Component, OnInit} from '@angular/core';
import {UserMainComponent} from '../user-main/user-main.component';
import {User} from '../model/user';

@Component({
  selector: 'app-common-user-list',
  templateUrl: './common-user-list.component.html',
  styleUrls: ['./common-user-list.component.css']
})
export class CommonUserListComponent extends UserMainComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.userService.getCommonUserList().subscribe(value => {
        this.updateTableData(value);
        console.log('Common user list successfully loaded');
      },
      error1 => {
        this.updateTableData([]);
        this.snackBar.open(`Problem by getting common user list: ${error1.error.message}`, 'Close');
      });
  }

  onUnlockAccountClick(user: User) {
    console.log(`Unlock account for user ${user.username} click`);
    this.userService.unlockAccount(user.username).subscribe(value => {
        this.snackBar.open(`Account for user ${user.username} successfully unlocked.`, 'Close');
        this.getData();
      },
      error1 => {
        this.snackBar.open(`Error by unlocking account for user ${user.username}: ${error1.error.message}`);
      });
  }

  onCreateNewCommonUserClick() {
    console.log('On create new common user account click');
    this.router.navigate(['/user/common']);
  }
}
