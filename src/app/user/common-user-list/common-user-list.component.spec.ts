import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CommonUserListComponent} from './common-user-list.component';

describe('CommonUserListComponent', () => {
  let component: CommonUserListComponent;
  let fixture: ComponentFixture<CommonUserListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommonUserListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonUserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
