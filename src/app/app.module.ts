import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonTemplateModule} from './common-template/common-template.module';
import {RouterModule} from '@angular/router';
import {MatFileUploadModule} from 'angular-material-fileupload';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AppConfigService} from './app-config/service/app-config.service';
import {JwtInterceptor} from './security-config/jwt.interceptor';
import {ErrorInterceptor} from './error/error.interceptor';
import {MatSnackBarModule} from '@angular/material';
import {ProviderSecretDialogComponent} from './provider/provider-secret-dialog/provider-secret-dialog.component';
import {ProviderModule} from './provider/provider.module';

export function initializeApp(appConfigService: AppConfigService) {
  return (): Promise<any> => {
    return appConfigService.loadConfiguration();
  };
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CommonTemplateModule,
    RouterModule,
    MatFileUploadModule,
    MatSnackBarModule,
    HttpClientModule,
    ProviderModule
  ],
  entryComponents: [
    ProviderSecretDialogComponent
  ],
  providers: [
    AppConfigService,
    {provide: APP_INITIALIZER, useFactory: initializeApp, deps: [AppConfigService], multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
