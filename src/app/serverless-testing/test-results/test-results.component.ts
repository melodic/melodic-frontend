import {Component, Input, OnInit} from '@angular/core';
import {TestResultTree} from '../model/TestResultTree';

@Component({
  selector: 'app-test-results',
  templateUrl: './test-results.component.html',
  styleUrls: ['./test-results.component.css'],
})
export class TestResultsComponent implements OnInit {

  @Input() results: TestResultTree;

  panelOpenState = false;

  ngOnInit(): void {
  }
}
