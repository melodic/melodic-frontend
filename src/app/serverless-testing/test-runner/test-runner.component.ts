import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-test-runner',
  templateUrl: './test-runner.component.html',
  styleUrls: ['./test-runner.component.css'],
})
export class TestRunnerComponent implements OnInit {

  @Input() testsRunning: boolean;
  @Output() runTestsEvent = new EventEmitter();

  ngOnInit(): void {
  }

  runTests() {
    this.runTestsEvent.emit();
  }


}
