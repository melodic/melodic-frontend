import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ServerlessTestingService} from '../service/serverless-testing.service';
import {MatSnackBar} from '@angular/material';
import {TestConfigurationResponse} from '../model/TestConfigurationResponse';

@Component({
  selector: 'app-uploader-yml',
  templateUrl: './uploader-yml.component.html',
  styleUrls: ['./uploader-yml.component.css'],
})
export class UploaderYmlComponent implements OnInit {
  uploadInProgress = false;
  panelOpenState = false;
  deletingTestConfig = false;
  fileToUpload: File = null;
  total: number = null;
  testConfig: TestConfigurationResponse = null;

  @ViewChild('ymlFile') ymlFile: ElementRef;

  constructor(
    private snackBar: MatSnackBar,
    private testingServerlessService: ServerlessTestingService,
  ) {
  }

  ngOnInit(): void {
    this.testingServerlessService.getTestConfiguration().subscribe(
      response => {
        this.testConfig = response;
      },
      error => {
        if (error.error.status === 404) {
          this.testConfig = null;
        } else {
          this.snackBar.open(
            `Error while getting test config: ${error.error.message}`,
            'Close',
            {duration: 10000}
          );
        }
      }
    );
  }

  @Input()
  set file(file: any) {
    this.fileToUpload = file;
    this.total = this.fileToUpload.size;
  }

  get file(): any {
    return this.fileToUpload;
  }

  convertBytesToSize(bytes): string {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes === 0) {
      return '0 Byte';
    }
    const i = Math.floor(Math.log(bytes) / Math.log(1024));
    return Math.round(bytes / Math.pow(1024, i)) + ' ' + sizes[i];
  }

  formatCondition(condition): string {
    switch (condition) {
      case 'EQUALS':
        return 'should equal';
      case 'STARTS_WITH':
        return 'should start with';
      case 'ENDS_WITH':
        return 'should end with';
      case 'CONTAINS_SUBSTRING':
        return 'should contain substring';
      case 'EQUALS_IGNORE_CASE':
        return 'should equal (ignoring the case)';
      case 'MATCHES_REGEX':
        return 'should match regex pattern';
    }
  }

  get fileUploadDisabled() {
    return this.uploadInProgress || this.fileToUpload == null;
  }

  onFileSelectionEvent(event: Event) {
    this.fileToUpload = (<HTMLInputElement> event.target).files[0];
    console.log('Received event: ', event);
  }

  onFileUploadEvent() {
    this.uploadInProgress = true;
    const formData: FormData = new FormData();
    formData.set('file', this.fileToUpload, this.fileToUpload.name);
    this.testingServerlessService.uploadTestConfiguration(formData).subscribe(
      response => {
        this.uploadInProgress = false;
        this.snackBar.open(
          `Successfully uploaded test config file: ${this.fileToUpload.name}`,
          'Close',
          {duration: 10000},
          );
        this.testConfig = response;
        this.removeSelectedFile();
      },
      error => {
        this.uploadInProgress = false;
        console.log(error);
        this.snackBar.open(
          `Error while uploading: ${error.error.message}`,
          'Close',
          {duration: 10000}
          );
      }
    );
  }

  removeSelectedFile() {
    this.fileToUpload = null;
    this.ymlFile.nativeElement.value = null;
  }

  onFileDeletionEvent() {
    this.deletingTestConfig = true;
    this.testingServerlessService.deleteTestConfiguration().subscribe(
      response => {
        this.deletingTestConfig = false;
        this.testConfig = null;
        this.snackBar.open(
          'Successfully deleted test config file.',
          'Close',
          {duration: 10000},
        );
      },
      error => {
        this.deletingTestConfig = false;
        console.log(error);
        this.snackBar.open(
          `Error while deleting test config file: ${error.error.message}`,
          'Close',
          {duration: 10000}
        );
      }
    );
  }
}
