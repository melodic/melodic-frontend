import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {AppConfigService} from '../../app-config/service/app-config.service';
import {TestResultTree} from '../model/TestResultTree';
import {TestConfigurationResponse} from '../model/TestConfigurationResponse';

@Injectable({
  providedIn: 'root'
})
export class ServerlessTestingService {
  apiUrl = `${AppConfigService.settings.apiUrl}/auth/test`;
  testConfigUrl = this.apiUrl + '/config';

  constructor(private http: HttpClient) {
  }

  uploadTestConfiguration(formData: FormData): Observable<TestConfigurationResponse> {
    return this.http.post(this.testConfigUrl, formData, { responseType: 'json' }).pipe(tap(
        (response: TestConfigurationResponse) => { console.log('Response: ', response); },
        error => { console.log('Error while uploading test configuration file: ', error); }
    ));
  }

  getTestConfiguration(): Observable<TestConfigurationResponse> {
    return this.http.get(this.testConfigUrl, { responseType: 'json' }).pipe(tap(
      (response: TestConfigurationResponse) => { console.log('Response: ', response); },
      error => { console.log('Error while getting test configuration file: ', error); }
    ));
  }

  deleteTestConfiguration(): Observable<any> {
    return this.http.delete(this.testConfigUrl, { responseType: 'json' }).pipe(tap(
      () => { console.log('Deleted test config file successfully.'); },
      error => { console.log('Error while deleting test configuration file: ', error); }
    ));
  }

  runTests(): Observable<TestResultTree> {
    const requestUrl = this.apiUrl + '/run';
    return this.http.post(requestUrl, null, {responseType: 'json'}).pipe(tap(
      (response: TestResultTree) => { console.log('Response: ', response); },
      error => { console.log('Error while running tests: ', error); }
    ));
  }
}
