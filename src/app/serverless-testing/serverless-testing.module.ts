import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ServerlessTestingMainComponent} from './serverless-testing-main/serverless-testing-main.component';
import {AngularMaterialModule} from '../angular-material/angular-material.module';
import {ServerlessTestingRoutingModule} from './route/serverless-testing-routing.module';
import {UploaderYmlComponent} from './uploader-yml/uploader-yml.component';
import {TestRunnerComponent} from './test-runner/test-runner.component';
import {TestResultsComponent} from './test-results/test-results.component';

@NgModule({
  declarations: [
    ServerlessTestingMainComponent,
    UploaderYmlComponent,
    TestRunnerComponent,
    TestResultsComponent,
  ],
  imports: [
    CommonModule,
    ServerlessTestingRoutingModule,
    AngularMaterialModule
  ],
  exports: [
  ],
  entryComponents: [
  ]
})
export class ServerlessTestingModule {
}
