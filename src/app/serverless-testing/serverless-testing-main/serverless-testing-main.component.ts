import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {UploaderYmlComponent} from '../uploader-yml/uploader-yml.component';
import {MatSnackBar} from '@angular/material';
import {ServerlessTestingService} from '../service/serverless-testing.service';
import {TestRunnerComponent} from '../test-runner/test-runner.component';
import {TestResultsComponent} from '../test-results/test-results.component';
import {TestResultTree} from '../model/TestResultTree';

@Component({
  selector: 'app-serverless-testing',
  templateUrl: './serverless-testing-main.component.html',
  styleUrls: ['../../app.component.css']
})
export class ServerlessTestingMainComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(UploaderYmlComponent) uploaderYmlComponent: UploaderYmlComponent;
  @ViewChild(TestRunnerComponent) testRunnerComponent: TestRunnerComponent;
  @ViewChild(TestResultsComponent) testResultsComponent: TestResultsComponent;

  testsRunning = false;
  testResultTree: TestResultTree = null;

  constructor(
    private snackBar: MatSnackBar,
    private testingServerlessService: ServerlessTestingService,
  ) {
  }

  ngOnInit() {
    localStorage.setItem('viewTitle', 'Serverless Testing');
    console.log('Component Serverless-testing-main initiated');
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    console.log('Component Serverless-testing-main destroyed');
  }

  runTests() {
    this.testsRunning = true;
    this.testingServerlessService.runTests().subscribe(
      response => {
        this.snackBar.open('Tests run ended.', 'Close', {duration: 10000});
        this.testResultTree = response;
        this.testsRunning = false;
      },
      error => {
        this.testsRunning = false;
        console.log(error);
        this.snackBar.open(`${error.error.message}`, 'Close', {duration: 10000});
      },
    );
  }
}
