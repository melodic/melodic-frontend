class TestCaseResult {
  event: string;
  condition: string;
  expectedValue: string;
  actualOutput: string;
  message: string;
  result: string;
  duration: number;
}

class FunctionTestResult {
  functionName: string;
  duration: number;
  passed: number;
  failed: number;
  ignored: number;
  testCaseResults: Array<TestCaseResult>;
  overallResult: string;
  failedAtStage: string;
  message: string;
}


export class TestResultTree {
  testsRunResult: string;
  failedAtStage: string;
  message: string;
  duration: number;
  functionTestResults: Array<FunctionTestResult>;
}
