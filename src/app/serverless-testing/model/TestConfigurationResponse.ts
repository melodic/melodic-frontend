export class TestConfigurationResponse {
  path: string;
  configuration: TestConfiguration;
}

class TestConfiguration {
  tests: Array<FunctionTestConfiguration>;
}

class FunctionTestConfiguration {
  functionName: string;
  triggerPath: string;
  testCases: Array<TestCaseConfiguration>;
}

class TestCaseConfiguration {
  event: string;
  condition: string;
  expectedValue: string;
}
