import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ServerlessTestingMainComponent} from '../serverless-testing-main/serverless-testing-main.component';

const routes: Routes = [
  {path: '', component: ServerlessTestingMainComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServerlessTestingRoutingModule {
}
