import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {UserService} from '../user/service/user.service';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {ProviderService} from '../provider/service/provider.service';

@Injectable({providedIn: 'root'})
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private userService: UserService,
              private providerService: ProviderService,
              private snackBar: MatSnackBar,
              private router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      if (err.status === 401 && this.userService.currentUser) {
        this.userService.logout(this.router.url);
        console.log(`${err.error}`);
        const message = `Too long time from last login. Your token expired. Please, log in again`;
        this.snackBar.open(message, 'Close');
      }

      if (err.status === 401) {
        console.log(`error 401: ${err.error}`);
        let message;
        if (request.url.includes('login')) {
          message = `Invalid credentials. Please try again`;
        } else {
          message = `Too long time from last login. Your token expired. Please, log in again`;
        }
        this.userService.logout(this.router.url);
        this.snackBar.open(message, 'Close');
      }

      if (err.status === 403) {
        console.log(`error 403: ${err.error}`);
        const message = `You haven't permission to requested view`;
        this.userService.logout(this.router.url);
        this.snackBar.open(message, 'Close');
      }

      if (err.status === 500) {
        console.log('Internal server error', err);
        const message = 'Service temporary is not working. Please try again.';
        this.snackBar.open(message, 'Close');
      }

      if (err.status === 0) {
        console.log('Backend does not respond: ', err);
        err.error.message = 'Melodic backend server is not responding.';
        return throwError(err);
      }

      // handle MissingSecureVariableErrorResponse
      if (err.status === 404 && err.error.hasOwnProperty('errorName') && err.error.errorName === 'SecureVariableNotFoundException') {
        console.log(`SecureVariableNotFoundException handle for variable: ${err.missingVariableName}`);
        const message = `Secure variable with key ${err.error.missingVariableName} not found in secure store.`;
        this.snackBar.open(message, 'Close');
        this.providerService.openMissingSecureVariableDialog(err.error.missingVariableName);
      }

      // for other errors
      return throwError(err);
    }));
  }
}
