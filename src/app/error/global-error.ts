export class GlobalError extends Error {
  error: ErrorMessage;

  constructor(message: string) {
    super();
    this.error = new ErrorMessage(message);
  }

}

export class ErrorMessage {
  message: string;

  constructor(message: string) {
    this.message = message;
  }
}
