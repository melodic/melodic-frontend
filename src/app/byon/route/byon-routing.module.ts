import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ByonListComponent} from '../byon-list/byon-list.component';

const routes: Routes = [
  {path: '', component: ByonListComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ByonRoutingModule {
}
