import {FormGroup} from '@angular/forms';

export function oneOfTwoFieldsRequiredValidator(oneFieldKey: string, otherFieldKey: string) {
  return (group: FormGroup): { [key: string]: any } => {
    const oneVal = group.controls[oneFieldKey];
    const otherVal = group.controls[otherFieldKey];
    const isOneOfFieldsFilled = oneVal.value || otherVal.value;
    if (!isOneOfFieldsFilled) {
      otherVal.setErrors({oneOfTwoFieldsRequired: oneFieldKey});
      oneVal.setErrors({oneOfTwoFieldsRequired: oneFieldKey});
      return {oneOfTwoFieldsRequired: true};
    }
    if (isOneOfFieldsFilled && otherVal.hasError('oneOfTwoFieldsRequired')) {
      oneVal.setErrors(null);
      otherVal.setErrors(null);
    }
    if (isOneOfFieldsFilled && oneVal.hasError('oneOfTwoFieldsRequired')) {
      oneVal.setErrors(null);
      otherVal.setErrors(null);
    }

    return null;
  };
}
