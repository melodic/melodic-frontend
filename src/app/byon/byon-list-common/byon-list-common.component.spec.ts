import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ByonListCommonComponent} from './byon-list-common.component';

describe('ByonListCommonComponent', () => {
  let component: ByonListCommonComponent;
  let fixture: ComponentFixture<ByonListCommonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ByonListCommonComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByonListCommonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
