import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {Byon} from '../model/byon';
import {IpAddressType, IpVersion} from '../../application/model/ip-address';
import {Cloud} from '../../process/process-details/offer/model/cloud';
import {ByonService} from '../service/byon.service';
import {AppInjector} from '../../injector/app-injector';

@Component({
  selector: 'app-byon-list-common',
  templateUrl: './byon-list-common.component.html',
  styleUrls: ['./byon-list-common.component.css']
})
export class ByonListCommonComponent implements OnInit {

  protected snackBar: MatSnackBar;
  protected byonService: ByonService;

  returnedValueForMissingInformation = 'UNKNOWN';
  byonLoadingInProgress = false;
  data: MatTableDataSource<Byon>;
  displayedColumns: string[];
  cloudsList = new Array<Cloud>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor() {
    const injector = AppInjector.getInjector();
    this.snackBar = injector.get(MatSnackBar);
    this.byonService = injector.get(ByonService);
  }

  ngOnInit() {
  }

  applyFilter(filterValue: string) {
    this.data.filter = filterValue.trim();

    if (this.data.paginator) {
      this.data.paginator.firstPage();
    }
  }

  getPublicIpV4(byon: Byon): string {
    const publicIP = byon.ipAddresses
      .find(value => (value.ipAddressType.toString() === IpAddressType[IpAddressType.PUBLIC_IP]
        && value.ipVersion.toString() === IpVersion[IpVersion.V4]));
    return publicIP ? publicIP.value : this.returnedValueForMissingInformation;
  }

  getSystemWithVersion(byon: Byon): string {
    return `${byon.nodeProperties.operatingSystem.operatingSystemFamily} ${byon.nodeProperties.operatingSystem.operatingSystemVersion}`;
  }

  getLocation(byon: Byon): string {
    return `${byon.nodeProperties.geoLocation.country}: ${byon.nodeProperties.geoLocation.city}`;
  }

  getProviderName(byon: Byon): string {
    const provider = this.cloudsList
      .find(value => value.id === byon.nodeProperties.providerId);
    return provider ? provider.api.providerName : this.returnedValueForMissingInformation;
  }

  protected updateByonTableData(byonDefValue: Array<Byon>) {
    this.data = new MatTableDataSource<Byon>(byonDefValue);

    this.data.filterPredicate = (filteringData, filter) => {
      return this.getByonDefinitionAsString(filteringData).indexOf(filter) !== -1;
    };

    this.data.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'publicIpAddress':
          return this.getPublicIpV4(item);
        case 'cores':
          return item.nodeProperties.numberOfCores;
        case 'memory':
          return item.nodeProperties.memory;
        case 'disk':
          return item.nodeProperties.disk;
        case 'system':
          return this.getSystemWithVersion(item);
        case 'location':
          return this.getLocation(item);
        case 'provider':
          return this.getProviderName(item);
        default:
          return item[property];
      }
    };
    this.data.paginator = this.paginator;
    this.data.sort = this.sort;
  }

  private getByonDefinitionAsString(byon: Byon) {
    return `${byon.name} ${this.getPublicIpV4(byon)} ${byon.nodeProperties.numberOfCores} ${byon.nodeProperties.memory}
     ${byon.nodeProperties.disk} ${this.getSystemWithVersion(byon)} ${this.getLocation(byon)} ${this.getProviderName(byon)}`;
  }
}
