import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IpAddressListComponent} from './ip-address-list.component';

describe('IpAddressListComponent', () => {
  let component: IpAddressListComponent;
  let fixture: ComponentFixture<IpAddressListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IpAddressListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IpAddressListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
