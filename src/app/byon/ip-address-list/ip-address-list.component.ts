import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {IpAddress} from '../../application/model/ip-address';
import {MatDialog, MatDialogRef, MatSort, MatTableDataSource} from '@angular/material';
import {IpAddressFormComponent} from '../ip-address-form/ip-address-form.component';
import {ByonEnums} from '../model/byon-enums';

@Component({
  selector: 'app-ip-address-list',
  templateUrl: './ip-address-list.component.html',
  styleUrls: ['./ip-address-list.component.css']
})
export class IpAddressListComponent implements OnInit {

  displayedColumns: string[] = ['no', 'ipAddressType', 'ipVersion', 'value', 'edit', 'delete'];
  data: MatTableDataSource<IpAddress>;
  ipAddressDialog: MatDialogRef<IpAddressFormComponent>;

  @Input()
  private ipAddresses = new Array<IpAddress>();

  @Input()
  private byonEnums = new ByonEnums();

  @Input()
  public isReadMode = false;

  @ViewChild(MatSort) sort: MatSort;

  constructor(private dialog: MatDialog) {
  }

  ngOnInit() {
    this.updateTableData();
  }

  getIpAddresses() {
    return this.ipAddresses;
  }

  private updateTableData() {
    this.data = new MatTableDataSource<IpAddress>(this.ipAddresses);
    this.data.sort = this.sort;
  }

  onAddNewIpAddressClick() {
    this.openIpAddressDialog();
  }

  onEditIpAddressClick(ipAddress: IpAddress) {
    this.openIpAddressDialog(ipAddress, true);
  }

  onDeleteIpAddressClick(ipAddress: IpAddress) {
    const ipAddressToDeleteId = this.ipAddresses.indexOf(ipAddress);
    this.ipAddresses.splice(ipAddressToDeleteId, 1);
    this.updateTableData();
  }

  private openIpAddressDialog(ipAddress?: IpAddress, isEditMode?: boolean) {
    this.ipAddressDialog = this.dialog.open(IpAddressFormComponent, {
      data: {
        byonEnums: this.byonEnums,
        ipAddress: ipAddress ? ipAddress : undefined
      },
      hasBackdrop: false,
      disableClose: true,
      width: '40%'
    });

    this.ipAddressDialog.afterClosed().subscribe(result => {
      if (result && !isEditMode) {
        console.log(`Closed dialog with new ip address`);
        this.ipAddresses.push(result);
        this.updateTableData();
      } else if (result && isEditMode) {
        console.log('Closed dialog with edited ip address');
        const ipAddressToUpdateIndex = this.ipAddresses.indexOf(ipAddress);
        this.ipAddresses.splice(ipAddressToUpdateIndex, 1);
        this.ipAddresses.push(result);
        this.updateTableData();
      }
    });
  }
}
