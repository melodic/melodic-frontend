import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ByonListComponent} from './byon-list/byon-list.component';
import {ByonRoutingModule} from './route/byon-routing.module';
import {AngularMaterialModule} from '../angular-material/angular-material.module';
import {ByonListCommonComponent} from './byon-list-common/byon-list-common.component';
import {ByonDefinitionFormComponent} from './byon-definition-form/byon-definition-form.component';
import {IpAddressListComponent} from './ip-address-list/ip-address-list.component';
import {IpAddressFormComponent} from './ip-address-form/ip-address-form.component';

@NgModule({
  declarations: [
    ByonListComponent,
    ByonListCommonComponent,
    ByonDefinitionFormComponent,
    IpAddressListComponent,
    IpAddressFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ByonRoutingModule,
    AngularMaterialModule
  ],
  entryComponents: [
    ByonDefinitionFormComponent,
    IpAddressFormComponent
  ]
})
export class ByonModule {
}
