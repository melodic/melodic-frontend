import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AppConfigService} from '../../app-config/service/app-config.service';
import {Observable} from 'rxjs';
import {Byon} from '../model/byon';
import {tap} from 'rxjs/operators';
import {ByonEnums} from '../model/byon-enums';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ByonService {

  apiUrl = `${AppConfigService.settings.apiUrl}/auth/byon`;

  constructor(private http: HttpClient) {
  }

  createNewByonDefinition(newByonDefinitionRequest: Byon): Observable<Byon> {
    return this.http.post(this.apiUrl, JSON.stringify(newByonDefinitionRequest), httpOptions).pipe(
      tap((response: Byon) => console.log(
        `Response for new byon definition with id: ${response.id} and name: ${response.name}`
        ),
        e => console.log(`Error by creating new byon definition: `, e)
      ));
  }

  getByonDefinitionList(): Observable<Array<Byon>> {
    return this.http.get(`${this.apiUrl}`, httpOptions).pipe(
      tap((response: Array<Byon>) => {
          console.log(`Get Byon definitions list with ${response.length} elements`);
        },
        e => console.log('Error by getting Byon definitions list:', e))
    );
  }

  deleteByonDefinition(byonDefinitionId: string) {
    const requestUrl = `${this.apiUrl}/${byonDefinitionId}`;
    return this.http.delete(requestUrl, httpOptions).pipe(
      tap(() => {
          console.log(`Byon definition with id: ${byonDefinitionId} successfully deleted`);
        },
        e => console.log(`Error by deleting byon definition with id: ${byonDefinitionId}`, e))
    );
  }

  updateByonDefinition(updatedByonDefinitionRequest: Byon, byonDefinitionId: string): Observable<Byon> {
    const requestUrl = `${this.apiUrl}/${byonDefinitionId}`;
    return this.http.put(requestUrl, JSON.stringify(updatedByonDefinitionRequest), httpOptions).pipe(
      tap((response: Byon) => console.log(
        `Response for updated byon definition with id: ${response.id} and name: ${response.name}`
        ),
        e => console.log(`Error by updating byon definition: `, e)
      ));
  }

  getByonFromCloudiatorList(): Observable<Array<Byon>> {
    return this.http.get(`${this.apiUrl}/cloudiator`, httpOptions).pipe(
      tap((response: Array<Byon>) => {
          console.log(`Get Byon list available in Cloudiator with ${response.length} elements`);
        },
        e => console.log('Error by getting Byon list available in Cloudiator:', e))
    );
  }

  addByonDefinitionToCloudiator(byonDefinitionId: string): Observable<Byon> {
    const requestUrl = `${this.apiUrl}/cloudiator/${byonDefinitionId}`;
    return this.http.post(requestUrl, httpOptions).pipe(
      tap((response: Byon) => {
          console.log(`Byon as byon with id ${response.id} from byon definition with id ${byonDefinitionId}
        successfully added to Cloudiator`);
        },
        e => console.log(`Error by adding Byon definition with id ${byonDefinitionId} to Cloudiator:`, e))
    );
  }

  deleteByonFromCloudiator(byonId: string) {
    const requestUrl = `${this.apiUrl}/cloudiator/${byonId}`;
    return this.http.delete(requestUrl, httpOptions).pipe(
      tap(() => {
          console.log(`Byon with id ${byonId} successfully deleted from Cloudiator`);
        },
        e => console.log(`Error by deleting byon with id ${byonId}:`, e))
    );
  }

  getByonEnums(): Observable<ByonEnums> {
    const requestUrl = `${this.apiUrl}/enum`;
    return this.http.get(requestUrl, httpOptions).pipe(
      tap((response: ByonEnums) => {
          console.log(`Byon enums successfully got`);
        },
        e => console.log(`Error by getting byon enums:`, e))
    );
  }
}
