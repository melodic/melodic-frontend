import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ByonListComponent} from './byon-list.component';

describe('ByonListComponent', () => {
  let component: ByonListComponent;
  let fixture: ComponentFixture<ByonListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ByonListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByonListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
