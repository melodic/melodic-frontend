import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import {Byon} from '../model/byon';
import {ProcessOfferService} from '../../process/process-details/offer/service/process-offer.service';
import {ByonListCommonComponent} from '../byon-list-common/byon-list-common.component';
import {ByonDefinitionFormComponent} from '../byon-definition-form/byon-definition-form.component';
import {ConfirmationDialogComponent} from '../../common-template/confirmation-dialog/confirmation-dialog.component';
import {WebSshService} from '../../application/service/web-ssh.service';

@Component({
  selector: 'app-byon-list',
  templateUrl: './byon-list.component.html',
  styleUrls: ['./byon-list.component.css']
})
export class ByonListComponent extends ByonListCommonComponent implements OnInit {

  displayedColumns: string[] = ['no', 'name', 'publicIpAddress', 'cores', 'memory', 'disk', 'system', 'location', 'provider',
    'diagnostic', 'sshConnection', 'edit', 'delete'];
  byonDefinitionDialog: MatDialogRef<ByonDefinitionFormComponent>;
  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;

  constructor(private processOfferService: ProcessOfferService,
              private dialog: MatDialog,
              private webSshService: WebSshService) {
    super();
  }

  ngOnInit() {
    localStorage.setItem('viewTitle', 'Byon definitions');
    this.getByonDefinitionList();
  }

  getByonDefinitionList() {
    this.byonLoadingInProgress = true;
    this.processOfferService.getCloudList().subscribe(cloudsResposne => {
        this.cloudsList = cloudsResposne;
        this.byonService.getByonDefinitionList().subscribe(byonDefValue => {
            console.log(`Successfully getting byon definitions`);
            this.updateByonTableData(byonDefValue);
            this.byonLoadingInProgress = false;
          },
          error1 => {
            this.byonLoadingInProgress = false;
            console.log('Error by getting byon definitions');
            if (error1.status !== 404) {
              this.snackBar.open(`Error by getting byon definitions: ${error1.error.message}`, 'Close');
            }
          });
      },
      error1 => {
        this.byonLoadingInProgress = false;
        this.cloudsList = [];
        this.snackBar.open(`Problem by getting data about clouds: ${error1.error.message}`, 'Close');
      });
  }

  onCreateNewByonDefinitionClick() {
    this.openByonDefinitionDialog();
  }

  isSshConnectionPossible(byon: Byon): boolean {
    return byon.loginCredential !== null && this.getPublicIpV4(byon) !== this.returnedValueForMissingInformation;
  }

  onSshConnectionClick(byon: Byon) {
    this.webSshService.createSshConnection(byon.loginCredential, this.getPublicIpV4(byon), byon.name);
  }

  onEditByonDefinitionClick(byon: Byon) {
    console.log(`Edit byon wtih name: ${byon.name} and id: ${byon.id}`);
    this.openByonDefinitionDialog(byon, true, false);
  }

  onReadByonDefinitionClick(byon: Byon) {
    console.log(`Read byon with name: ${byon.name}`);
    this.openByonDefinitionDialog(byon, false, true);
  }

  onDeleteByonDefinitionClick(byon: Byon) {
    console.log(`Delete byon with name: ${byon.name} and id: ${byon.id}`);
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      hasBackdrop: false,
      data: {
        title: 'Delete byon definition',
        message: `Byon definition with name ${byon.name} will be deleted from your configuration.
         Do you want to continue?`
      },
      width: '30%'
    });

    this.confirmationDialog.afterClosed().subscribe(value => {
      if (value) {
        console.log(`Confirmed deleting of byon definition with name ${byon.name}`);
        this.deleteByonDefinition(byon);
      } else {
        console.log(`Cancel deleting of byon definition with name ${byon.name}`);
      }
    });
  }

  private openByonDefinitionDialog(byon?: Byon, isEditMode?: boolean, isReadMode?: boolean) {
    this.byonDefinitionDialog = this.dialog.open(ByonDefinitionFormComponent, {
      hasBackdrop: false,
      disableClose: true,
      width: '80%',
      data: {
        byonData: byon ? byon : undefined,
        isEditMode: isEditMode ? isEditMode : false,
        isReadMode: isReadMode ? isReadMode : false
      }
    });

    this.byonDefinitionDialog.afterClosed().subscribe(result => {
      if (result && !isReadMode && !isEditMode) {
        console.log(`Saving new byon definition with name: ${result.name}`);
        this.saveNewByonDefinition(result);
      } else if (result && isEditMode) {
        console.log(`Saving edited byon definition with name: ${result.name}`);
        this.updateByonDefinition(byon.id, result);
      }
    });
  }

  private saveNewByonDefinition(newByonDef: Byon) {
    this.byonLoadingInProgress = true;
    this.byonService.createNewByonDefinition(newByonDef).subscribe(value => {
        this.snackBar.open(`New byon definition ${value.name} successfully added to your configuration`, 'Close');
        this.getByonDefinitionList();
      },
      error1 => {
        this.byonLoadingInProgress = false;
        this.snackBar.open(`Error by adding new byon definition with name: ${newByonDef.name} to your configuration:
      ${error1.error.message}`, 'Close');
      });
  }

  private updateByonDefinition(updatedByonId: string, newByon: Byon) {
    this.byonLoadingInProgress = true;
    this.byonService.updateByonDefinition(newByon, updatedByonId).subscribe(value => {
        this.snackBar.open(`Byon definition ${value.name} successfully updated in your configuration`, 'Close');
        this.getByonDefinitionList();
      },
      error1 => {
        this.byonLoadingInProgress = false;
        this.snackBar.open(`Error by updating byon definition with new name ${newByon.name}: ${error1.error.message}`, 'Close');
        this.getByonDefinitionList();
      });
  }

  private deleteByonDefinition(byon: Byon) {
    this.byonLoadingInProgress = true;
    this.byonService.deleteByonDefinition(byon.id).subscribe(() => {
        this.snackBar.open(`Byon definition ${byon.name} successfully deleted from your settings`, 'Close');
        this.getByonDefinitionList();
      },
      error1 => {
        this.byonLoadingInProgress = false;
        this.snackBar.open(`Error by deleting byon definition with id ${byon.id} from your settings: ${error1.error.message}`, 'Close');
      }
    );
  }
}
