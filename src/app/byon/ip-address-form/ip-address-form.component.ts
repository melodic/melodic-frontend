import {Component, Inject, OnInit, Optional} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {IpAddress} from '../../application/model/ip-address';
import {ByonEnums} from '../model/byon-enums';

@Component({
  selector: 'app-ip-address-form',
  templateUrl: './ip-address-form.component.html',
  styleUrls: ['./ip-address-form.component.css']
})
export class IpAddressFormComponent implements OnInit {

  ipAddress: IpAddress;
  ipAddressForm: FormGroup;
  byonEnums = new ByonEnums();
  ipAddresses = new Array<IpAddress>();

  constructor(private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<IpAddressFormComponent>,
              @Optional() @Inject(MAT_DIALOG_DATA) public dialogData?: any) {
  }

  ngOnInit() {
    if (this.dialogData && this.dialogData.ipAddress) {
      this.ipAddress = this.dialogData.ipAddress ? this.dialogData.ipAddress : undefined;
    }
    this.ipAddresses = this.dialogData.ipAddresses;
    this.byonEnums = this.dialogData.byonEnums;
    this.ipAddressForm = this.formBuilder.group({
      ipAddressType: this.ipAddress ? new FormControl({value: this.ipAddress.ipAddressType, disabled: false}) : ['', Validators.required],
      ipVersion: this.ipAddress ? new FormControl({value: this.ipAddress.ipVersion, disabled: false}) : ['', Validators.required],
      value: this.ipAddress ? new FormControl({value: this.ipAddress.value, disabled: false}) : ['', Validators.required]
    });
  }

  get form() {
    return this.ipAddressForm.controls;
  }

  getRequiredMsg() {
    return 'Mandatory field';
  }

  addIpAddress(ipAddressForm: NgForm) {
    const ipAddress = <IpAddress> ipAddressForm.value;
    this.dialogRef.close(ipAddress);
    console.log(`Adding ip address with value: ${ipAddress.value}`);
  }
}
