import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IpAddressFormComponent} from './ip-address-form.component';

describe('IpAddressFormComponent', () => {
  let component: IpAddressFormComponent;
  let fixture: ComponentFixture<IpAddressFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IpAddressFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IpAddressFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
