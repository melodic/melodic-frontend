import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ByonDefinitionFormComponent} from './byon-definition-form.component';

describe('ByonDefinitionFormComponent', () => {
  let component: ByonDefinitionFormComponent;
  let fixture: ComponentFixture<ByonDefinitionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ByonDefinitionFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByonDefinitionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
