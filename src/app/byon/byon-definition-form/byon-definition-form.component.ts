import {Component, Inject, NgZone, OnInit, Optional, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {IpAddress} from '../../application/model/ip-address';
import {Byon} from '../model/byon';
import {CdkTextareaAutosize} from '@angular/cdk/text-field';
import {take} from 'rxjs/operators';
import {IpAddressListComponent} from '../ip-address-list/ip-address-list.component';
import {ByonEnums} from '../model/byon-enums';
import {ByonService} from '../service/byon.service';
import {oneOfTwoFieldsRequiredValidator} from '../validator/one-of-two-fields-required.validator';

@Component({
  selector: 'app-byon-definition-form',
  templateUrl: './byon-definition-form.component.html',
  styleUrls: ['./byon-definition-form.component.css']
})
export class ByonDefinitionFormComponent implements OnInit {

  @ViewChild(IpAddressListComponent) ipAddressesComponent: IpAddressListComponent;
  @ViewChild('autosize', {read: false}) autosize: CdkTextareaAutosize;

  byonData: Byon;
  isReadMode = false;
  passwordHide = true;
  byonEnums = new ByonEnums();
  byonEnumsLoadingInProgress = false;

  ipAddresses = new Array<IpAddress>();
  loginCredentialForm: FormGroup;
  geoLocationForm: FormGroup;
  operatingSystemForm: FormGroup;
  byonDefinitionForm: FormGroup;
  nodePropertiesForm: FormGroup;


  constructor(private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<ByonDefinitionFormComponent>,
              private ngZone: NgZone,
              private byonService: ByonService,
              private snackBar: MatSnackBar,
              @Optional() @Inject(MAT_DIALOG_DATA) public dialogData?: any) {
  }

  triggerResize() {
    // Wait for changes to be applied, then trigger textarea resize.
    this.ngZone.onStable.pipe(take(1))
      .subscribe(() => this.autosize.resizeToFitContent(true));
  }

  ngOnInit() {
    if (this.dialogData) {
      this.byonData = this.dialogData ? this.dialogData.byonData : undefined;
      this.isReadMode = this.dialogData ? this.dialogData.isReadMode : false;
      this.ipAddresses = this.byonData ? this.byonData.ipAddresses : [];
    }

    this.byonEnumsLoadingInProgress = true;
    this.byonService.getByonEnums().subscribe(byonEnumsResponse => {
        this.byonEnums = byonEnumsResponse;
        this.byonEnumsLoadingInProgress = false;
      },
      error1 => {
        this.byonEnumsLoadingInProgress = false;
        this.snackBar.open(`Error by getting available options for byons form: ${error1.error.message}`, 'Close');
      }
    );
    this.loginCredentialForm = this.formBuilder.group({
        id: this.byonData ? this.byonData.loginCredential.id : null,
        username: this.byonData ? new FormControl({value: this.byonData.loginCredential.username, disabled: this.isReadMode})
          : ['', Validators.required],
        password: this.byonData ? new FormControl({value: this.byonData.loginCredential.password, disabled: this.isReadMode})
          : [''],
        privateKey: this.byonData ? new FormControl({value: this.byonData.loginCredential.privateKey, disabled: this.isReadMode})
          : ['']
      },
      {
        validators: oneOfTwoFieldsRequiredValidator('password', 'privateKey')
      });
    this.geoLocationForm = this.formBuilder.group({
      id: this.byonData ? this.byonData.nodeProperties.geoLocation.id : null,
      city: this.byonData ? new FormControl({value: this.byonData.nodeProperties.geoLocation.city, disabled: this.isReadMode})
        : ['', Validators.required],
      country: this.byonData ? new FormControl({value: this.byonData.nodeProperties.geoLocation.country, disabled: this.isReadMode})
        : ['', Validators.required],
      latitude: this.byonData ? new FormControl({value: this.byonData.nodeProperties.geoLocation.latitude, disabled: this.isReadMode})
        : ['', [Validators.required, Validators.pattern(/^-*[0-9]*\.*[0-9]+$/),
          Validators.min(-90), Validators.max(90)]], // double
      longitude: this.byonData ? new FormControl({value: this.byonData.nodeProperties.geoLocation.longitude, disabled: this.isReadMode})
        : ['', [Validators.required, Validators.pattern(/^-*[0-9]*\.*[0-9]+$/),
          Validators.min(-180), Validators.max(180)]] // double
    });

    this.operatingSystemForm = this.formBuilder.group({
      id: this.byonData ? this.byonData.nodeProperties.operatingSystem.id : null,
      operatingSystemFamily: this.byonData ? new FormControl({
        value: this.byonData.nodeProperties.operatingSystem.operatingSystemFamily,
        disabled: this.isReadMode
      }) : ['', Validators.required],
      operatingSystemArchitecture: this.byonData ? new FormControl({
        value:
        this.byonData.nodeProperties.operatingSystem.operatingSystemArchitecture, disabled: this.isReadMode
      }) : ['', Validators.required],
      operatingSystemVersion: this.byonData ? new FormControl({
        value: this.byonData.nodeProperties.operatingSystem.operatingSystemVersion,
        disabled: this.isReadMode
      }) : ['', [Validators.required, Validators.pattern(/^[0-9]*\.*[0-9]+$/)]] // bigDecimal
    });

    this.nodePropertiesForm = this.formBuilder.group({
      id: this.byonData ? this.byonData.nodeProperties.id : null,
      providerId: this.byonData ? new FormControl({value: this.byonData.nodeProperties.providerId, disabled: this.isReadMode})
        : ['', Validators.required],
      numberOfCores: this.byonData ? new FormControl({value: this.byonData.nodeProperties.numberOfCores, disabled: this.isReadMode})
        : ['', [Validators.required, Validators.pattern(/^[0-9]+$/)]], // int
      memory: this.byonData ? new FormControl({value: this.byonData.nodeProperties.memory, disabled: this.isReadMode})
        : ['', [Validators.required, Validators.pattern(/^[0-9]+$/)]], // long
      disk: this.byonData ? new FormControl({value: this.byonData.nodeProperties.disk, disabled: this.isReadMode})
        : ['', [Validators.required, Validators.pattern(/^[0-9]*\.*[0-9]+$/)]], // float
      operatingSystem: this.operatingSystemForm,
      geoLocation: this.geoLocationForm
    });

    this.byonDefinitionForm = this.formBuilder.group({
      id: this.byonData ? this.byonData.id : null,
      name: this.byonData ? new FormControl({value: this.byonData.name, disabled: this.isReadMode}) : ['', Validators.required],
      loginCredential: this.loginCredentialForm,
      ipAddresses: this.byonData ? this.byonData.ipAddresses : [],
      nodeProperties: this.nodePropertiesForm
    });
  }

  get form() {
    return this.byonDefinitionForm.controls;
  }

  get loginCredentialFormControl() {
    return this.loginCredentialForm.controls;
  }

  get nodePropertiesFormControl() {
    return this.nodePropertiesForm.controls;
  }

  get operatingSystemFormControl() {
    return this.operatingSystemForm.controls;
  }

  get geoLocationFormControl() {
    return this.geoLocationForm.controls;
  }

  saveByonDefinition(byonDefinitionFrom: NgForm) {
    this.form.ipAddresses.setValue(this.ipAddressesComponent.getIpAddresses());
    const byonDefinition = <Byon> byonDefinitionFrom.value;
    this.dialogRef.close(byonDefinition);
    console.log(`Saving byon definition with name: ${byonDefinition.name}`);
  }

  getRequiredMsg() {
    return 'Mandatory field';
  }

  getLatitudeMsg() {
    return 'Latitude should be a float number in range: from -90 to 90';
  }

  getLongitudeMsg() {
    return 'Longitude should be a float number in range: from -180 to 180';
  }

  getOsVersionMsg() {
    return 'Operating system version should be a float positive number';
  }

  getNumberOfCoresMsg() {
    return 'Number of cores should be a natural number';
  }

  getMemoryMsg() {
    return 'Memory should be a natural number';
  }

  getDiskMsg() {
    return 'Disk should be a float positive number';
  }

  getPasswordOrKeyRequiredMsg() {
    return 'Filling one of fields: password or key is required';
  }
}
