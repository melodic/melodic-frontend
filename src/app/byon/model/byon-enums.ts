export class ByonEnums {
  ipAddressTypes: Array<string>;
  ipVersions: Array<string>;
  osFamilies: Array<string>;
  osArchitectures: Array<string>;
}
