import {LoginCredential} from '../../application/model/login-credential';
import {IpAddress} from '../../application/model/ip-address';
import {NodeProperties} from '../../application/model/node-properties';

export class Byon {
  id: string;
  name: string;
  loginCredential: LoginCredential;
  ipAddresses: Array<IpAddress>;
  nodeProperties: NodeProperties;
  reason: string;
  diagnostic: string;
  nodeCandidate: string;
}
