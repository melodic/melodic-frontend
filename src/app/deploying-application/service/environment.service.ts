import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {AppConfigService} from '../../app-config/service/app-config.service';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class EnvironmentService {

  apiUrl = `${AppConfigService.settings.apiUrl}/auth/environment`;

  constructor(private http: HttpClient) {
  }

  restart(): Observable<{}> {
    const url = `${this.apiUrl}/restart`;
    return this.http.put(url, httpOptions).pipe(
      tap(() => console.log(
        `Machine restarted`
        ),
        e => console.log(`Error by restarting machine: ${e}`)
      ));
  }
}
