import {Injectable, QueryList} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {CloudDefinitionForRead} from '../../provider/model/view/cloud-definition-for-read';
import {CloudDefinition} from '../../provider/model/cloud-definition';
import {CloudConfigurationForRead} from '../../provider/model/view/cloud-configuration-for-read';
import {CloudConfiguration} from '../../provider/model/cloud-configuration';
import {AppConfigService} from '../../app-config/service/app-config.service';
import {UserService} from '../../user/service/user.service';
import {FileUploaderComponent} from '../../file-uploader/uploader/file-uploader.component';
import {UploadXmiResponse} from '../../file-uploader/model/upload-xmi-response';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class DeploymentService {

  apiUrl = `${AppConfigService.settings.apiUrl}/auth/deployment`;

  constructor(private http: HttpClient,
              private userService: UserService) {
  }

  createDeploymentProcess(deploymentRequest: any): Observable<any> {
    const requestUrl = this.apiUrl + '/process';
    const httpDeploymentProcessHeader = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Refresh: this.userService.currentUser.refreshToken
      })
    };
    return this.http.post(requestUrl, JSON.stringify(deploymentRequest), httpDeploymentProcessHeader).pipe(
      tap(() => console.log(
        `Deployment request for application ${deploymentRequest.applicationId} in simulation mode = ${deploymentRequest.isSimulation} sent`
        ),
        e => console.log(`Error by creating deployment process: `, e)
      ));
  }

  uploadModel(formData: FormData): Observable<any> {
    const uploadUrl = this.apiUrl + '/xmi';
    return this.http.post(uploadUrl, formData, {
      observe: 'events',
      reportProgress: true,
      responseType: 'json'
    });
  }

  uploadMultipleModels(queryList: QueryList<FileUploaderComponent>): Observable<Array<UploadXmiResponse>> {
    const formData = new FormData();
    queryList.forEach(item => {
      formData.append('files', item.uploadingFile, item.uploadingFile.name);
    });
    const uploadUrl = this.apiUrl + '/xmi/multiple';
    return this.http.post(uploadUrl, formData).pipe(
      tap((response: Array<UploadXmiResponse>) => {
          console.log(`Multiple files in number of ${response.length} uploaded`);
        },
        e => console.log('Error by uploading multiple files', e)));
  }

  deleteModel(modelName: any): Observable<{}> {
    const requestUrl = this.apiUrl + `/xmi/${modelName}`;
    return this.http.delete(requestUrl, httpOptions).pipe(
      tap(() => console.log(
        `Model deleted from CDO`
        ),
        e => console.log('Error by deleting model', e)
      ));
  }

  getUploadedModels(): Observable<Array<string>> {
    const requestUrl = this.apiUrl + `/xmi`;
    return this.http.get(requestUrl).pipe(
      tap((response: Array<string>) => console.log(
        'Get all xmi request'
        ),
        e => console.log('Error by getting all models', e)
      ));
  }

  mapChosenCloudDefToCloudDef(cloudDefinitionsForChoose: CloudDefinitionForRead[]): CloudDefinition[] {
    const result = new Array<CloudDefinition>();
    cloudDefinitionsForChoose.forEach(cloudDef => {
      if (cloudDef.checked) {
        const cloudDefForRequest = new CloudDefinition(cloudDef.endpoint, cloudDef.cloudType, cloudDef.api, cloudDef.credential,
          this.mapCloudConfForReadToCloudConf(cloudDef.cloudConfiguration));
        console.log(`Mapped cloud def for provider: ${cloudDefForRequest.api.providerName}`);
        result.push(cloudDefForRequest);
      }
    });
    return result;
  }

  private mapCloudConfForReadToCloudConf(cloudConf: CloudConfigurationForRead): CloudConfiguration {
    const chosenProperty = cloudConf.properties.filter(value => value.checked);
    return new CloudConfiguration(cloudConf.nodeGroup, chosenProperty);
  }

}

