import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Byon} from '../../byon/model/byon';
import {Router} from '@angular/router';
import {ByonListCommonComponent} from '../../byon/byon-list-common/byon-list-common.component';

@Component({
  selector: 'app-byon-definitions',
  templateUrl: './byon-definitions.component.html',
  styleUrls: ['./byon-definitions.component.css']
})
export class ByonDefinitionsComponent extends ByonListCommonComponent implements OnInit {

  byonLoadingInProgress = false;

  byonInCloudiatorList: Array<Byon>;
  data: MatTableDataSource<Byon>;
  displayedColumns: string[] = ['no', 'name', 'publicIpAddress', 'cores', 'memory', 'disk', 'system', 'location',
    'diagnostic', 'add', 'delete'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private router: Router) {
    super();
  }

  ngOnInit() {
    this.getByonDefinitionList();
  }

  onByonButtonClick() {
    console.log('On Byon configuration click');
    this.router.navigate(['/byon']);
  }

  onAddByonToCloudiatorClick(byon: Byon) {
    console.log(`Add byon node with name: ${byon.name} and id: ${byon.id} click`);
    this.byonLoadingInProgress = true;
    this.byonService.addByonDefinitionToCloudiator(byon.id).subscribe(value => {
        this.snackBar.open(`New byon ${value.name} successfully added to Cloudiator`, 'Close');
        this.getByonDefinitionList();
      },
      error1 => {
        this.byonLoadingInProgress = false;
        this.snackBar.open(`Error by adding new byon to Cloudiator: ${error1.error.message}`, 'Close');
      });
  }

  onDeleteByonFromCloudiatorClick(byon: Byon) {
    this.byonLoadingInProgress = true;
    console.log(`Delete byon with name: ${byon.name} and id: ${byon.id} from Cloudiator click`);
    this.byonService.deleteByonFromCloudiator(byon.id).subscribe(value => {
        this.snackBar.open(`Byon ${byon.name} successfully deleted from Cloudiator`, 'Close');
        this.getByonDefinitionList();
      },
      error1 => {
        this.byonLoadingInProgress = false;
        this.snackBar.open(`Error by deleting byon ${byon.name} from Cloudiator: ${error1.error.message}`, 'Close');
      });
  }

  isByonFromCloudiator(byon: Byon) {
    return this.byonInCloudiatorList
      .find(valueFromList => (this.getPublicIpV4(valueFromList) === this.getPublicIpV4(byon)
        && valueFromList.name === byon.name));
  }

  getByonDefinitionList() {
    this.byonLoadingInProgress = true;

    this.byonService.getByonFromCloudiatorList().subscribe(byonVal => {
        this.byonInCloudiatorList = byonVal;
        this.byonService.getByonDefinitionList().subscribe(byonDefValue => {
            console.log(`Successfully getting byon definitions`);
            this.fillByonIdForCloudiatorByons(byonDefValue);
            this.updateByonTableData(byonDefValue);
            this.byonLoadingInProgress = false;
          },
          error1 => {
            this.byonLoadingInProgress = false;
            console.log('Error by getting byon definitions');
            if (error1.status !== 404) {
              this.snackBar.open(`Error by getting byon definitions: ${error1.error.message}`, 'Close');
            }
          });
      },
      error1 => {
        this.byonLoadingInProgress = false;
        if (error1.status !== 404) {
          this.snackBar.open(`Error by getting byon list available for deployment: ${error1.error.message}`, 'Close');
        }
      });
  }

  private fillByonIdForCloudiatorByons(byonDefValue: Array<Byon>) {
    byonDefValue.forEach(value => {
      if (this.isByonFromCloudiator(value)) {
        value.id = this.byonInCloudiatorList
          .find(valueFromList => (this.getPublicIpV4(valueFromList) === this.getPublicIpV4(value)
            && valueFromList.name === value.name)).id;
      }
    });
  }
}
