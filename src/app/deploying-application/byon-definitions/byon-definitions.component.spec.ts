import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ByonDefinitionsComponent} from './byon-definitions.component';

describe('ByonDefinitionsComponent', () => {
  let component: ByonDefinitionsComponent;
  let fixture: ComponentFixture<ByonDefinitionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ByonDefinitionsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByonDefinitionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
