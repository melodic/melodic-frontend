import {Component, OnInit} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {EnvironmentService} from '../service/environment.service';

@Component({
  selector: 'app-preparation-environment',
  templateUrl: './preparation-environment.component.html',
  styleUrls: ['./preparation-environment.component.css']
})
export class PreparationEnvironmentComponent implements OnInit {

  preparationInProgress = false;
  title = 'Melodic machine is ready to use';
  operationInfo = '';

  constructor(private snackBar: MatSnackBar,
              private environmentService: EnvironmentService
  ) {
  }

  ngOnInit() {
    // setTimeout(() => {
    //   this.informAboutReadyMachine();
    // }, 6000);

    // this.checkAvailableSpaceOnDevice();
    // this.cleanDatabase();
    // this.restartMachine();
    // this.informAboutReadyMachine();
  }

  private checkAvailableSpaceOnDevice() {
    this.operationInfo = 'Preparing environment...';
  }

  private cleanDatabase() {
    this.operationInfo = 'Cleaning database...';
  }

  private restartMachine() {
    this.title = 'Preparing your machine in progress...';
    this.operationInfo = 'Restarting Melodic and Cloudiator containers...';
    this.environmentService.restart().subscribe(() => {
        // checkStatus
        this.informAboutReadyMachine();
      },
      error1 => {
        console.log(`Error by restarting machine: ${error1}`);
        this.snackBar.open(`Problem by restarting your machine: ${error1.error.message}. Please, try again`, 'Close', {duration: 10000});
      }
    );
  }

  private informAboutReadyMachine() {
    this.preparationInProgress = false;
    this.title = 'Melodic machine is ready to use';
    this.operationInfo = '';
    this.snackBar.open(`${this.title}`, 'Close', {duration: 10000});
  }
}
