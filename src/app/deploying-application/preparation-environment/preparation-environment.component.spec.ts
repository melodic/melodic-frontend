import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PreparationEnvironmentComponent} from './preparation-environment.component';

describe('PreparationEnvironmentComponent', () => {
  let component: PreparationEnvironmentComponent;
  let fixture: ComponentFixture<PreparationEnvironmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PreparationEnvironmentComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreparationEnvironmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
