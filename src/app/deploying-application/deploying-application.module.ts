import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UploaderXmiComponent} from './uploader-xmi/uploader-xmi.component';
import {DeployingFormComponent} from './deploying-form/deploying-form.component';
import {DeployingApplicationRoutingModule} from './route/deploying-application-routing.module';
import {CommonTemplateModule} from '../common-template/common-template.module';
import {MatFileUploadModule} from 'angular-material-fileupload';
import {AngularMaterialModule} from '../angular-material/angular-material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DeployingMainComponent} from './deploying-main/deploying-main.component';
import {PreparationEnvironmentComponent} from './preparation-environment/preparation-environment.component';
import {CloudDefinitionFormComponent} from '../provider/cloud-definition-form/cloud-definition-form.component';
import {ProviderModule} from '../provider/provider.module';
import {FileUploaderModule} from '../file-uploader/file-uploader.module';
import {SensitiveVariablesDialogComponent} from '../file-uploader/sensitive-variables-dialog/sensitive-variables-dialog.component';
import {ByonDefinitionsComponent} from './byon-definitions/byon-definitions.component';
import {ByonModule} from '../byon/byon.module';
import {CloudDefinitionsComponent} from './cloud-definitions/cloud-definitions.component';

@NgModule({
  declarations: [
    UploaderXmiComponent,
    DeployingFormComponent,
    DeployingMainComponent,
    PreparationEnvironmentComponent,
    ByonDefinitionsComponent,
    CloudDefinitionsComponent,
  ],
  imports: [
    CommonModule,
    DeployingApplicationRoutingModule,
    CommonTemplateModule,
    MatFileUploadModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ProviderModule,
    FileUploaderModule,
    ByonModule
  ],
  exports: [
    UploaderXmiComponent,
    DeployingFormComponent,
  ],
  entryComponents: [
    CloudDefinitionFormComponent,
    SensitiveVariablesDialogComponent
  ]
})
export class DeployingApplicationModule {
}
