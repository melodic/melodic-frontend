import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DeployingMainComponent} from '../deploying-main/deploying-main.component';

const routes: Routes = [
  {path: '', component: DeployingMainComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeployingApplicationRoutingModule {
}
