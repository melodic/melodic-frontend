import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DeployingMainComponent} from './deploying-main.component';

describe('DeployingMainComponent', () => {
  let component: DeployingMainComponent;
  let fixture: ComponentFixture<DeployingMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DeployingMainComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeployingMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
