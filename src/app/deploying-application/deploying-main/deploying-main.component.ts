import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {DeploymentService} from '../service/deployment.service';
import {PreparationEnvironmentComponent} from '../preparation-environment/preparation-environment.component';
import {DeployingFormComponent} from '../deploying-form/deploying-form.component';
import {UploaderXmiComponent} from '../uploader-xmi/uploader-xmi.component';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-deploying-main',
  templateUrl: './deploying-main.component.html',
  styleUrls: ['./deploying-main.component.css']
})
export class DeployingMainComponent implements OnInit {

  @ViewChild(PreparationEnvironmentComponent) preparationEnvironmentComponent: PreparationEnvironmentComponent;
  @ViewChild(UploaderXmiComponent) uploaderXmiComponent: UploaderXmiComponent;
  @ViewChild(DeployingFormComponent) deployingFormComponent: DeployingFormComponent;

  isSendingRequest = false;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private deploymentService: DeploymentService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    localStorage.setItem('viewTitle', 'Deployment');
  }

  get deploymentForm(): FormGroup {
    return this.deployingFormComponent.getFormValue();
  }

  onStartButtonClick() {
    console.log('Deployment request from main component');
    const deploymentRequest = this.deploymentForm;
    this.isSendingRequest = true;
    this.snackBar.open('Your application is starting... It takes some time. Please wait for the result.', 'Close');
    this.deploymentService.createDeploymentProcess(deploymentRequest).subscribe(value => {
        console.log(`Successful deployment request: ${value} with process id=${value.processCreationResult.processId}`);
        this.snackBar.open(`Successful starting your application`, 'Close', {duration: 10000});
        this.router.navigate([`/process/${value.processCreationResult.processId}`]);
        this.isSendingRequest = false;
      },
      error1 => {
        console.log(`Error by sending deployment request, message: ${error1.error.message}`);
        this.snackBar.open(`${error1.error.message}`, 'Close', {duration: 1000000});
        this.isSendingRequest = false;
      });
  }

  // update available xmi models names
  onGoToDeploymentFormClick() {
    this.deployingFormComponent.getAvailableApplicationIds();
  }
}
