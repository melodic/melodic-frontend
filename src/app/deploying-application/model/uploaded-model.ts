import {SecureVariable} from '../../file-uploader/model/secure-variable';

export enum SecureVariablesExistence {
  YES, NO, UNKNOWN
}

export class UploadedModel {
  name: string;
  secureVariablesToDefine: Array<SecureVariable>;
  secureVariablesExistence: SecureVariablesExistence;

  constructor(private nameArg: string, private secureVariablesToDefineArg: Array<SecureVariable>,
              private secureVariablesExistenceArg: SecureVariablesExistence) {
    this.name = nameArg;
    if (secureVariablesToDefineArg === null) {
      this.secureVariablesToDefine = [];
      this.secureVariablesExistence = SecureVariablesExistence.UNKNOWN;
    } else {
      this.secureVariablesToDefine = secureVariablesToDefineArg;
      if (secureVariablesExistenceArg !== null) {
        this.secureVariablesExistence = secureVariablesExistenceArg;
      } else {
        this.secureVariablesExistence = secureVariablesToDefineArg.length > 0 ? SecureVariablesExistence.YES : SecureVariablesExistence.NO;
      }
    }
  }
}
