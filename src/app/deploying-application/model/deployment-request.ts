import {CloudDefinition} from '../../provider/model/cloud-definition';

export class DeploymentRequest {
  applicationId: string;
  isSimulation: boolean;
  username: string;
  password: string;
  cloudDefinitions: Array<CloudDefinition>;
}
