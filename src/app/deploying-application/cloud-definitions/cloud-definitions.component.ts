import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {CloudDefinitionForRead} from '../../provider/model/view/cloud-definition-for-read';
import {ProviderService} from '../../provider/service/provider.service';
import {ParentProperty} from '../../provider/model/parent-property';
import {Router} from '@angular/router';

@Component({
  selector: 'app-cloud-definitions',
  templateUrl: './cloud-definitions.component.html',
  styleUrls: ['./cloud-definitions.component.css']
})
export class CloudDefinitionsComponent implements OnInit {

  providersSettingsLoadingInProgress = false;

  data: MatTableDataSource<CloudDefinitionForRead>;
  displayedColumns: string[] = ['no', 'provider', 'properties', 'choose'];

  private cloudDefinitions: CloudDefinitionForRead[];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private snackBar: MatSnackBar,
              private providerService: ProviderService,
              private router: Router
  ) {
  }

  ngOnInit() {
    this.providersSettingsLoadingInProgress = true;
    this.providerService.getAllCloudDefinitions().subscribe(value => {
        console.log(`Successful getting cloud definitions for all providers in number: ${value.length}`);

        this.cloudDefinitions = value;
        this.cloudDefinitions.forEach(cloudDef =>
          cloudDef.checked = false);

        this.data = new MatTableDataSource<CloudDefinitionForRead>(this.cloudDefinitions);
        this.data.paginator = this.paginator;
        this.data.sort = this.sort;
        this.providersSettingsLoadingInProgress = false;
      },
      error1 => {
        this.providersSettingsLoadingInProgress = false;
        console.log('Error by getting cloud definitions for all providers');
        if (error1.status !== 404) {
          this.snackBar.open(`Error by getting cloud definition for all providers: ${error1.error.message}`, 'Close');
        }
      });
  }

  getCloudDefinitions() {
    return this.cloudDefinitions;
  }

  onProvidersButtonClick() {
    console.log('On providers button click');
    this.router.navigate(['/provider/cloud-definition']);
  }

  onCheckboxUseInDeploymentChange(cloudDef: CloudDefinitionForRead) {
    console.log(`Change checkbox for cloud def: ${cloudDef.api.providerName}, now checked = ${cloudDef.checked}
    the whole number of definitions: ${this.cloudDefinitions.length}`);
    if (cloudDef.checked === false) { // uncheck all properties for unchecked provider
      cloudDef.cloudConfiguration.properties.forEach(value => value.checked = false);
    }
  }

  onPropertiesCheckboxChange(properties: ParentProperty, cloudDef: CloudDefinitionForRead) {
    if (properties.checked === true) { // check cloud definition for checked properties
      cloudDef.checked = true;
    }
    this.onCheckboxUseInDeploymentChange(cloudDef);
  }

  private hasDuplicatedProperty(properties: ParentProperty, cloudDef: CloudDefinitionForRead): boolean {
    for (const newProperty of properties.properties) {
      for (const parentProperty of cloudDef.cloudConfiguration.properties) {
        if (parentProperty.checked && parentProperty.name !== properties.name) {
          for (const singleProperty of parentProperty.properties) {
            if (singleProperty.key === newProperty.key) {
              return true;
            }
          }
        }
      }
    }
    return false;
  }
}
