import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CloudDefinitionsComponent} from './cloud-definitions.component';

describe('CloudDefinitionsComponent', () => {
  let component: CloudDefinitionsComponent;
  let fixture: ComponentFixture<CloudDefinitionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CloudDefinitionsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloudDefinitionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
