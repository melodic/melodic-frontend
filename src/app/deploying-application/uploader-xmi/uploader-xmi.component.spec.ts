import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UploaderXmiComponent} from './uploader-xmi.component';

describe('UploaderXmiComponent', () => {
  let component: UploaderXmiComponent;
  let fixture: ComponentFixture<UploaderXmiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UploaderXmiComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploaderXmiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
