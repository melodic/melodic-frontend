import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {DeploymentService} from '../service/deployment.service';
import {AppConfigService} from '../../app-config/service/app-config.service';
import {SecureVariablesExistence, UploadedModel} from '../model/uploaded-model';
import {SensitiveVariablesDialogComponent} from '../../file-uploader/sensitive-variables-dialog/sensitive-variables-dialog.component';
import {SecureVariablesService} from '../../file-uploader/service/secure-variables.service';
import {SecureVariable} from '../../file-uploader/model/secure-variable';
import {MenuComponent} from '../../common-template/menu/menu.component';


@Component({
  selector: 'app-uploader-xmi',
  templateUrl: './uploader-xmi.component.html',
  styleUrls: ['./uploader-xmi.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class UploaderXmiComponent implements OnInit {

  sensitiveVariablesDialog: MatDialogRef<SensitiveVariablesDialogComponent>;

  title = 'Upload xmi';
  uploadInProgress = false;
  deletingInProgress = false;
  downloadingModelsListInProgress = false;
  secureVariablesUploadingInProgress = false;
  uploadXmiUrl = `${AppConfigService.settings.apiUrl}/auth/deployment/xmi`;
  uploadedModels = new Array<UploadedModel>();

  constructor(private snackBar: MatSnackBar,
              private dialog: MatDialog,
              private deploymentService: DeploymentService,
              private secureVariablesService: SecureVariablesService,
              private menuComponent: MenuComponent) {
  }

  ngOnInit() {
    this.getUploadedModelsList();
  }

  onFileUploadEvent(event) {
    this.uploadInProgress = true;
    console.log('Received event: ', event);
    if (event.event.hasOwnProperty('status')) {
      console.log(`Response from uploading status: ${event.event.status}`);

      if (event.event.status === 201 && event.event.body && event.file) {
        this.snackBar.open(`Successful uploading your model: ${event.file.name}`, 'Close', {duration: 10000});
        this.uploadInProgress = false;
        this.getUploadedModelsList();
      } else if (event.event.error) {
        console.log(`Error by uploading model: ${event.file.name}, message: ${event.event.error.message}`);
        let errorMessageForUser = event.event.error.message ? event.event.error.message : 'Error by uploading xmi model. ' +
          'Problem with connection with CDO.';
        if (errorMessageForUser.length > 300) {
          errorMessageForUser = errorMessageForUser.substring(0, 300) + ' ...';
        }
        this.snackBar.open(`${errorMessageForUser}`, 'Close');
        this.uploadInProgress = false;
      }
    }
  }

  public isAnyUploadedReadyModel() {
    return this.uploadedModels.length !== 0 && !this.secureVariablesService.isAnyModelWithMissingSecureVariables();
  }

  public getUploadedModelsList() {
    this.downloadingModelsListInProgress = true;
    this.deploymentService.getUploadedModels().subscribe(value => {
        console.log(`Getting all uploaded models`);
        this.downloadingModelsListInProgress = false;
        if (value.length === 0) {
          localStorage.removeItem(this.secureVariablesService.modelsInLocalStorageLabel);
          this.uploadedModels = [];
        } else {
          this.uploadedModels = this.secureVariablesService.mapUploadedModelNamesToModels(value);
          let uploadedModelsFromLocalStorage = JSON.parse(localStorage.getItem(this.secureVariablesService.modelsInLocalStorageLabel));
          uploadedModelsFromLocalStorage = uploadedModelsFromLocalStorage !== null ? uploadedModelsFromLocalStorage : [];
          this.secureVariablesService.removeInfoFromLocalStorageAboutFilesNotFromCdo(value, uploadedModelsFromLocalStorage);
        }
        console.log('Uploaded models: ', this.uploadedModels);
      },
      error1 => {
        this.downloadingModelsListInProgress = false;
        this.uploadedModels = [];
        console.log(`Problem by getting uploaded models`);
        this.snackBar.open(`Problem by getting available models from CDO.`, 'Close', {duration: 10000});
      });
  }

  onDeleteModelClick(model: UploadedModel) {
    const modelName = model.name;
    console.log(`Delete click for model with name: ${modelName}`);
    this.deletingInProgress = true;
    this.menuComponent.blockUIView(`Deleting model ${modelName} in progress...`);
    this.deploymentService.deleteModel(modelName).subscribe(() => {
        this.menuComponent.unblockUIView();
        console.log(`Model ${modelName} successfully deleted`);
        this.snackBar.open(`Model ${modelName} successfully deleted from CDO.`, 'Close', {duration: 10000});
        this.deletingInProgress = false;
        this.getUploadedModelsList();
      },
      error1 => {
        this.menuComponent.unblockUIView();
        console.log(`Problem by deleting model ${modelName}`);
        this.snackBar.open(`Problem by deleting model ${modelName} from CDO.`, 'Close', {duration: 10000});
        this.deletingInProgress = false;
      });
  }

  getInformationForModel(model: UploadedModel): string {
    if (model.secureVariablesExistence === SecureVariablesExistence.NO) {
      return 'Application model without secure variables.';
    } else if (model.secureVariablesExistence === SecureVariablesExistence.YES && model.secureVariablesToDefine.length !== 0) {
      return 'Defining of secure variables for application model is required!';
    } else if (model.secureVariablesExistence === SecureVariablesExistence.YES && model.secureVariablesToDefine.length === 0) {
      return 'Secure variables for application model are stored.';
    } else {
      return 'No information about secure variables. If you not sure about putting them, please remove and upload again your model.';
    }
  }

  variablesForDefiningExist(model: UploadedModel): boolean {
    return model.secureVariablesToDefine.length !== 0;
  }

  onDefineSecureVariablesClick(model: UploadedModel) {
    console.log(`Defining secure variables for model: ${model.name} click`);
    this.openDialog(model.secureVariablesToDefine, model.name);
  }

  public openDialog(sensitiveVariables: Array<SecureVariable>, modelName: string): void {
    this.sensitiveVariablesDialog = this.dialog.open(SensitiveVariablesDialogComponent, {
      hasBackdrop: true,
      disableClose: true,
      data: {
        variables: sensitiveVariables
      },
      width: '50%'
    });

    this.sensitiveVariablesDialog.afterClosed().subscribe(result => {
      console.log(`Secure variables dialog closed with result`, result);
      this.secureVariablesUploadingInProgress = true;
      this.saveSensitiveVariables(result, modelName);
    });
  }

  saveSensitiveVariables(result: any, modelName: string) {
    this.informAboutSavingSecureVariables(modelName);
    this.secureVariablesService.saveSecureVariables(result).subscribe(value => {
        console.log(`Sensitive variables successfully stored`);
        this.setSavingSecureVariablesAsFinished();
        this.snackBar.open(`Sensitive variables successfully saved. Your model is ready for deployment`, 'Close');
        this.secureVariablesService.removeSecureExistenceFromModelInLocalStorage(modelName);
        this.getUploadedModelsList();
      },
      error1 => {
        this.setSavingSecureVariablesAsFinished();
        this.snackBar.open(`${error1.error.message}`, 'Close');
      });
  }

  informAboutSavingSecureVariables(modelName: string) {
    this.secureVariablesUploadingInProgress = true;
    this.menuComponent.blockUIView(`Saving secure variables for model ${modelName} in progress...`);
  }

  setSavingSecureVariablesAsFinished() {
    this.secureVariablesUploadingInProgress = false;
    this.menuComponent.unblockUIView();
  }
}
