import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DeployingFormComponent} from './deploying-form.component';

describe('DeployingFormComponent', () => {
  let component: DeployingFormComponent;
  let fixture: ComponentFixture<DeployingFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DeployingFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeployingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
