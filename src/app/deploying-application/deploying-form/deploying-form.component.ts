import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {ProviderService} from '../../provider/service/provider.service';
import {Router} from '@angular/router';
import {CloudDefinitionForRead} from '../../provider/model/view/cloud-definition-for-read';
import {DeploymentService} from '../service/deployment.service';
import {UserService} from '../../user/service/user.service';
import {CloudDefinitionsComponent} from '../cloud-definitions/cloud-definitions.component';

@Component({
  selector: 'app-deploying-form',
  templateUrl: './deploying-form.component.html',
  styleUrls: ['./deploying-form.component.css']
})
export class DeployingFormComponent implements OnInit {

  @ViewChild(CloudDefinitionsComponent) cloudDefinitionsComponent: CloudDefinitionsComponent;

  deploymentForm: FormGroup;
  passwordHide = true;
  xmiModelsLoadingInProgress = false;
  applicationIds = [];

  constructor(private snackBar: MatSnackBar,
              private formBuilder: FormBuilder,
              private providerService: ProviderService,
              private router: Router,
              private deploymentService: DeploymentService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.getAvailableApplicationIds();
    this.deploymentForm = this.formBuilder.group({
      username: [this.userService.currentUser.username, Validators.required],
      applicationId: ['', Validators.required],
      isSimulation: [false, Validators.required],
      cloudDefinitions: new Array<CloudDefinitionForRead>(),
    });
  }

  get form() {
    return this.deploymentForm.controls;
  }

  getFormValue() {
    this.form.cloudDefinitions.setValue(this.deploymentService
      .mapChosenCloudDefToCloudDef(this.cloudDefinitionsComponent.getCloudDefinitions()));
    return this.deploymentForm.value;
  }

  getAvailableApplicationIds() {
    this.xmiModelsLoadingInProgress = true;
    this.deploymentService.getUploadedModels().subscribe(
      value => {
        this.applicationIds = value;
        if (this.applicationIds.length > 0) {
          this.form.applicationId.setValue(this.applicationIds[0]);
        }
        this.xmiModelsLoadingInProgress = false;
      },
      error1 => {
        this.xmiModelsLoadingInProgress = false;
        console.log('Error by getting all application ids');
        this.snackBar.open(`Problem by getting available models from CDO.`, 'Close', {duration: 10000});
      }
    );
  }

  isDeploymentFormCompleted(): boolean {
    return this.deploymentForm === undefined ? false : this.deploymentForm.valid;
  }

  isCloudDefinitionLoadingInProgress(): boolean {
    return this.cloudDefinitionsComponent.providersSettingsLoadingInProgress;
  }

  getErrorRequiredMessage() {
    return 'You must enter a value';
  }
}
