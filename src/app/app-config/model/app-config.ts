export interface AppConfig {
  apiUrl: string;
  camundaUrl: string;
  grafanaUrl: string;
  webSshUrl: string;
  kibanaUrl: string;
}
