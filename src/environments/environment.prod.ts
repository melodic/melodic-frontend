export const environment = {
  production: true,
  useRuntimeConfig: true,
  href: '/',
  apiUrl: 'https://gui-backend:8078',
  camundaUrl: 'http://gui-backend:8095',
  grafanaUrl: 'http://gui-backend:3000',
  webSshUrl: 'https://gui-backend:4433',
  kibanaUrl: 'http://gui-backend:5601'
};
